webpackJsonp([0],{

/***/ 11:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanguageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the LanguageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
let LanguageProvider = class LanguageProvider {
    constructor(http) {
        this.http = http;
        this.language = 'english';
        this.newLanguageArray = [];
        this.labelLanguageArray = [];
        console.log('Hello LanguageProvider Provider');
    }
    fnGetLanguage(string) {
        if (this.labelLanguageArray && this.labelLanguageArray[this.language] && this.labelLanguageArray[this.language][string] && this.labelLanguageArray[this.language][string] != '') {
            return this.labelLanguageArray[this.language][string];
        }
        else {
            return string;
        }
    }
    fnSetLanguageArray(language) {
        localStorage.setItem('langList', JSON.stringify(language));
        this.languageArray = language;
    }
    fnSetAppLanguage(language) {
        localStorage.setItem('appLang', language);
        this.language = language;
    }
    fnSetLangLabelArray(labelArray) {
        localStorage.setItem('langArray', JSON.stringify(labelArray));
        this.labelLanguageArray = labelArray;
    }
    fnGetSelectedAppLang() {
        return this.language;
    }
};
LanguageProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
], LanguageProvider);

//# sourceMappingURL=language.js.map

/***/ }),

/***/ 13:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomeProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_http__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the CustomeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
let CustomeProvider = class CustomeProvider {
    constructor(http, language, nativeHttp) {
        this.http = http;
        this.language = language;
        this.nativeHttp = nativeHttp;
        this.baseUrl = 'http://www.brijwasibj.com/api/v1.0/';
        // baseUrl = 'http://www.hrsales.co.in/api/test_v1.2/'
        this.token = '';
        this.j = 0;
        console.log('Hello CustomeProvider Provider');
        if (localStorage.getItem('token')) {
            this.token = localStorage.getItem('token');
        }
    }
    post(url, data) {
        var headers = {};
        if (localStorage.getItem('token')) {
            headers = {
                'authorization': localStorage.getItem('token')
            };
        }
        if (data == 'data' || data == '') {
            data = {};
        }
        return new Promise((resolve, reject) => {
            return this.nativeHttp.post(this.baseUrl + url, data, headers)
                .then((result) => {
                var data;
                if (result.data) {
                    data = JSON.parse(result.data);
                }
                if (data.status == 1) {
                    resolve(data);
                }
                else {
                    reject(data);
                }
            }, (error) => {
                let err = {
                    status: 'error',
                    message: this.language.fnGetLanguage('make sure that you are connected with internet'),
                    flag: this.j
                };
                this.j++;
                reject(err);
            });
        });
    }
    imageDownloder(url, data) {
        var headers = {};
        if (localStorage.getItem('token')) {
            headers = {
                'authorization': localStorage.getItem('token')
            };
        }
        return new Promise((resolve, reject) => {
            return this.http.post(this.baseUrl + url, data, headers).subscribe((result) => {
                if (result.status == 1) {
                    resolve(result);
                }
                else {
                    reject(result);
                }
            }, (error) => {
                let err = {
                    status: 'error',
                    message: 'Make sure that you are connected with internet',
                    flag: this.j
                };
                this.j++;
                reject(err);
            });
        });
    }
};
CustomeProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
        __WEBPACK_IMPORTED_MODULE_3__language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_http__["a" /* HTTP */]])
], CustomeProvider);

//# sourceMappingURL=custome.js.map

/***/ }),

/***/ 132:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__category_category__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__product_product__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__cart_cart__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jquery__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_language_language__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__filter_filter__ = __webpack_require__(277);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










let HomePage = class HomePage {
    constructor(navCtrl, language, loadingCtrl, navParams, actionSheetCtrl, event, modalCtrl, alertCtrl, service, cart) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.actionSheetCtrl = actionSheetCtrl;
        this.event = event;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.service = service;
        this.cart = cart;
        this.showFilter = false;
        this.Special = false;
        this.itemCode = '';
        this.size = '';
        this.showHeaderFilterBar = true;
        this.width = (window.innerWidth - 52) / 2;
        this.items = [];
        this.selectedSub = 'all';
        this.sortBy = 'default';
        this.selectedSubCategory = '';
        this.selectedSize = '';
        this.isSpecial = false;
        this.inStock = false;
        this.price = { upper: '0', lower: '0' };
        this.selectedPrice = { upper: '0', lower: '0' };
        this.sizeArray = [];
        this.appLang = localStorage.getItem('appLang');
        this.lazyLoadding = localStorage.getItem('lazyLoadding');
    }
    ionViewDidLoad() {
        this.appLang = localStorage.getItem('appLang');
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.cartItem = this.cart.fnGetCountOfCart();
        this.event.subscribe('cart:itemAdded', () => {
            this.cartItem = this.cart.fnGetCountOfCart();
        });
        this.event.subscribe('appLangChange', () => {
            this.appLang = this.language.fnGetSelectedAppLang();
        });
        let category = this.navParams.get('data');
        this.data = category;
        let form = {
            'main_category_id': category.category_id,
            'is_main': '1'
        };
        this.service.post('item/get_item_list', form)
            .then((result) => {
            this.items = result.item_list;
            console.log(this.items);
            this.subCategorys = result.sub_category;
            this.pagination = result.pagination;
            this.price.lower = '0';
            this.price.upper = result.price_max.toString();
            this.selectedPrice.lower = '0';
            this.selectedPrice.upper = result.price_max.toString();
            this.sizeArray = result.size;
            this.scrollToTop();
            loader.dismiss();
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
        });
    }
    scrollToTop() {
        this.content.scrollToTop();
    }
    fnCategary() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__category_category__["a" /* CategoryPage */]);
    }
    subCategoryChange() {
        this.itemCode = '';
        this.size = '';
        this.Special = false;
        if (this.selectedSub == 'all') {
            this.ionViewDidLoad();
        }
        else if (this.selectedSub == 'special') {
            let category = this.navParams.get('data');
            let form = {
                'main_category_id': this.mainCategoryId,
                'category_id': category.category_id,
                'is_main': '1',
                'is_special': '1'
            };
            this.service.post('item/get_item_list', form).then((result) => {
                this.items.length = 0;
                this.items = result.item_list;
                this.scrollToTop();
            }, (error) => {
            });
        }
        else {
            let form = {
                'category_id': this.selectedSub,
                'is_main': '0'
            };
            this.service.post('item/get_item_list', form).then((result) => {
                this.items.length = 0;
                this.items = result.item_list;
                this.scrollToTop();
            }, (error) => {
            });
        }
    }
    fnOpenCart() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__cart_cart__["a" /* CartPage */]);
    }
    fnOpenProductPage(item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__product_product__["a" /* ProductPage */], { 'data': item });
    }
    fnFilter() {
        let form = {};
        if (this.selectedSubCategory && this.selectedSubCategory != '') {
            form['category_id'] = this.selectedSubCategory;
            form['is_main'] = '0';
        }
        else {
            form['is_main'] = '1';
        }
        if (this.selectedSize && this.selectedSize != '') {
            form['size'] = this.selectedSize;
        }
        if (this.itemCode && this.itemCode != '') {
            form['item_code'] = this.itemCode;
        }
        if (this.isSpecial) {
            form['is_special'] = '1';
        }
        if (this.inStock) {
            form['in_stock'] = '1';
        }
        form['main_category_id'] = this.data.category_id;
        form['lower'] = this.selectedPrice.lower;
        form['upper'] = this.selectedPrice.upper;
        if (this.sortBy == 'lth') {
            form['sort_order'] = 'asc';
            form['sort_by'] = 'price';
        }
        if (this.sortBy == 'htl') {
            form['sort_order'] = 'desc';
            form['sort_by'] = 'price';
        }
        let loader = this.loadingCtrl.create({ content: 'Loadding...' });
        loader.present();
        this.service.post('item/get_item_list', form)
            .then((result) => {
            loader.dismiss();
            this.items = result.item_list;
            this.pagination = result.pagination;
            this.showFilter = false;
            this.scrollToTop();
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
        });
    }
    fnLoadMoreData(e) {
        let form = {};
        if (this.selectedSubCategory && this.selectedSubCategory != '') {
            form['category_id'] = this.selectedSubCategory;
            form['is_main'] = '0';
        }
        else {
            form['is_main'] = '1';
        }
        if (this.selectedSize && this.selectedSize != '') {
            form['size'] = this.selectedSize;
        }
        if (this.itemCode && this.itemCode != '') {
            form['item_code'] = this.itemCode;
        }
        if (this.isSpecial) {
            form['is_special'] = '1';
        }
        if (this.inStock) {
            form['in_stock'] = '1';
        }
        form['main_category_id'] = this.data.category_id;
        form['lower'] = this.selectedPrice.lower;
        form['upper'] = this.selectedPrice.upper;
        if (this.sortBy == 'lth') {
            form['sort_order'] = 'asc';
            form['sort_by'] = 'price';
        }
        if (this.sortBy == 'htl') {
            form['sort_order'] = 'desc';
            form['sort_by'] = 'price';
        }
        form['page'] = this.pagination.next;
        this.service.post('item/get_item_list', form)
            .then((result) => {
            this.items = this.items.concat(result.item_list);
            this.pagination = result.pagination;
            e.complete();
        }, (error) => {
            e.complete();
        });
    }
    fnImageNotFound(event) {
        event.target.src = "assets/img_logo/default-product.jpg";
    }
    onScroll(e) {
        if (e.directionY === 'up') {
            this.showHeaderFilterBar = true;
            __WEBPACK_IMPORTED_MODULE_7_jquery__('.selector').fadeIn();
            if (this.showFilter)
                __WEBPACK_IMPORTED_MODULE_7_jquery__('.filters').fadeIn();
        }
        else {
            if (e.deltaY < 80 && e.scrollTop < 80) {
                this.showHeaderFilterBar = true;
                __WEBPACK_IMPORTED_MODULE_7_jquery__('.selector').fadeIn();
                if (this.showFilter) {
                    __WEBPACK_IMPORTED_MODULE_7_jquery__('.filters').fadeIn();
                }
            }
            else {
                this.showHeaderFilterBar = false;
                __WEBPACK_IMPORTED_MODULE_7_jquery__('.selector').fadeOut();
                __WEBPACK_IMPORTED_MODULE_7_jquery__('.filters').fadeOut();
            }
        }
    }
    fnOpenFilterModal() {
        let data = {
            category_id: this.selectedSubCategory,
            size: this.size,
            item_code: this.itemCode,
            is_special: this.isSpecial,
            in_stock: this.inStock,
            selectedPrice: this.selectedPrice,
            subCategorys: this.subCategorys,
            sizeArray: this.sizeArray,
            price: this.price,
            appLang: this.appLang
        };
        let searchModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__filter_filter__["a" /* FilterPage */], { 'data': data }, { enableBackdropDismiss: false });
        searchModal.present();
        searchModal.onDidDismiss((data) => {
            if (!data.data.isCancelled && data.data.filterData) {
                this.selectedSubCategory = data.data.filterData.category_id;
                this.size = data.data.filterData.size;
                this.itemCode = data.data.filterData.item_code;
                this.isSpecial = data.data.filterData.is_special;
                this.inStock = data.data.filterData.in_stock;
                this.selectedPrice = data.data.filterData.selectedPrice;
                this.items = [];
                this.fnFilter();
            }
        });
    }
    fnSort() {
        let SortActionSheet = this.actionSheetCtrl.create({
            title: this.language.fnGetLanguage('sort_by'),
            buttons: [
                {
                    text: this.language.fnGetLanguage('new_arrival'),
                    cssClass: this.sortBy == 'default' ? 'selected' : 'notselected',
                    icon: this.sortBy == 'default' ? 'checkmark' : '',
                    handler: () => {
                        if (this.sortBy != 'default') {
                            this.sortBy = 'default';
                            this.items = [];
                            this.ionViewDidLoad();
                        }
                    }
                },
                {
                    text: this.language.fnGetLanguage('price_low_to_high'),
                    cssClass: this.sortBy == 'lth' ? 'selected' : 'notselected',
                    icon: this.sortBy == 'lth' ? 'checkmark' : '',
                    handler: () => {
                        if (this.sortBy != 'lth') {
                            this.sortBy = 'lth';
                            this.items = [];
                            this.fnFilter();
                        }
                    }
                },
                {
                    text: this.language.fnGetLanguage('price_high_to_low'),
                    cssClass: this.sortBy == 'htl' ? 'selected' : 'notselected',
                    icon: this.sortBy == 'htl' ? 'checkmark' : '',
                    handler: () => {
                        if (this.sortBy != 'htl') {
                            this.sortBy = 'htl';
                            this.items = [];
                            this.fnFilter();
                        }
                    }
                }
            ]
        });
        SortActionSheet.present();
    }
    fnLoadImage(elementID) {
        let id = "#" + elementID;
        __WEBPACK_IMPORTED_MODULE_7_jquery__(id).css("background", "none");
        __WEBPACK_IMPORTED_MODULE_7_jquery__(id).css("background", "#fff");
    }
};
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Content */])
], HomePage.prototype, "content", void 0);
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-home',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/home/home.html"*/'<!--\n  Generated template for the CategoryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n        <ion-navbar style="color: black;">\n                <button menuToggle ion-button icon-only>\n                        <ion-icon color="dark" name="menu"></ion-icon>\n                </button>\n                <ion-title>\n                        <div></div>\n                        <div *ngIf="data && data.category_name">\n                                <span *ngIf="data.category_name_lang[appLang]">\n                                        {{data.category_name_lang[appLang]}}\n                                </span>\n                                <span *ngIf="!data.category_name_lang[appLang]">\n                                        {{ data.category_name }}\n                                </span>\n                        </div>\n                </ion-title>\n                <ion-buttons end>\n                        <button (click)="fnOpenCart()" ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n        <ion-toolbar style="height: auto; padding: 0;" class="selector">\n                <ion-row>\n                        <ion-col col-6 text-center (click)="fnSort()">\n                                <ion-icon color="dart" name="md-arrow-round-up"></ion-icon>\n                                <ion-icon color="dart" style="margin-left: -5px" name="md-arrow-round-down"></ion-icon>\n                                {{language.fnGetLanguage(\'sort\')}}\n                        </ion-col>\n                        <ion-col col-6 text-center (click)="fnOpenFilterModal()">\n                                <ion-icon color="dart" name="funnel"></ion-icon>{{language.fnGetLanguage(\'filter\')}} \n                        </ion-col>\n                </ion-row>\n        </ion-toolbar>\n\n</ion-header>\n\n<ion-content class="asd" (ionScroll)="onScroll($event)" overflow-scroll="false">\n        <div padding style="margin-top:55px ">\n                <div *ngIf="items && items.length > 0">\n                        <ion-row lazy-load-images>\n                                <ion-col *ngFor="let item of items" col-6 (click)="fnOpenProductPage(item)">\n                                        <div class="setBackground" [style.width.px]="width" [style.minHeight.px]="width*4/3" id="prod_image_{{i}}_{{item.item_id}}">\n                                                <img [attr.data-src]="item.item_image" (load)="fnLoadImage(\'prod_image_\'+i+\'_\'+item.item_id)" *ngIf="lazyLoadding">\n                                                <img [src]="item.item_image" *ngIf="!lazyLoadding">\n                                        </div>\n                                        <div class="special_tag" *ngIf="item.in_stock==\'1\'&&item.is_special==1">\n                                                {{language.fnGetLanguage(\'special\')}}\n                                        </div>\n                                        <div class="special_tag" style="background: red !important" *ngIf="item.in_stock==\'0\'">\n                                                {{language.fnGetLanguage(\'title_out_of_stock\')}}\n                                        </div>\n                                        <div style="text-align: center">\n                                                <p style="line-height: 20px">\n                                                        <span style="font-weight: 800">\n                                                                <span style="color: gray">\n                                                                        {{language.fnGetLanguage(\'code\')}} : {{\n                                                                        item.item_code }}\n                                                                </span>\n                                                                <br>\n                                                                <span *ngIf="item.item_name_lang[appLang]">\n                                                                        {{item.item_name_lang[appLang]}}\n                                                                </span>\n                                                                <span *ngIf="!item.item_name_lang[appLang]">\n                                                                        {{ item.item_name }}\n                                                                </span>\n                                                                <br>\n                                                        </span>\n                                                        <span style="color: #6b4d0f; font-weight:bold; line-height: 20px;padding-top: 20px">₹{{\n                                                                item.sale_price.toLocaleString(\'en-IN\') }} </span>\n\n                                                </p>\n                                        </div>\n                                </ion-col>\n                        </ion-row>\n                </div>\n                <div *ngIf="pagination && pagination.next">\n                        <ion-infinite-scroll (ionInfinite)="fnLoadMoreData($event)">\n                                <ion-infinite-scroll-content></ion-infinite-scroll-content>\n                        </ion-infinite-scroll>\n                </div>\n                <div *ngIf="!items || items.length == 0" style="margin: 10%; text-align: center; position: absolute; top: 30%"\n                        class="noitem">\n                        <img src="assets/img_logo/empty-shopping-cart.png">\n                        <br>\n                        <h6>\n                                {{language.fnGetLanguage(\'no_any_record_found\')}}\n                        </h6>\n                </div>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_8__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_custome_custome__["a" /* CustomeProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_cart_cart__["a" /* CartProvider */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OtpVerifyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__category_category__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shipping_address_shipping_address__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the OtpVerifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let OtpVerifyPage = class OtpVerifyPage {
    constructor(navCtrl, language, cart, event, loadingCtrl, navParams, service, alertCtrl) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.cart = cart;
        this.event = event;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.password = '';
        this.conirmPassword = '';
        this.otp = '';
        this.firstStap = true;
        this.resenedOtpFlag = true;
        this.second = 60;
        this.ForgottPAsswordIcon = false;
    }
    ionViewDidLoad() {
        this.phoneNo = this.navParams.get('mobile_no');
        if (this.navCtrl.getPrevious().name == 'ForgottPasswordPage') {
            this.ForgottPAsswordIcon = true;
        }
        if (this.phoneNo) {
            let interval = setInterval(() => {
                if (this.second > 0) {
                    this.second--;
                }
                else {
                    this.resenedOtpFlag = false;
                    clearInterval(interval);
                }
            }, 1000);
        }
    }
    fnRegisterSecondStap() {
        if (!this.otp || this.otp == '') {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('enter_valid_otp') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        if (!this.password || this.password == '') {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('enter_valid_password') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        if (this.password != this.conirmPassword) {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('password_and_confirm_password_does_not_match') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        let form = {
            'otp': this.otp,
            'password': this.password,
            'mobile_no': this.phoneNo
        };
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.service.post('login/create_password', form).then((result) => {
            let cart = localStorage.getItem('cart');
            localStorage.clear();
            localStorage.setItem('cart', cart);
            loader.dismiss();
            localStorage.setItem('token', result.auth_token);
            this.service.token = result.auth_token;
            this.service.post('account/get_account_details', form).then((result) => {
                localStorage.setItem('customer_id', result.user.customer_id);
                localStorage.setItem('name', result.user.name);
                localStorage.setItem('mobile_no', result.user.mobile_no);
                localStorage.setItem('address_line_1', result.user.address_line_1);
                localStorage.setItem('address_line_2', result.user.address_line_2);
                localStorage.setItem('city', result.user.city);
                localStorage.setItem('state', result.user.state);
                localStorage.setItem('country', result.user.country);
                localStorage.setItem('email_id', result.user.email_id);
                localStorage.setItem('phone_no', result.user.phone_no);
                localStorage.setItem('photo', result.user.photo);
                localStorage.setItem('created_on', result.user.created_on);
                localStorage.setItem('status', result.user.status);
                this.event.publish('userloggedin');
                if (this.ForgottPAsswordIcon) {
                    let alert = this.alertCtrl.create({
                        subTitle: this.language.fnGetLanguage('new_password_has_been_set') + '.',
                        buttons: [this.language.fnGetLanguage('title_ok')]
                    });
                    alert.present();
                    alert.onDidDismiss((result) => {
                        if (this.cart.isPlaceOrder) {
                            if (!localStorage.getItem('name') || localStorage.getItem('name') == '' || localStorage.getItem('name') == 'null' || localStorage.getItem('name') == null) {
                                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__["a" /* UserDetailPage */]);
                            }
                            else {
                                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__shipping_address_shipping_address__["a" /* ShippingAddressPage */]);
                            }
                        }
                        else {
                            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__category_category__["a" /* CategoryPage */]);
                        }
                    });
                }
                else {
                    if (this.cart.isPlaceOrder) {
                        if (!localStorage.getItem('name') || localStorage.getItem('name') == '' || localStorage.getItem('name') == 'null' || localStorage.getItem('name') == null) {
                            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__["a" /* UserDetailPage */]);
                        }
                        else {
                            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__shipping_address_shipping_address__["a" /* ShippingAddressPage */]);
                        }
                    }
                    else {
                        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__["a" /* UserDetailPage */]);
                    }
                }
            }, (error) => {
                loader.dismiss();
                this.event.publish('userloggedin');
                if (this.ForgottPAsswordIcon) {
                    let alert = this.alertCtrl.create({
                        subTitle: this.language.fnGetLanguage('new_password_set') + '.',
                        buttons: [this.language.fnGetLanguage('title_ok')]
                    });
                    alert.present();
                    alert.onDidDismiss((result) => {
                        if (this.cart.isPlaceOrder) {
                            if (!localStorage.getItem('name') || localStorage.getItem('name') == '' || localStorage.getItem('name') == 'null' || localStorage.getItem('name') == null) {
                                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__["a" /* UserDetailPage */]);
                            }
                            else {
                                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__shipping_address_shipping_address__["a" /* ShippingAddressPage */]);
                            }
                        }
                        else {
                            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__category_category__["a" /* CategoryPage */]);
                        }
                    });
                }
                else {
                    if (this.cart.isPlaceOrder) {
                        if (!localStorage.getItem('name') || localStorage.getItem('name') == '' || localStorage.getItem('name') == 'null' || localStorage.getItem('name') == null) {
                            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__["a" /* UserDetailPage */]);
                        }
                        else {
                            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__shipping_address_shipping_address__["a" /* ShippingAddressPage */]);
                        }
                    }
                    else {
                        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__["a" /* UserDetailPage */]);
                    }
                }
            });
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            else {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
            }
        });
    }
    fnResendOtp() {
        this.second = 60;
        this.resenedOtpFlag = true;
        let form = {};
        form = {
            'mobile_no': this.phoneNo
        };
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.service.post('login/resend_otp', form).then((result) => {
            loader.dismiss();
            this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('otp_has_been_sent_on_your_mobile_no') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            }).present();
            let interval = setInterval(() => {
                if (this.second > 0) {
                    this.second--;
                }
                else {
                    clearInterval(interval);
                    this.resenedOtpFlag = false;
                }
            }, 1000);
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            else {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
            }
        });
    }
};
OtpVerifyPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-otp-verify',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/otp-verify/otp-verify.html"*/'<!--\n  Generated template for the OtpVerifyPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n        <ion-navbar>\n                <ion-title>{{language.fnGetLanguage(\'title_set_password\')}}</ion-title>\n                <ion-buttons end style="opacity: 0;" showWhen="android">\n                        <button ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n        <div>\n                <div *ngIf="!ForgottPAsswordIcon">\n                        <ion-row>\n                                <ion-col col-4></ion-col>\n                                <ion-col col-4 style="text-align: center">\n                                        <img src="assets/img_logo/signup.png">\n                                </ion-col>\n                                <ion-col col-4></ion-col>\n                        </ion-row>\n                        <ion-row>\n                                <ion-col col-12 style="text-align: center">\n                                        <h4 style="letter-spacing:3px">\n                                                {{language.fnGetLanguage(\'title_signup\')}}\n                                        </h4>\n                                        <h6 style="font-weight: 100; ">\n                                                {{language.fnGetLanguage(\'please_submit_otp_and_set_your_password\')}}.\n                                        </h6>\n                                </ion-col>\n                        </ion-row>\n                </div>\n                <ion-row *ngIf="ForgottPAsswordIcon">\n                        <ion-col col-12 style="text-align: center">\n                                <img src="assets/img_logo/login-icon.png">\n                                <h4 style="letter-spacing:3px">\n                                        {{language.fnGetLanguage(\'forgot_password\')}}\n                                </h4>\n                                <h6 style="font-weight: 100; ">\n                                        {{language.fnGetLanguage(\'fill_up_the_bellow_to_reset_your_password\')}}\n                                </h6>\n                        </ion-col>\n                </ion-row>\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'title_otp\')}}</span>\n                        <ion-input col-12 type="tel" placeholder="{{language.fnGetLanguage(\'enter_otp\')}}" class="inputarea"\n                                [(ngModel)]="otp"></ion-input>\n                </ion-row>\n\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'password\')}}</span>\n                        <ion-input col-12 type="password" placeholder="{{language.fnGetLanguage(\'enter_your_password\')}}"\n                                class="inputarea" [(ngModel)]="password"></ion-input>\n                </ion-row>\n\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'confirm_password\')}}</span>\n                        <ion-input col-12 type="password" placeholder="{{language.fnGetLanguage(\'re_enter_your_password\')}}"\n                                class="inputarea" [(ngModel)]="conirmPassword"></ion-input>\n                </ion-row>\n                <button ion-button large color="golden" block (click)="fnRegisterSecondStap()" class="mt20px">{{language.fnGetLanguage(\'next\')}}</button>\n                <ion-row style="margin-bottom: -15px;     padding: 5px;">\n                        <ion-col col-12 style="text-align: center;">\n                                {{language.fnGetLanguage(\'didn_t_get_otp\')}}?\n                        </ion-col>\n                </ion-row>\n                <button ion-button large color="golden" outline block (click)="fnResendOtp()" [disabled]="resenedOtpFlag"\n                        class="mt20px">\n                        {{language.fnGetLanguage(\'resend\')}}\n                        <span *ngIf="!resenedOtpFlag">&nbsp;{{language.fnGetLanguage(\'otp\')}}</span>\n                        <span *ngIf="resenedOtpFlag">&nbsp;{{language.fnGetLanguage(\'_after_\')}}&nbsp;{{ second }} s</span>\n                </button>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/otp-verify/otp-verify.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_7__providers_language_language__["a" /* LanguageProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_cart_cart__["a" /* CartProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__["a" /* CustomeProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], OtpVerifyPage);

//# sourceMappingURL=otp-verify.js.map

/***/ }),

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageViewerController; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__image_viewer__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__image_viewer_component__ = __webpack_require__(141);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};




var ImageViewerController = (function () {
    function ImageViewerController(_app, config, deepLinker) {
        this._app = _app;
        this.config = config;
        this.deepLinker = deepLinker;
    }
    /**
     * Create a image-viewer instance to display. See below for options.
     *
     * @param {object} imageElement The image element
     * @param {object} opts ImageViewer options
     */
    /**
       * Create a image-viewer instance to display. See below for options.
       *
       * @param {object} imageElement The image element
       * @param {object} opts ImageViewer options
       */
    ImageViewerController.prototype.create = /**
       * Create a image-viewer instance to display. See below for options.
       *
       * @param {object} imageElement The image element
       * @param {object} opts ImageViewer options
       */
    function (imageElement, opts) {
        if (opts === void 0) { opts = {}; }
        var image = imageElement.src;
        var position = imageElement.getBoundingClientRect();
        var options = __assign({ image: image, position: position }, opts);
        return new __WEBPACK_IMPORTED_MODULE_2__image_viewer__["a" /* ImageViewer */](this._app, __WEBPACK_IMPORTED_MODULE_3__image_viewer_component__["a" /* ImageViewerComponent */], options, this.config, this.deepLinker);
    };
    ImageViewerController.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    /** @nocollapse */
    ImageViewerController.ctorParameters = function () { return [
        { type: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */], },
        { type: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Config */], },
        { type: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* DeepLinker */], },
    ]; };
    return ImageViewerController;
}());

//# sourceMappingURL=image-viewer.controller.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageViewerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__image_viewer_src_animation__ = __webpack_require__(452);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__image_viewer_transition_gesture__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__image_viewer_zoom_gesture__ = __webpack_require__(454);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();






var ImageViewerComponent = (function (_super) {
    __extends(ImageViewerComponent, _super);
    function ImageViewerComponent(_gestureCtrl, elementRef, _nav, _zone, renderer, domCtrl, platform, _navParams, _config, _sanitizer) {
        var _this = _super.call(this, _config, elementRef, renderer) || this;
        _this._gestureCtrl = _gestureCtrl;
        _this.elementRef = elementRef;
        _this._nav = _nav;
        _this._zone = _zone;
        _this.renderer = renderer;
        _this.domCtrl = domCtrl;
        _this.platform = platform;
        _this._navParams = _navParams;
        _this._sanitizer = _sanitizer;
        var url = _navParams.get('image');
        _this.updateImageSrc(url);
        return _this;
    }
    ImageViewerComponent.prototype.updateImageSrc = function (src) {
        this.imageUrl = this._sanitizer.bypassSecurityTrustUrl(src);
    };
    ImageViewerComponent.prototype.updateImageSrcWithTransition = function (src) {
        var imageElement = this.image.nativeElement;
        var lowResImgWidth = imageElement.clientWidth;
        this.updateImageSrc(src);
        var animation = new __WEBPACK_IMPORTED_MODULE_3__image_viewer_src_animation__["a" /* ImageViewerSrcAnimation */](this.platform, this.image);
        imageElement.onload = function () { return animation.scaleFrom(lowResImgWidth); };
    };
    ImageViewerComponent.prototype.ngOnInit = function () {
        var _this = this;
        var navPop = function () { return _this._nav.pop(); };
        this.unregisterBackButton = this.platform.registerBackButtonAction(navPop);
        this._zone.runOutsideAngular(function () { return _this.dragGesture = new __WEBPACK_IMPORTED_MODULE_4__image_viewer_transition_gesture__["a" /* ImageViewerTransitionGesture */](_this.platform, _this, _this.domCtrl, _this.renderer, navPop); });
    };
    ImageViewerComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // imageContainer is set after the view has been initialized
        this._zone.runOutsideAngular(function () { return _this.pinchGesture = new __WEBPACK_IMPORTED_MODULE_5__image_viewer_zoom_gesture__["a" /* ImageViewerZoomGesture */](_this, _this.image, _this.platform, _this.renderer); });
    };
    ImageViewerComponent.prototype.ngOnDestroy = function () {
        this.dragGesture && this.dragGesture.destroy();
        this.pinchGesture && this.pinchGesture.destroy();
        this.unregisterBackButton();
    };
    ImageViewerComponent.prototype.bdClick = function () {
        if (this._navParams.get('enableBackdropDismiss')) {
            this._nav.pop();
        }
    };
    ImageViewerComponent.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"], args: [{
                    selector: 'image-viewer',
                    template: "\n\t\t<ion-header no-border>\n\t\t\t<ion-navbar>\n\t\t\t</ion-navbar>\n\t\t</ion-header>\n\n\t\t<ion-backdrop (click)=\"bdClick()\"></ion-backdrop>\n\n\t\t<div class=\"image-wrapper\">\n\t\t\t<div class=\"image\" >\n\t\t\t\t<img [src]=\"imageUrl\" tappable #image />\n\t\t\t</div>\n\t\t</div>\n\t",
                    styles: ['image-viewer.ion-page { position: absolute; top: 0; right: 0; bottom: 0; left: 0; display: flex; flex-direction: column; height: 100%; opacity: 1; } image-viewer.ion-page ion-navbar.toolbar .toolbar-background { background-color: transparent; } image-viewer.ion-page ion-navbar.toolbar.toolbar-ios { padding-top: calc(20px + 4px); } image-viewer.ion-page ion-navbar .bar-button-default { color: white; } image-viewer.ion-page .backdrop { will-change: opacity; } image-viewer.ion-page .image-wrapper { position: relative; z-index: 10; display: flex; overflow: hidden; flex-direction: column; pointer-events: none; margin-top: 56px; flex-grow: 1; justify-content: center; } image-viewer.ion-page .image { will-change: transform; } image-viewer.ion-page img { display: block; pointer-events: auto; max-width: 100%; max-height: 100vh; margin: 0 auto; } '],
                    encapsulation: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewEncapsulation"].None
                },] },
    ];
    /** @nocollapse */
    ImageViewerComponent.ctorParameters = function () { return [
        { type: __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["k" /* GestureController */], },
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], },
        { type: __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["t" /* NavController */], },
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["NgZone"], },
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Renderer"], },
        { type: __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["h" /* DomController */], },
        { type: __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["x" /* Platform */], },
        { type: __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["u" /* NavParams */], },
        { type: __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["e" /* Config */], },
        { type: __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["c" /* DomSanitizer */], },
    ]; };
    ImageViewerComponent.propDecorators = {
        "image": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"], args: ['image',] },],
    };
    return ImageViewerComponent;
}(__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* Ion */]));

//# sourceMappingURL=image-viewer.component.js.map

/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoteModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the NoteModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let NoteModalPage = class NoteModalPage {
    constructor(navCtrl, language, navParams, viewCtrl, platform) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.note = '';
        platform.ready().then(() => {
            platform.registerBackButtonAction(() => this.fnDismissModal());
        });
    }
    ionViewDidLoad() {
        this.item = this.navParams.get('data');
        this.isCart = this.navParams.get('isCart');
        if (this.navParams.get('data').note) {
            this.item = this.navParams.get('data');
            this.note = this.navParams.get('data').note;
        }
    }
    fnDismissModal() {
        let data = {
            addToCart: false
        };
        this.viewCtrl.dismiss(data);
    }
    fnAddtoCart() {
        let data = {
            addToCart: true,
            note: this.note
        };
        this.viewCtrl.dismiss(data);
    }
};
NoteModalPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-note-modal',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/note-modal/note-modal.html"*/'<ion-header>\n        <ion-navbar>\n                <ion-buttons start left (click)="fnDismissModal()">\n                        <button ion-button icon-only>\n                                <ion-icon name="arrow-back"></ion-icon>\n                        </button>\n                </ion-buttons>\n                <ion-title>{{language.fnGetLanguage(\'title_notes\')}}</ion-title>\n                <ion-buttons right end style="opacity: 0;" showWhen="android">\n                        <button ion-button icon-only>\n                                <ion-icon name="arrow-back"></ion-icon>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n</ion-header>\n<ion-content>\n        <div padding>\n                <div *ngIf="navParams.get(\'isCart\')">\n                        <h6 *ngIf=" item && item.item_name">\n                                {{language.fnGetLanguage(\'enter_your_notes\')}}\n                        </h6>\n                        <ion-textarea rows="8" style="border: 1px solid lightgray" [(ngModel)]="note" placeholder="{{language.fnGetLanguage(\'note\')}}..."></ion-textarea>\n                        <div col-12 style="text-align: right">\n                                <button ion-button col-6 color="golden" (click)="fnAddtoCart()">\n                                        <span> {{language.fnGetLanguage(\'title_ok\')}} </span>\n                                </button>\n                        </div>\n                </div>\n                <div *ngIf="!navParams.get(\'isCart\')">\n                        <h6 *ngIf="item && item.item_name">\n                                {{language.fnGetLanguage(\'notes_for\')}} {{ item.item_name }}\n                        </h6>\n                        <p *ngIf="item && item.note" style="min-height:100px;background: rgba(218, 216, 216, 0.32);;padding: 10px;">\n                                {{ item.note }}\n                        </p>\n                </div>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/note-modal/note-modal.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */]])
], NoteModalPage);

//# sourceMappingURL=note-modal.js.map

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__category_category__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





let SettingsPage = class SettingsPage {
    constructor(navCtrl, navParams, service, loader, alertCtrl, platform, menuCtrl, language, events, LanguageService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.loader = loader;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.menuCtrl = menuCtrl;
        this.language = language;
        this.events = events;
        this.LanguageService = LanguageService;
        this.languages = [];
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad SettingsPage');
        this.menuCtrl.enable(false);
        this.getLanguageList();
    }
    getLanguageList() {
        let loader = this.loader.create({ content: 'Please wait...' });
        loader.present();
        this.service.post('startup/get_lang', {}).then((result) => {
            loader.dismiss();
            this.languages = [];
            let languages = result.language;
            for (var k in languages) {
                this.languages.push({ key: k, value: languages[k] });
            }
            this.lang = this.languages[0];
            this.LanguageService.fnSetLanguageArray(this.languages);
            this.LanguageService.fnSetLangLabelArray(result.label);
        }, (error) => {
            loader.dismiss();
            if (error && error.msg && error.msg != '') {
                this.alertCtrl.create({
                    message: error.msg,
                    buttons: [
                        {
                            text: this.language.fnGetLanguage('title_ok'),
                            handler: data => {
                                this.platform.exitApp();
                            }
                        }
                    ]
                }).present();
            }
            else {
                this.alertCtrl.create({
                    message: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                    buttons: [
                        {
                            text: this.language.fnGetLanguage('title_ok'),
                            handler: data => {
                                this.platform.exitApp();
                            }
                        }
                    ]
                }).present();
            }
        });
    }
    fnSelectLanguage(lang) {
        this.lang = lang;
    }
    fnChoseLang() {
        this.LanguageService.fnSetAppLanguage(this.lang.key);
        this.events.publish('langSet');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__category_category__["a" /* CategoryPage */]);
    }
};
SettingsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-settings',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/settings/settings.html"*/'<ion-content>\n        <div class="set-item-pos">\n                <ion-row>\n                        <ion-col text-center>\n                                <img style="width: 200px" src="assets/imgs/logo.png">\n                        </ion-col>\n                </ion-row>\n                <ion-grid *ngIf="lang && lang.key">\n                        <ion-row>\n                                <ion-col text-center>\n                                        <h6>\n                                                Choose your language\n                                        </h6>\n                                </ion-col>\n                        </ion-row>\n                        <ion-row>\n                                <ion-col text-center>\n                                        <ng-template ngFor let-la [ngForOf]="languages">\n                                                <ion-badge (click)="fnSelectLanguage(la)" *ngIf="la.key == lang.key"\n                                                        class="langactive">\n                                                        <ion-icon name="checkmark" style="color: transparent">\n                                                        </ion-icon>{{la.value}}\n                                                        <ion-icon name="checkmark"></ion-icon>\n                                                </ion-badge>\n                                                <ion-badge (click)="fnSelectLanguage(la)" *ngIf="la.key != lang.key">\n                                                        <ion-icon name="checkmark" style="color: transparent">\n                                                        </ion-icon>{{la.value}}\n                                                        <ion-icon name="checkmark" style="color: transparent">\n                                                        </ion-icon>\n                                                </ion-badge>\n                                        </ng-template>\n\n                                </ion-col>\n                        </ion-row>\n                        <ion-row style="margin-top:15px; ">\n                                <ion-col text-center>\n                                        <button ion-button round\n                                                style="border-radius: 32px;padding: 0px 15px; height: 2em !important;"\n                                                (click)="fnChoseLang()" color="golden">\n                                                NEXT &nbsp; <ion-icon name="arrow-round-forward"></ion-icon>\n                                        </button>\n                                </ion-col>\n                        </ion-row>\n                </ion-grid>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/settings/settings.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__["a" /* CustomeProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
        __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */]])
], SettingsPage);

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 155:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 155;

/***/ }),

/***/ 19:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the CartProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
let CartProvider = class CartProvider {
    constructor(http) {
        this.http = http;
        this.appUpdate = 0;
        this.isPlaceOrder = false;
        this.cartData = [];
        console.log('Hello CartProvider Provider');
    }
    fnAddToCart(data) {
        let cart = JSON.parse(localStorage.getItem('cart'));
        if (cart) {
            cart.push(data);
            this.cartData = cart;
            localStorage.setItem('cart', JSON.stringify(this.cartData));
        }
        else {
            this.cartData = [];
            this.cartData.push(data);
            localStorage.setItem('cart', JSON.stringify(this.cartData));
        }
    }
    fnGetCart() {
        this.cartData = JSON.parse(localStorage.getItem('cart'));
        if (this.cartData) {
            for (let index = 0; index < this.cartData.length; index++) {
                if (this.cartData[index].item_name_lang && this.cartData[index].item_name_lang != '' && this.cartData[index].item_name_lang.length > 0) {
                    alert(this.cartData[index].item_name_lang.length);
                    alert(this.cartData[index].item_name_lang);
                    this.cartData[index].item_name_lang = JSON.parse(this.cartData[index].item_name_lang);
                }
            }
        }
        return this.cartData;
    }
    fnGetCountOfCart() {
        this.cartData = JSON.parse(localStorage.getItem('cart'));
        if (this.cartData) {
            return this.cartData.length;
        }
        return '0';
    }
    fnRemoveFromCart(index) {
        this.cartData.splice(index, 1);
        localStorage.setItem('cart', JSON.stringify(this.cartData));
    }
    fnChangeQty(index, qty) {
        this.cartData = JSON.parse(localStorage.getItem('cart'));
        this.cartData[index].qty = qty;
        localStorage.setItem('cart', JSON.stringify(this.cartData));
    }
    fnNoteChange(index, note) {
        this.cartData = JSON.parse(localStorage.getItem('cart'));
        this.cartData[index].note = note;
        localStorage.setItem('cart', JSON.stringify(this.cartData));
    }
};
CartProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
], CartProvider);

//# sourceMappingURL=cart.js.map

/***/ }),

/***/ 198:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 198;

/***/ }),

/***/ 23:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cart_cart__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__notofication_notofication__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__product_product__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__search_item_search_item__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_language_language__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_jquery__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the CategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let CategoryPage = class CategoryPage {
    constructor(navCtrl, navParams, event, platform, language, menuCtrl, alertCtrl, service, cart, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.event = event;
        this.platform = platform;
        this.language = language;
        this.menuCtrl = menuCtrl;
        this.alertCtrl = alertCtrl;
        this.service = service;
        this.cart = cart;
        this.loadingCtrl = loadingCtrl;
        this.categary = "all";
        this.appUpdate = 0;
        this.searchString = '';
        this.appLang = localStorage.getItem('appLang');
        this.lazyLoadding = localStorage.getItem('lazyLoadding');
        this.width = (window.innerWidth - 52) / 2;
    }
    ionViewDidLoad() {
        this.menuCtrl.enable(false);
        this.appLang = localStorage.getItem('appLang');
        this.platform.ready().then(() => {
            if (localStorage.getItem('notificationOpen') == '1') {
                this.fnGoToProductPageForNotification();
            }
            this.menuCtrl.enable(true);
            this.cartItem = this.cart.fnGetCountOfCart();
            this.event.subscribe('cart:itemAdded', () => {
                this.cartItem = this.cart.fnGetCountOfCart();
            });
            this.event.subscribe('notification:vieved', () => {
                this.notificationCount = 0;
            });
            let loader = this.loadingCtrl.create({
                content: 'Loading...'
            });
            this.event.subscribe('appLangChange', () => {
                this.appLang = this.language.fnGetSelectedAppLang();
            });
            loader.present();
            this.platform.ready().then(() => {
                let data = {};
                if (localStorage.getItem('notificationTime')) {
                    data['last_seen'] = localStorage.getItem('notificationTime');
                }
                this.service.post('category/get_category', data).then((result) => {
                    this.banners = result.banner;
                    this.categories = result.category;
                    console.log(this.categories);
                    console.log(this.banners);
                    this.bank_detail = result.bank_detail;
                    this.notificationCount = result.item_count;
                    this.event.publish('category:updated', this.categories);
                    this.event.publish('userNotloggedin', result.helpline_number);
                    localStorage.setItem('helpline', result.helpline_number);
                    localStorage.setItem('whatsapp_number', result.whatsapp_number);
                    loader.dismiss();
                }, (error) => {
                    loader.dismiss();
                    if (error.message) {
                        let alert = this.alertCtrl.create({
                            subTitle: error.message,
                            buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                    }
                    let alert = this.alertCtrl.create({
                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes') + '.',
                        buttons: [this.language.fnGetLanguage('title_ok')]
                    });
                    alert.present();
                });
            });
        });
    }
    fnOpenCart() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__cart_cart__["a" /* CartPage */]);
    }
    fnOpenNotificationPage() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__notofication_notofication__["a" /* NotoficationPage */]);
    }
    fnCategary(category) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], { 'data': category });
    }
    fnImageNotFound(event) {
        event.target.src = "http://www.sunerpsolutions.in/hr/img/category-block-thumb.jpg";
    }
    fnBannerNotFound(event) {
        event.target.src = " http://www.sunerpsolutions.in/hr/img/banner.jpg";
    }
    fnGoToProductPageForNotification() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__product_product__["a" /* ProductPage */], { 'data': this.navParams.get('data') });
    }
    fnSearch() {
        if (this.searchString && this.searchString != '') {
            let str = this.searchString;
            this.searchString = '';
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__search_item_search_item__["a" /* SearchItemPage */], { 'data': str });
        }
    }
    fnLoadImage(elementID) {
        let id = "#" + elementID;
        __WEBPACK_IMPORTED_MODULE_10_jquery__(id).css("background", "none");
        __WEBPACK_IMPORTED_MODULE_10_jquery__(id).css("background", "#fff");
    }
};
CategoryPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-category',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/category/category.html"*/'<!--\n  Generated template for the CategoryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n        <ion-navbar style="color: black;">\n                <!-- <ion-buttons color="dark" start style="position: absolute;" class="posAb"> -->\n                <button menuToggle ion-button icon-only start>\n                        <ion-icon color="dark" name="menu"></ion-icon>\n                </button>\n                <button menuToggle ion-button icon-only start disabled style="opacity: 0;" showWhen="android">\n                        <ion-icon color="dark" name="menu"></ion-icon>\n                </button>\n                <!-- </ion-buttons> -->\n                <ion-title>\n                        <img src="assets/imgs/logo-192x192.png" class="title_image">\n                </ion-title>\n                <ion-buttons end>\n                        <button (click)="fnOpenNotificationPage()" end ion-button icon-only id="notification-button"\n                                class="mr_min_15">\n                                <ion-icon color="dark" name="notifications"></ion-icon>\n                                <ion-badge small id="notifications-badge"\n                                        *ngIf="notificationCount && notificationCount>0">\n                                        {{ notificationCount }} </ion-badge>\n                        </button>\n                        <button (click)="fnOpenCart()" end ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}\n                                </ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n        <ion-toolbar class="cstTool">\n                <ion-item>\n                        <ion-input type="text" placeholder="{{language.fnGetLanguage(\'search_by_item_code\')}} "\n                                [(ngModel)]="searchString"></ion-input>\n                        <button ion-button color="golden" (click)="fnSearch()" item-end icon-only clear\n                                style="margin: 0">\n                                <ion-icon color="golden" name="search"></ion-icon>\n                        </button>\n                </ion-item>\n        </ion-toolbar>\n</ion-header>\n\n\n<ion-content [ngClass]="{\'mtm8\':!platform.is(\'ios\')}">\n        <div *ngIf="banners && banners.length>0 && categories && categories.length>0 && bank_detail"\n                style="padding-top: 5px;">\n                <div>\n                        <ion-slides pager="true" autoplay="3000" loop="true" lazy-load-images>\n                                <ion-slide  *ngFor="let banner of banners;let i = index" style="text-align: center" id ="baner_image_\'+{{i}}+\'_\'">\n                                        <img [attr.data-src]="banner.image_url" *ngIf="lazyLoadding"\n                                                (load)="fnLoadImage(\'baner_image_\'+i+\'_\')">\n                                        <img [src]="banner.image_url" *ngIf="!lazyLoadding">\n                                </ion-slide>\n                        </ion-slides>\n                </div>\n                <div class="category_block">\n                        <ion-row>\n                                <ion-col>\n                                        <p style="text-align: center;font-size: 20px; margin: 0;color: #5e361e">\n                                                {{language.fnGetLanguage(\'our_collection\')}}\n                                        </p>\n                                </ion-col>\n\n                        </ion-row>\n                        <ion-row style="padding-top: 0;padding-left: 5px;padding-right: 5px" lazy-load-images>\n                                <ion-col col-6 (click)="fnCategary(category)"\n                                        style="text-align: center;padding-left: 5px;padding-right: 5px"\n                                        *ngFor="let category of categories;let j = index" id ="category_image_\'+{{j}}+\'_\'">\n\n                                        <img [attr.data-src]="category.image_url" *ngIf="lazyLoadding"\n                                                 (load)="fnLoadImage(\'category_image_\'+j+\'_\')">\n                                        <img [src]="category.image_url" *ngIf="!lazyLoadding" >\n                                        <div class="category-name">\n                                                <div class="main-name" *ngIf="category.category_name_lang[appLang]">\n                                                        {{category.category_name_lang[appLang]}}\n                                                </div>\n                                                <div class="main-name" *ngIf="!category.category_name_lang[appLang]">\n                                                        {{ category.category_name }}\n                                                </div>\n                                                <div class="viewCollection">\n                                                        {{language.fnGetLanguage(\'view_collection\')}}\n\n                                                </div>\n                                        </div>\n                                </ion-col>\n                        </ion-row>\n                </div>\n                <div class="payment_detail">\n                        <h6>\n                                {{language.fnGetLanguage(\'payment_details\')}}\n                        </h6>\n                        <div class="underLine">\n                                &nbsp;\n                        </div>\n                        <p style="margin-top: 20px;line-height: 21px">\n                                {{language.fnGetLanguage(\'we_are_happy_to_accepts_your_payments_in_our_bank_account\')}}\n                        </p>\n                        <p style="text-align: center; line-height: 21px">\n                                {{ bank_detail.bank_name }}\n                                <br> {{language.fnGetLanguage(\'account_no\')}} : {{bank_detail.ac_no}}\n                                <br> {{language.fnGetLanguage(\'account_name\')}} : {{bank_detail.ac_name}}\n                                <br> {{language.fnGetLanguage(\'ifsc_code\')}} : {{bank_detail.ifsc}}\n                        </p>\n                </div>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/category/category.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_9__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_custome_custome__["a" /* CustomeProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_cart_cart__["a" /* CartProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */]])
], CategoryPage);

//# sourceMappingURL=category.js.map

/***/ }),

/***/ 244:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__otp_verify_otp_verify__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let SignupPage = class SignupPage {
    constructor(navCtrl, language, loadingCtrl, navParams, alertCtrl, service) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.service = service;
        this.password = '';
        this.conirmPassword = '';
        this.otp = '';
        this.firstStap = true;
        this.resenedOtpFlag = true;
        this.second = 60;
    }
    ionViewDidLoad() {
    }
    fnRegister() {
        var reg = /^\d+$/;
        if (!this.phoneNo || this.phoneNo == '' || this.phoneNo.length != 10 || !reg.test(this.phoneNo)) {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('enter_valid_mobile_no') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        let form = {
            'mobile_no': this.phoneNo
        };
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.service.post('login/create_account', form).then((result) => {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__otp_verify_otp_verify__["a" /* OtpVerifyPage */], { 'mobile_no': this.phoneNo });
            loader.dismiss();
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            else {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
            }
        });
    }
    fnRegisterSecondStap() {
        var reg = /^\d+$/;
        if (!this.otp || this.otp == '' || !reg.test(this.phoneNo)) {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('enter_valid_otp'),
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        if (!this.password || this.password == '' || this.password.length < 6) {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('enter_valid_password') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        if (this.password != this.conirmPassword) {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('password_and_confirm_password_does_not_match') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        let form = {
            'otp': this.otp,
            'password': this.password,
            'mobile_no': this.phoneNo
        };
        this.service.post('login/create_password', form).then((result) => {
            // localStorage.clear();
            localStorage.setItem('token', result.auth_token);
            this.service.token = result.auth_token;
            this.service.post('account/get_account_details', form).then((result) => {
                localStorage.setItem('customer_id', result.user.customer_id);
                localStorage.setItem('name', result.user.name);
                localStorage.setItem('mobile_no', result.user.mobile_no);
                localStorage.setItem('address_line_1', result.user.address_line_1);
                localStorage.setItem('address_line_2', result.user.address_line_2);
                localStorage.setItem('city', result.user.city);
                localStorage.setItem('state', result.user.state);
                localStorage.setItem('country', result.user.country);
                localStorage.setItem('email_id', result.user.email_id);
                localStorage.setItem('phone_no', result.user.phone_no);
                localStorage.setItem('photo', result.user.photo);
                localStorage.setItem('created_on', result.user.created_on);
                localStorage.setItem('status', result.user.status);
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__["a" /* UserDetailPage */]);
            }, (error) => {
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__["a" /* UserDetailPage */]);
            });
        }, (error) => {
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            else {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
            }
        });
    }
    fnResendOtp() {
        this.second = 60;
        this.resenedOtpFlag = true;
        let form = {
            'mobile_no': this.phoneNo
        };
        this.service.post('login/resend_otp', form).then((result) => {
            let interval = setInterval(() => {
                if (this.second > 0) {
                    this.second--;
                }
                else {
                    clearInterval(interval);
                    this.resenedOtpFlag = false;
                }
            }, 1000);
        }, (error) => {
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            else {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
            }
        });
    }
};
SignupPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-signup',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/signup/signup.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n        <ion-navbar>\n                <ion-title>{{language.fnGetLanguage(\'title_signup\')}}</ion-title>\n                <ion-buttons end style="opacity: 0;" showWhen="android">\n                        <button ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n        <div>\n                <ion-row>\n                        <ion-col col-4></ion-col>\n                        <ion-col col-4 style="text-align: center">\n                                <img src="assets/img_logo/signup.png">\n                        </ion-col>\n                        <ion-col col-4></ion-col>\n                </ion-row>\n                <ion-row>\n                        <ion-col col-12 style="text-align: center">\n                                <h4 style="letter-spacing:3px">\n                                        {{language.fnGetLanguage(\'title_signup\')}}\n                                </h4>\n                                <h6 style="font-weight: 100; ">\n                                        {{language.fnGetLanguage(\'please_signup_with_your_mobile_no\')}}.\n                                </h6>\n                        </ion-col>\n                </ion-row>\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'mobile_no\')}}.</span>\n                        <ion-input col-12 type="tel" placeholder="{{language.fnGetLanguage(\'enter_your_mobile_no\')}}." class="inputarea" [(ngModel)]="phoneNo"></ion-input>\n                </ion-row>\n                <button ion-button large color="golden" block (click)="fnRegister()" class="mt20px">{{language.fnGetLanguage(\'next\')}}</button>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/signup/signup.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_language_language__["a" /* LanguageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__["a" /* CustomeProvider */]])
], SignupPage);

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 247:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangeMobileNoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__verify_change_mobile_verify_change_mobile__ = __webpack_require__(248);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ChangeMobileNoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let ChangeMobileNoPage = class ChangeMobileNoPage {
    constructor(navCtrl, language, alertCtrl, navParams, loadingCtrl, service) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.service = service;
    }
    ionViewDidLoad() {
    }
    fnChangeMobileNo() {
        var reg = /^\d+$/;
        if (!this.phoneNo || this.phoneNo == '' || this.phoneNo.length != 10 || !reg.test(this.phoneNo)) {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('enter_valid_mobile_no') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        let form = {
            'mobile_no': this.phoneNo
        };
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.service.post('account/change_mobile', form).then((result) => {
            loader.dismiss();
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__verify_change_mobile_verify_change_mobile__["a" /* VerifyChangeMobilePage */], { 'data': this.phoneNo });
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
        });
    }
};
ChangeMobileNoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-change-mobile-no',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/change-mobile-no/change-mobile-no.html"*/'<!--\n  Generated template for the ChangeMobileNoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n        <ion-navbar>\n                <ion-title>{{language.fnGetLanguage(\'title_change_mobile_no\')}}</ion-title>\n                <ion-buttons end style="opacity: 0;" showWhen="android">\n                        <button ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n        <div>\n                <ion-row>\n                        <ion-col col-12 style="text-align: center">\n                                <img src="assets/img_logo/login-icon.png">\n                                <h4 style="letter-spacing:3px">\n                                        {{language.fnGetLanguage(\'change_mobile_no\')}}\n                                </h4>\n                                <h6 style="font-weight: 100; ">\n                                        {{language.fnGetLanguage(\'fill_up_the_bellow_to_change_registered_mobile_no\')}}\n\n                                </h6>\n                        </ion-col>\n                </ion-row>\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'new_mobile_no\')}}</span>\n                        <ion-input col-12 type="tel" placeholder="{{language.fnGetLanguage(\'enter_new_mobile_no\')}}" class="inputarea" [(ngModel)]="phoneNo"></ion-input>\n                </ion-row>\n                <button ion-button large color="golden" block class="mt20px" (click)="fnChangeMobileNo()">{{language.fnGetLanguage(\'next\')}}</button>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/change-mobile-no/change-mobile-no.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__["a" /* CustomeProvider */]])
], ChangeMobileNoPage);

//# sourceMappingURL=change-mobile-no.js.map

/***/ }),

/***/ 248:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VerifyChangeMobilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the VerifyChangeMobilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let VerifyChangeMobilePage = class VerifyChangeMobilePage {
    constructor(navCtrl, language, alertCtrl, service, loadingCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.alertCtrl = alertCtrl;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.second = 60;
        this.resenedOtpFlag = true;
    }
    ionViewDidLoad() {
        this.phoneNo = this.navParams.get('data');
        if (this.phoneNo) {
            let interval = setInterval(() => {
                if (this.second > 0) {
                    this.second--;
                }
                else {
                    this.resenedOtpFlag = false;
                    clearInterval(interval);
                }
            }, 1000);
        }
    }
    fnChangeMobileNo() {
        var reg = /^\d+$/;
        if (!this.otp || this.otp == '' || !reg.test(this.phoneNo)) {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('enter_valid_otp') + '.',
                buttons: [this.language.fnGetLanguage('title_OK')]
            });
            alert.present();
            return false;
        }
        let form = {
            'otp': this.otp,
            'mobile_no': this.phoneNo
        };
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.service.post('account/verify_change_mobile', form).then((result) => {
            loader.dismiss();
            localStorage.setItem('mobile_no', this.phoneNo);
            if (result.message) {
                let alert = this.alertCtrl.create({
                    subTitle: result.message,
                    buttons: [this.language.fnGetLanguage('title_OK')]
                });
                alert.present();
                alert.onDidDismiss(() => {
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__["a" /* UserDetailPage */], { 'data': true });
                });
            }
            else {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('your_mobile_no') + '.' + this.language.fnGetLanguage('has_been_changed') + '.',
                    buttons: [this.language.fnGetLanguage('title_OK')]
                });
                alert.present();
                alert.onDidDismiss(() => {
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__["a" /* UserDetailPage */], { 'data': true });
                });
            }
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_OK')]
                });
                alert.present();
                return false;
            }
            else {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                    buttons: [this.language.fnGetLanguage('title_OK')]
                });
                alert.present();
            }
        });
    }
    fnResendOtp() {
        this.second = 60;
        this.resenedOtpFlag = true;
        let form = {
            'mobile_no': this.phoneNo,
            'old_mobile_no': localStorage.getItem('mobile_no')
        };
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.service.post('login/resend_otp_for_change_number', form).then((result) => {
            loader.dismiss();
            this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('otp_has_been_sent_on_your_mobile_no') + '.',
                buttons: [this.language.fnGetLanguage('title_OK')]
            }).present();
            let interval = setInterval(() => {
                if (this.second > 0) {
                    this.second--;
                }
                else {
                    clearInterval(interval);
                    this.resenedOtpFlag = false;
                }
            }, 1000);
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_OK')]
                });
                alert.present();
                return false;
            }
            else {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                    buttons: [this.language.fnGetLanguage('title_OK')]
                });
                alert.present();
            }
        });
    }
    fnRoutetoUSerDeatilPage() {
        this.service.post('account/get_account_details', 'nodata')
            .then((result) => {
            localStorage.setItem('customer_id', result.account_details.customer_id);
            localStorage.setItem('name', result.account_details.name);
            localStorage.setItem('mobile_no', result.account_details.mobile_no);
            localStorage.setItem('address_line_1', result.account_details.address_line_1);
            localStorage.setItem('address_line_2', result.account_details.address_line_2);
            localStorage.setItem('city', result.account_details.city);
            localStorage.setItem('state', result.account_details.state);
            localStorage.setItem('country', result.account_details.country);
            localStorage.setItem('email_id', result.account_details.email_id);
            localStorage.setItem('phone_no', result.account_details.phone_no);
            localStorage.setItem('photo', result.account_details.photo);
            localStorage.setItem('created_on', result.account_details.created_on);
            localStorage.setItem('status', result.account_details.status);
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__["a" /* UserDetailPage */]);
        }, () => {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__user_detail_user_detail__["a" /* UserDetailPage */]);
        });
    }
};
VerifyChangeMobilePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-verify-change-mobile',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/verify-change-mobile/verify-change-mobile.html"*/'<!--\n  Generated template for the VerifyChangeMobilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n        <ion-navbar>\n                <ion-title>{{language.fnGetLanguage(\'title_change_mobile_no\')}}.</ion-title>\n                <ion-buttons end style="opacity: 0;" showWhen="android">\n                        <button ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n        <div>\n                <ion-row>\n                        <ion-col col-12 style="text-align: center">\n                                <img src="assets/img_logo/login-icon.png">\n                                <h4 style="letter-spacing:3px">\n                                        {{language.fnGetLanguage(\'title_change_mobile_no\')}}.\n                                </h4>\n                                <h6 style="font-weight: 100; ">\n                                        {{language.fnGetLanguage(\'please_submit_otp_to_change_your_registered_mobile_no\')}}.\n                                </h6>\n                        </ion-col>\n                </ion-row>\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'otp\')}}</span>\n                        <ion-input col-12 type="tel" placeholder="{{language.fnGetLanguage(\'enter_otp\')}}" class="inputarea"\n                                [(ngModel)]="otp"></ion-input>\n                </ion-row>\n                <button ion-button large color="golden" block (click)="fnChangeMobileNo()" class="mt20px">{{language.fnGetLanguage(\'change_number\')}}</button>\n                <ion-row style="margin-bottom: -15px;     padding: 5px;">\n                        <ion-col col-12 style="text-align: center;">\n                                {{language.fnGetLanguage(\'didnt_get_otp\')}}?\n                        </ion-col>\n                </ion-row>\n                <button ion-button large color="golden" outline block (click)="fnResendOtp()" [disabled]="resenedOtpFlag"\n                        class="mt20px">\n                        {{language.fnGetLanguage(\'resend\')}}\n                        <span *ngIf="!resenedOtpFlag">&nbsp;{{language.fnGetLanguage(\'otp\')}}</span>\n                        <span *ngIf="resenedOtpFlag">&nbsp;{{language.fnGetLanguage(\'after\')}}&nbsp;{{ second }} s</span>\n                </button>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/verify-change-mobile/verify-change-mobile.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__["a" /* CustomeProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */]])
], VerifyChangeMobilePage);

//# sourceMappingURL=verify-change-mobile.js.map

/***/ }),

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgottPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__otp_verify_otp_verify__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ForgottPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let ForgottPasswordPage = class ForgottPasswordPage {
    constructor(navCtrl, language, loadingCtrl, navParams, alertCtrl, service) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.service = service;
    }
    ionViewDidLoad() {
    }
    fnForgottPassword() {
        var reg = /^\d+$/;
        if (!this.phoneNo || this.phoneNo == '' || this.phoneNo.length != 10 || !reg.test(this.phoneNo)) {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('enter_valid_mobile_no') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        let form = {
            'mobile_no': this.phoneNo
        };
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.service.post('login/forget_password', form).then((result) => {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__otp_verify_otp_verify__["a" /* OtpVerifyPage */], { 'mobile_no': this.phoneNo });
            loader.dismiss();
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            else {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
            }
        });
    }
};
ForgottPasswordPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-forgott-password',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/forgott-password/forgott-password.html"*/'<!--\n  Generated template for the ForgottPasswordPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n        <ion-navbar>\n                <ion-title>{{language.fnGetLanguage(\'title_reset_password\')}}</ion-title>\n                <ion-buttons end style="opacity: 0;" showWhen="android">\n                        <button ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n        <div>\n                <ion-row>\n                        <ion-col col-12 style="text-align: center">\n                                <img src="assets/img_logo/login-icon.png">\n                                <h4 style="letter-spacing:3px">\n                                        {{language.fnGetLanguage(\'forget_password\')}}\n                                </h4>\n                                <h6 style="font-weight: 100; ">\n                                        {{language.fnGetLanguage(\'fill_up_the_bellow_to_reset_your_password\')}}\n                                </h6>\n                        </ion-col>\n                </ion-row>\n                <ion-row class="mt20px">\n                        <span class="spanPadding">\n                                        {{language.fnGetLanguage(\'mobile_no\')}}\n                        </span>\n                        <ion-input col-12 type="tel" placeholder="{{language.fnGetLanguage(\'enter_your_mobile_no\')}}" class="inputarea" [(ngModel)]="phoneNo"></ion-input>\n                </ion-row>\n                <button ion-button large color="golden" block (click)="fnForgottPassword()" class="mt20px">{{language.fnGetLanguage(\'next\')}}</button>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/forgott-password/forgott-password.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__providers_custome_custome__["a" /* CustomeProvider */]])
], ForgottPasswordPage);

//# sourceMappingURL=forgott-password.js.map

/***/ }),

/***/ 250:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderSuccessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__category_category__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the OrderSuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let OrderSuccessPage = class OrderSuccessPage {
    constructor(navCtrl, language, menu, navParams) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.menu = menu;
        this.navParams = navParams;
    }
    ionViewDidLoad() {
        if (!this.navCtrl.canGoBack()) {
            this.menu.enable(true);
        }
        console.log('ionViewDidLoad OrderSuccessPage');
    }
    fnGotToCategoryPage() {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__category_category__["a" /* CategoryPage */]);
    }
};
OrderSuccessPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-order-success',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/order-success/order-success.html"*/'<!--\n  Generated template for the OrderSuccessPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n        <ion-navbar>\n                <button menuToggle ion-button icon-only start>\n                        <ion-icon color="dark" name="menu"></ion-icon>\n                </button>\n                <ion-title>{{language.fnGetLanguage(\'title_success\')}}</ion-title>\n                <ion-buttons end style="opacity: 0;" showWhen="android">\n                        <button ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n        <div class="outer">\n                <div class="middle">\n                        <div class="inner">\n                                <img src="assets/imgs/ok.png">\n                                <p>{{language.fnGetLanguage(\'your_order_is_successfully_placed\')}}</p>\n                                <button ion-button clear color="golden" (click)="fnGotToCategoryPage()">\n                                        {{language.fnGetLanguage(\'continue_shopping\')}} &nbsp;\n                                        <ion-icon name="arrow-round-forward"></ion-icon>\n                                </button>\n                        </div>\n                </div>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/order-success/order-success.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */]])
], OrderSuccessPage);

//# sourceMappingURL=order-success.js.map

/***/ }),

/***/ 251:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageViewerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_local_notifications__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_fabric__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_fabric___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_fabric__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_base64_to_gallery__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_photo_library__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_android_permissions__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_language_language__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__Library_ionic_img_viewer_dist_es2015_ionic_img_viewer__ = __webpack_require__(351);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the ImageViewerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let ImageViewerPage = class ImageViewerPage {
    constructor(navCtrl, localNotifications, elementRef, language, viewCtrl, navParams, loadingCtrl, modal, androidPermissions, toastCtrl, imageViewerCtrl, platform, service, base64ToGallery, el, photoLibrary, alertCtrl, renderer) {
        this.navCtrl = navCtrl;
        this.localNotifications = localNotifications;
        this.elementRef = elementRef;
        this.language = language;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.modal = modal;
        this.androidPermissions = androidPermissions;
        this.toastCtrl = toastCtrl;
        this.imageViewerCtrl = imageViewerCtrl;
        this.platform = platform;
        this.service = service;
        this.base64ToGallery = base64ToGallery;
        this.el = el;
        this.photoLibrary = photoLibrary;
        this.alertCtrl = alertCtrl;
        this.renderer = renderer;
        this.selectedSlide = 0;
        this.value = 0;
        this.height = window.innerHeight - 120;
    }
    ionViewDidLoad() {
        this.item = this.navParams.get('data');
        this.data = this.navParams.get('data').gallary;
        this.loader = this.loadingCtrl.create({
            content: 'Please wait...'
        });
    }
    fnCloseImageViewer() {
        this.viewCtrl.dismiss().then(() => {
        }, () => {
        });
    }
    slideChanged() {
        let index = this.slides.getActiveIndex();
        if (index < this.data.length) {
            this.selectedSlide = index;
        }
    }
    fnchangeSlide(i) {
        this.slides.slideTo(i);
    }
    fnDownloadImage(img, id) {
        this.localNotifications.schedule({
            id: 1,
            title: this.language.fnGetLanguage('image_is_downloading'),
            progressBar: { value: this.value }
        });
        if (this.platform.is('ios')) {
            this.loader = this.loadingCtrl.create({
                content: 'Please wait...'
            });
            this.loader.present();
            this.download(img, this.item.item_code);
        }
        else {
            this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then((result) => {
                if (result.hasPermission) {
                    this.loader = this.loadingCtrl.create({
                        content: 'Please wait...'
                    });
                    this.loader.present();
                    this.getMeta(img, id);
                }
                else {
                    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE]).then((r) => {
                        if (r.hasPermission) {
                            this.loader = this.loadingCtrl.create({
                                content: 'Please wait...'
                            });
                            this.loader.present();
                            this.getMeta(img, id);
                        }
                        else {
                            this.alertCtrl.create({ 'message': this.language.fnGetLanguage('please_give_permission_to_save_file') }).present();
                        }
                    }, (error) => {
                        this.alertCtrl.create({ 'message': this.language.fnGetLanguage('error_while_saving_a_file') + '.' }).present();
                    });
                }
            }, (error) => {
                this.alertCtrl.create({ 'message': this.language.fnGetLanguage('error_while_saving_a_file') + '.' }).present();
            });
        }
    }
    download(url, text) {
        this.photoLibrary.requestAuthorization({
            read: true,
            write: true
        }).then((res) => {
            let mainUrl = this.service.baseUrl + 'item/image_addtext?image_path=' + url + '&item_name=' + text;
            this.photoLibrary.saveImage(mainUrl, 'HRSALES').then((result) => {
                this.localNotifications.clear(1);
                this.localNotifications.schedule({
                    id: 2,
                    title: this.item.item_name + this.language.fnGetLanguage("image_downloded")
                });
                this.toastCtrl.create({
                    message: this.language.fnGetLanguage('image_is_saved_successfully'),
                    duration: 3000,
                }).present();
                this.loader.dismiss();
            }, (error) => {
                this.loader.dismiss();
                this.toastCtrl.create({
                    message: this.language.fnGetLanguage('image_can_not_be_downloaded') + '.',
                    duration: 3000,
                }).present();
                this.localNotifications.clear(1);
                this.localNotifications.schedule({
                    id: 2,
                    title: this.language.fnGetLanguage('error_occured_during_image_saving')
                });
            });
        }, (err) => {
            this.loader.dismiss();
            this.alertCtrl.create({ message: err }).present();
        });
    }
    fnOpenImageZoomer(cmp, i) {
        let imageViewer = this.imageViewerCtrl.create(this.components._results[i].nativeElement);
        imageViewer.present();
    }
    getMeta(url, id) {
        this.value = 0;
        var img = document.getElementById('gallary' + id);
        this.clientWidth = img.clientWidth;
        this.clientHeight = img.clientHeight;
        this.canvas = new fabric.Canvas('canvas', {
            hoverCursor: 'pointer',
            selection: true,
            selectionBorderColor: 'blue',
        });
        fabric.Image.fromURL(url, (image) => {
            this.clientWidth = image.width;
            this.clientHeight = image.height;
            this.canvas.setHeight(this.clientHeight);
            this.canvas.setWidth(this.clientWidth);
            this.canvas.add(image);
            let textString = this.item.item_code;
            let text = new fabric.Text(textString, {
                fontFamily: 'helvetica',
                fill: '#000000',
                right: 100,
                top: 100,
                fontSize: 80,
                left: 100,
                fontWeight: 'bold',
                objectType: 'text',
                hasRotatingPoint: false
            });
            this.canvas.add(text);
            var imageDownload = new Image();
            imageDownload.crossOrigin = 'anonymous';
            imageDownload.src = this.canvas.toDataURL('jpeg', 1.0);
            this.downloadAndroid(imageDownload.src);
        });
    }
    downloadAndroid(url) {
        this.base64ToGallery.base64ToGallery(url)
            .then((res) => {
            this.localNotifications.clear(1);
            this.localNotifications.schedule({
                id: 2,
                title: this.item.item_name + ' ' + this.language.fnGetLanguage('image_downloded')
            });
            this.toastCtrl.create({
                message: this.language.fnGetLanguage('image_is_saved_successfully'),
                duration: 3000,
            }).present();
            this.value = 100;
            this.canvas = null;
            this.loader.dismiss();
        }, (err) => {
            this.localNotifications.clear(1);
            this.localNotifications.schedule({
                id: 2,
                title: this.language.fnGetLanguage('error_saving_image_to_gallery') + ',' + err
            });
            this.value = 100;
            this.value = 100;
            this.loader.dismiss();
            this.toastCtrl.create({
                message: this.language.fnGetLanguage('image_can_not_be_download') + '.',
                duration: 3000,
            }).present();
        });
    }
};
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* Slides */])
], ImageViewerPage.prototype, "slides", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChildren"])('cmp'),
    __metadata("design:type", Object)
], ImageViewerPage.prototype, "components", void 0);
ImageViewerPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-image-viewer',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/image-viewer/image-viewer.html"*/'<!--\n  Generated template for the ImageViewerPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n\n<ion-content no-bounce disable-scroll class="no-scroll">\n\n        <ion-slides style="position: absolute; top: 50px;" class="top0" (ionSlideDidChange)="slideChanged()"  speed="300" \n                pager="false" [style.height.px]="height">\n                <ion-slide *ngFor="let img of data; let i = index" >\n                        <img [src]="img.o" id="gallary{{i}}" imageViewer="{{ img.o }}" style="z-index: 9998;" #cmp >\n                        <div (click)="fnDownloadImage(img.o,i)" class="closeBtn" style="z-index: 9999;" >\n                                <ion-icon name="cloud-download"></ion-icon>\n                        </div>\n\n                        <div (click)="fnOpenImageZoomer(cmp,i)" class="closeBtn top50" style="z-index: 9999;">\n                                <ion-icon name="search"></ion-icon>\n                        </div>\n                </ion-slide>\n        </ion-slides>\n\n        <ion-row class="thumbnil" style="z-index: 9996;">\n                <div style="width: 100%;">\n                        <img [src]="img.o" *ngFor="let img of data;let i = index" style="margin: 5px;cursor: pointer;" [ngClass]="{\'border\':i == selectedSlide}"\n                                 (click)="fnchangeSlide(i)">\n                </div>\n        </ion-row>\n        <div (click)="fnCloseImageViewer()" class="closeBtn" style="z-index: 9999;">X</div>\n        <canvas id="canvas" style="display: none"></canvas>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/image-viewer/image-viewer.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_local_notifications__["a" /* LocalNotifications */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
        __WEBPACK_IMPORTED_MODULE_8__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_native_android_permissions__["a" /* AndroidPermissions */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_9__Library_ionic_img_viewer_dist_es2015_ionic_img_viewer__["a" /* ImageViewerController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_5__providers_custome_custome__["a" /* CustomeProvider */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_base64_to_gallery__["a" /* Base64ToGallery */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_photo_library__["a" /* PhotoLibrary */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]])
], ImageViewerPage);

//# sourceMappingURL=image-viewer.js.map

/***/ }),

/***/ 256:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IonicImageViewerModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__image_viewer_directive__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__image_viewer_component__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__image_viewer_controller__ = __webpack_require__(136);





var IonicImageViewerModule = (function () {
    function IonicImageViewerModule() {
    }
    IonicImageViewerModule.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"], args: [{
                    imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* IonicModule */]],
                    declarations: [
                        __WEBPACK_IMPORTED_MODULE_3__image_viewer_component__["a" /* ImageViewerComponent */],
                        __WEBPACK_IMPORTED_MODULE_2__image_viewer_directive__["a" /* ImageViewerDirective */]
                    ],
                    providers: [__WEBPACK_IMPORTED_MODULE_4__image_viewer_controller__["a" /* ImageViewerController */]],
                    exports: [__WEBPACK_IMPORTED_MODULE_2__image_viewer_directive__["a" /* ImageViewerDirective */]],
                    entryComponents: [__WEBPACK_IMPORTED_MODULE_3__image_viewer_component__["a" /* ImageViewerComponent */]]
                },] },
    ];
    /** @nocollapse */
    IonicImageViewerModule.ctorParameters = function () { return []; };
    return IonicImageViewerModule;
}());

//# sourceMappingURL=module.js.map

/***/ }),

/***/ 257:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageViewerDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__image_viewer_controller__ = __webpack_require__(136);


var ImageViewerDirective = (function () {
    function ImageViewerDirective(_el, imageViewerCtrl) {
        this._el = _el;
        this.imageViewerCtrl = imageViewerCtrl;
        this.close = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    ImageViewerDirective.prototype.onClick = function (event) {
        var _this = this;
        event.stopPropagation();
        var element = this._el.nativeElement;
        var onCloseCallback = function () { return _this.close.emit(); };
        var imageViewer = this.imageViewerCtrl.create(element, { fullResImage: this.src, onCloseCallback: onCloseCallback });
        imageViewer.present();
    };
    ImageViewerDirective.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"], args: [{
                    selector: '[imageViewer]'
                },] },
    ];
    /** @nocollapse */
    ImageViewerDirective.ctorParameters = function () { return [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"], },
        { type: __WEBPACK_IMPORTED_MODULE_1__image_viewer_controller__["a" /* ImageViewerController */], },
    ]; };
    ImageViewerDirective.propDecorators = {
        "src": [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"], args: ['imageViewer',] },],
        "close": [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
        "onClick": [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"], args: ['click', ['$event'],] },],
    };
    return ImageViewerDirective;
}());

//# sourceMappingURL=image-viewer.directive.js.map

/***/ }),

/***/ 258:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageViewer; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular_navigation_overlay_proxy__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__image_viewer_impl__ = __webpack_require__(352);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();


var ImageViewer = (function (_super) {
    __extends(ImageViewer, _super);
    function ImageViewer(app, component, opts, config, deepLinker) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, app, component, config, deepLinker) || this;
        _this.opts = opts;
        return _this;
    }
    ImageViewer.prototype.getImplementation = function () {
        return new __WEBPACK_IMPORTED_MODULE_1__image_viewer_impl__["a" /* ImageViewerImpl */](this._app, this._component, this.opts, this._config);
    };
    return ImageViewer;
}(__WEBPACK_IMPORTED_MODULE_0_ionic_angular_navigation_overlay_proxy__["a" /* OverlayProxy */]));

//# sourceMappingURL=image-viewer.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotoficationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cart_cart__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__product_product__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__category_category__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_badge__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_language_language__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_jquery__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};










/**
 * Generated class for the NotoficationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let NotoficationPage = class NotoficationPage {
    constructor(navCtrl, language, badge, event, navParams, cart, loadingCtrl, servics, alertCtrl) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.badge = badge;
        this.event = event;
        this.navParams = navParams;
        this.cart = cart;
        this.loadingCtrl = loadingCtrl;
        this.servics = servics;
        this.alertCtrl = alertCtrl;
        this.width = (window.innerWidth - 52) / 2;
        this.lazyLoadding = localStorage.getItem('lazyLoadding');
        this.unseen = false;
        this.seenData = [];
        this.newData = [];
        this.appLang = localStorage.getItem('appLang');
    }
    ionViewDidLoad() {
        this.event.unsubscribe('cart:itemAdded');
        this.event.publish('notification:vieved');
        this.cartItem = this.cart.fnGetCountOfCart();
        this.event.subscribe('cart:itemAdded', () => {
            this.cartItem = this.cart.fnGetCountOfCart();
        });
        this.event.subscribe('appLangChange', () => {
            this.appLang = this.language.fnGetSelectedAppLang();
        });
        let loader = this.loadingCtrl.create({
            content: 'Loading...'
        });
        loader.present();
        this.servics.post('notification/get_item_list', {}).then((result) => {
            loader.dismiss();
            let d = result.items;
            this.lastTime = parseInt(localStorage.getItem('notificationTime'));
            var j = 0;
            var i = 0;
            if (result.items) {
                for (let index = 0; index < d.length; index++) {
                    if (parseInt(d[index].created_on) > parseInt(this.lastTime)) {
                        this.newData.splice(j, 0, d[index]);
                        j++;
                    }
                    else {
                        this.seenData.splice(i, 0, d[index]);
                        i++;
                    }
                }
                this.items = result.items;
            }
            localStorage.removeItem('notificationTime');
            localStorage.setItem('notificationTime', result.time);
        }, (error) => {
            loader.dismiss();
            if (error.time) {
                localStorage.setItem('notificationTime', error.time);
            }
            let alert = this.alertCtrl.create({
                subTitle: error.msg,
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
        });
    }
    fnGotoHomePage() {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__category_category__["a" /* CategoryPage */]);
    }
    fnOpenCart() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__cart_cart__["a" /* CartPage */]);
    }
    fnOpenProductPage(item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__product_product__["a" /* ProductPage */], { 'data': item });
    }
    fnImageNotFound(event) {
        event.target.src = "assets/img_logo/default-product.jpg";
    }
    clearBadges() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.badge.clear();
            }
            catch (e) {
            }
        });
    }
    fnLoadImage(elementID) {
        let id = "#" + elementID;
        __WEBPACK_IMPORTED_MODULE_9_jquery__(id).css("background", "none");
        __WEBPACK_IMPORTED_MODULE_9_jquery__(id).css("background", "#fff");
    }
};
NotoficationPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-notofication',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/notofication/notofication.html"*/'<!--\n  Generated template for the NotoficationPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n        <ion-navbar>\n                <button (click)="fnGotoHomePage()" ion-button icon-only clear *ngIf="!navCtrl.canGoBack()">\n                        <ion-icon color="dark" name="arrow-back"></ion-icon>\n                </button>\n                <ion-title>{{language.fnGetLanguage(\'title_hot_collections\')}}</ion-title>\n                <ion-buttons end>\n                        <button (click)="fnOpenCart()" end ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n        <div *ngIf="items && items.length > 0">\n                <div class="new_item" *ngIf="newData && newData.length > 0">\n                        {{language.fnGetLanguage(\'unseen\')}}\n                </div>\n                <div padding *ngIf="newData && newData.length > 0">\n                        <ion-row *ngIf="newData && newData.length > 0" lazy-load-images>\n\n                                <ion-col *ngFor="let item of newData" col-6 (click)="fnOpenProductPage(item)" class="cst_item">\n                                        <div class="setBackground" [style.width.px]="width" [style.minHeight.px]="width*4/3">\n                                                <img [attr.data-src]="item.item_image" (load)="fnLoadImage(\'prod_image_\'+i+\'_\'+c.item_id)" *ngIf="!lazyLoadding">\n                                                <img [src]="item.item_image" (load)="fnLoadImage(\'prod_image_\'+i+\'_\'+c.item_id)" *ngIf="!lazyLoadding">\n                                       \n                                        </div>\n                                        <div class="special_tag" *ngIf="item.is_special==1">\n                                                {{language.fnGetLanguage(\'title_special\')}}\n                                        </div>\n                                        <div style="text-align: center">\n                                                <p style="line-height: 20px">\n                                                        <span style="font-weight: 800">\n\n                                                                <span style="color: gray">\n                                                                        {{language.fnGetLanguage(\'code\')}} : {{\n                                                                        item.item_code }}\n                                                                </span>\n                                                                <br><span *ngIf="item.item_name_lang[appLang]">\n                                                                                {{item.item_name_lang[appLang]}}\n                                                                        </span>\n                                                                        <span *ngIf="!item.item_name_lang[appLang]">\n                                                                                {{ item.item_name }}\n                                                                        </span>\n                                                                <br>\n                                                        </span>\n                                                        <span style="color: #6b4d0f; font-weight:bold; line-height: 20px;padding-top: 20px">₹{{\n                                                                item.sale_price.toLocaleString(\'en-IN\') }} </span>\n\n                                                </p>\n                                        </div>\n                                </ion-col>\n                        </ion-row>\n                </div>\n                <div class="new_item" *ngIf="seenData && seenData.length > 0">\n                        Seen\n                </div>\n                <div padding *ngIf="seenData && seenData.length > 0">\n                        <ion-row *ngIf="seenData && seenData.length > 0" lazy-load-images>\n                                <ion-col *ngFor="let item of seenData" col-6 (click)="fnOpenProductPage(item)" class="cst_item">\n                                        <div class="setBackground" [style.width.px]="width" [style.minHeight.px]="width*4/3">\n                                                <img [attr.data-src]="item.item_image" (load)="fnLoadImage(\'prod_image_\'+i+\'_\'+c.item_id)" *ngIf="!lazyLoadding">\n                                                <img [src]="item.item_image" (load)="fnLoadImage(\'prod_image_\'+i+\'_\'+c.item_id)" *ngIf="!lazyLoadding">\n                                       \n                                        </div>\n                                        <div class="special_tag" *ngIf="item.is_special==1">\n                                                {{language.fnGetLanguage(\'title_special\')}}\n                                        </div>\n                                        <div style="text-align: center">\n                                                <p style="line-height: 20px">\n                                                        <span style="font-weight: 800">\n\n                                                                <span style="color: gray">\n                                                                        {{language.fnGetLanguage(\'code\')}} : {{\n                                                                        item.item_code }}\n                                                                </span>\n                                                                <br> <span *ngIf="item.item_name_lang[appLang]">\n                                                                {{item.item_name_lang[appLang]}}\n                                                        </span>\n                                                        <span *ngIf="!item.item_name_lang[appLang]">\n                                                                {{ item.item_name }}\n                                                        </span>\n                                                                <br>\n                                                        </span>\n                                                        <span style="color: #6b4d0f; font-weight:bold; line-height: 20px;padding-top: 20px">₹{{\n                                                                item.sale_price.toLocaleString(\'en-IN\') }} </span>\n                                                </p>\n                                        </div>\n                                </ion-col>\n                        </ion-row>\n                </div>\n        </div>\n\n        <div *ngIf="!items || items.length == 0" style="margin: 10%; text-align: center; position: absolute; top: 30%"\n                class="noitem">\n                <img src="assets/img_logo/empty-shopping-cart.png">\n                <br>\n                <h6>\n                        {{language.fnGetLanguage(\'no_any_record_found\')}}.\n                </h6>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/notofication/notofication.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_8__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_native_badge__["a" /* Badge */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_5__providers_cart_cart__["a" /* CartProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__["a" /* CustomeProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], NotoficationPage);

//# sourceMappingURL=notofication.js.map

/***/ }),

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchItemPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__category_category__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__product_product__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__cart_cart__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jquery__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the SearchItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let SearchItemPage = class SearchItemPage {
    constructor(navCtrl, language, loadingCtrl, navParams, event, alertCtrl, service, cart) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.event = event;
        this.alertCtrl = alertCtrl;
        this.service = service;
        this.cart = cart;
        this.showFilter = false;
        this.Special = false;
        this.itemCode = '';
        this.size = '';
        this.showHeaderFilterBar = true;
        this.searchString = '';
        this.width = (window.innerWidth - 52) / 2;
        this.appLang = localStorage.getItem('appLang');
        this.items = [];
        this.selectedSub = 'all';
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad SearchItemPage');
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.cartItem = this.cart.fnGetCountOfCart();
        this.event.subscribe('cart:itemAdded', () => {
            this.cartItem = this.cart.fnGetCountOfCart();
        });
        this.event.subscribe('appLangChange', () => {
            this.appLang = this.language.fnGetSelectedAppLang();
        });
        this.searchString = this.navParams.get('data');
        let form = {
            q: this.searchString
        };
        this.service.post('item/get_search_items', form)
            .then((result) => {
            this.items = result.item_list;
            this.subCategorys = result.sub_category;
            this.pagination = result.pagination;
            this.scrollToTop();
            loader.dismiss();
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
        });
    }
    scrollToTop() {
        this.content.scrollToTop();
    }
    fnCategary() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__category_category__["a" /* CategoryPage */]);
    }
    fnSearch() {
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        let form = {
            q: this.searchString
        };
        this.items = [];
        this.pagination = null;
        this.service.post('item/get_search_items', form)
            .then((result) => {
            this.items = result.item_list;
            this.pagination = result.pagination;
            this.scrollToTop();
            loader.dismiss();
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
        });
    }
    fnOpenCart() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__cart_cart__["a" /* CartPage */]);
    }
    fnOpenProductPage(item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__product_product__["a" /* ProductPage */], { 'data': item });
    }
    fnLoadMoreData(e) {
        let form = {};
        form['q'] = this.searchString;
        form['page'] = this.pagination.next;
        this.service.post('item/get_search_items', form)
            .then((result) => {
            this.items = this.items.concat(result.item_list);
            this.pagination = result.pagination;
            e.complete();
        }, (error) => {
            e.complete();
        });
    }
    fnImageNotFound(event) {
        event.target.src = "assets/img_logo/default-product.jpg";
    }
    onScroll(e) {
        if (e.directionY === 'up') {
            this.showHeaderFilterBar = true;
            __WEBPACK_IMPORTED_MODULE_7_jquery__('.selector').fadeIn();
            if (this.showFilter)
                __WEBPACK_IMPORTED_MODULE_7_jquery__('.filters').fadeIn();
        }
        else {
            if (e.deltaY < 80 && e.scrollTop < 80) {
                this.showHeaderFilterBar = true;
                __WEBPACK_IMPORTED_MODULE_7_jquery__('.selector').fadeIn();
                if (this.showFilter) {
                    __WEBPACK_IMPORTED_MODULE_7_jquery__('.filters').fadeIn();
                }
            }
            else {
                this.showHeaderFilterBar = false;
                __WEBPACK_IMPORTED_MODULE_7_jquery__('.selector').fadeOut();
                __WEBPACK_IMPORTED_MODULE_7_jquery__('.filters').fadeOut();
            }
        }
    }
};
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Content */])
], SearchItemPage.prototype, "content", void 0);
SearchItemPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-search-item',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/search-item/search-item.html"*/'<!--\n  Generated template for the SearchItemPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n        <ion-navbar>\n                <ion-title>\n                        <!-- <img src="assets/imgs/logo-192x192.png" class="title_image"> -->\n                        {{language.fnGetLanguage(\'title_search_item\')}}\n                       \n                </ion-title>\n                <ion-buttons end style="opacity: 0;" showWhen="android">\n                        <button ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n        <ion-toolbar class="cstTool">\n                <ion-item>\n                        <ion-input type="text" placeholder="{{language.fnGetLanguage(\'search_by_item_code\')}}" [(ngModel)]="searchString"></ion-input>\n                        <button ion-button color="golden" (click)="fnSearch()" item-right clear icon-only style="margin: 0">\n                                <ion-icon color="golden" name="search"></ion-icon>\n                        </button>\n                </ion-item>\n        </ion-toolbar>\n</ion-header>\n\n<ion-content class="asd" (ionScroll)="onScroll($event)" overflow-scroll="false">\n        <div padding style="margin-top:55px ">\n                <div *ngIf="items && items.length > 0">\n                        <ion-row lazy-load-images>\n                                <ion-col *ngFor="let item of items" col-6 (click)="fnOpenProductPage(item)">\n                                        <div class="setBackground" [style.width.px]="width" [style.minHeight.px]="width*4/3" id="prod_image_{{i}}_{{item.item_id}}">\n                                                <img [attr.data-src]="item.item_image" (load)="fnLoadImage(\'prod_image_\'+i+\'_\'+item.item_id)" *ngIf="lazyLoadding">\n                                                <img [src]="item.item_image" *ngIf="!lazyLoadding">\n                                                \n                                        </div>\n                                        <div class="special_tag" *ngIf="item.in_stock==\'1\'&&item.is_special==1">\n                                                {{ language.fnGetLanguage(\'title_special\') }}\n                                        </div>\n                                        <div class="special_tag" style="background: red !important" *ngIf="item.in_stock==\'0\'">\n\n                                                {{ language.fnGetLanguage(\'title_out_of_stock\') }}\n                                        </div>\n                                        <div style="text-align: center">\n                                                <p style="line-height: 20px">\n                                                        <span style="font-weight: 800">\n\n                                                                <span style="color: gray">\n                                                                        {{language.fnGetLanguage(\'code\')}} : {{\n                                                                        item.item_code }}\n                                                                </span>\n                                                                <br> <span *ngIf="item.item_name_lang[appLang]">\n                                                                                {{item.item_name_lang[appLang]}}\n                                                                        </span>\n                                                                        <span *ngIf="!item.item_name_lang[appLang]">\n                                                                                {{ item.item_name }}\n                                                                        </span>\n                                                                <br>\n                                                        </span>\n                                                        <span style="color: #6b4d0f; font-weight:bold; line-height: 20px;padding-top: 20px">₹{{\n                                                                item.sale_price.toLocaleString(\'en-IN\') }}\n                                                        </span>\n\n                                                </p>\n                                        </div>\n                                </ion-col>\n                        </ion-row>\n                </div>\n                <div *ngIf="pagination && pagination.next">\n                        <ion-infinite-scroll (ionInfinite)="fnLoadMoreData($event)">\n                                <ion-infinite-scroll-content></ion-infinite-scroll-content>\n                        </ion-infinite-scroll>\n                </div>\n                <div *ngIf="!items || items.length == 0" style="margin: 10%; text-align: center; position: absolute; top: 30%"\n                        class="noitem">\n                        <img src="assets/img_logo/empty-shopping-cart.png">\n                        <br>\n                        <h6>\n                                {{language.fnGetLanguage(\'no_any_record_found\')}}.\n                        </h6>\n                </div>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/search-item/search-item.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_8__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_custome_custome__["a" /* CustomeProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_cart_cart__["a" /* CartProvider */]])
], SearchItemPage);

//# sourceMappingURL=search-item.js.map

/***/ }),

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the FilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let FilterPage = class FilterPage {
    constructor(navCtrl, language, platform, event, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.platform = platform;
        this.event = event;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        // prise: any
        this.selectedPrice = { lower: 0, upper: 0 };
        this.categories = [];
        this.sizeArray = [];
        this.is_special = false;
        this.in_stock = false;
        this.item_code = '';
        this.min = 0;
        this.appLang = '';
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad FilterPage');
        let data = this.navParams.get('data');
        data = JSON.parse(JSON.stringify(data));
        this.event.subscribe('appLangChange', () => {
            this.appLang = this.language.fnGetSelectedAppLang();
        });
        this.appLang = data.appLang;
        let categories = data.subCategorys;
        let selectedcategory = data.category_id;
        if (selectedcategory && selectedcategory != '') {
            selectedcategory = ',' + selectedcategory + ',';
        }
        else {
            selectedcategory = '';
        }
        for (let index = 0; index < categories.length; index++) {
            if (selectedcategory.indexOf(',' + categories[index].category_id + ',') != -1) {
                categories[index]['isSelected'] = true;
            }
            else {
                categories[index]['isSelected'] = false;
            }
        }
        this.categories = categories;
        let sizeArray = data.sizeArray;
        let selectedSize = data.size;
        if (selectedSize && selectedSize != '') {
            selectedSize = ',' + selectedSize + ',';
        }
        else {
            selectedSize = '';
        }
        for (let index = 0; index < sizeArray.length; index++) {
            if (selectedSize.indexOf(',' + sizeArray[index].size_id + ',') != -1) {
                sizeArray[index]['isSelected'] = true;
            }
            else {
                sizeArray[index]['isSelected'] = false;
            }
        }
        this.sizeArray = sizeArray;
        this.in_stock = data.in_stock;
        this.is_special = data.is_special;
        this.min = data.price.lower;
        this.max = data.price.upper;
        this.selectedPrice.lower = data.selectedPrice.lower;
        this.selectedPrice.upper = data.selectedPrice.upper;
        this.item_code = data.item_code;
    }
    fnSearch() {
        let selectedCategory = '';
        for (let index = 0; index < this.categories.length; index++) {
            if (this.categories[index].isSelected) {
                if (selectedCategory && selectedCategory != '') {
                    selectedCategory = selectedCategory + ',' + this.categories[index].category_id;
                }
                else {
                    selectedCategory = this.categories[index].category_id;
                }
            }
        }
        let size = '';
        for (let index = 0; index < this.sizeArray.length; index++) {
            if (this.sizeArray[index].isSelected) {
                if (size && size != '') {
                    size = size + ',' + this.sizeArray[index].size_id;
                }
                else {
                    size = this.sizeArray[index].size_id;
                }
            }
        }
        let data = {
            category_id: selectedCategory,
            size: size,
            item_code: this.item_code,
            is_special: this.is_special,
            in_stock: this.in_stock,
            selectedPrice: this.selectedPrice,
        };
        this.viewCtrl.dismiss({ data: { isCancelled: false, filterData: data } });
    }
    fnCancel() {
        this.viewCtrl.dismiss({ data: { isCancelled: true } });
    }
};
FilterPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-filter',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/filter/filter.html"*/'<!--\n  Generated template for the FilterPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n        <ion-toolbar *ngIf="platform.is(\'android\')">\n                <ion-row style="padding-top: 40px">\n                        <button (click)="fnCancel()" clear ion-button icon-only col-2>\n                                <ion-icon color="dark" name="arrow-back" style="    font-size: 22px;"></ion-icon>\n                        </button>\n                        <ion-title col-8 text-center>{{language.fnGetLanguage(\'title_filter\')}}</ion-title>\n                        <ion-buttons end col-2>\n                                <button ion-button icon-only id="notification-button" style="opacity: 0;">\n                                        <ion-icon color="dark" name="cart"></ion-icon>\n                                        <ion-badge small id="notifications-badge">1</ion-badge>\n                                </button>\n                        </ion-buttons>\n                </ion-row>\n        </ion-toolbar>\n        <ion-toolbar *ngIf="platform.is(\'ios\')">\n                <button (click)="fnCancel()" clear ion-button icon-only>\n                        <ion-icon color="dark" name="arrow-back"></ion-icon>\n                </button>\n                <ion-title>{{language.fnGetLanguage(\'title_filter\')}}</ion-title>\n        </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n        <ion-item class="price-slider">\n                <ion-label class="price-slider-main-label">{{language.fnGetLanguage(\'price_range\')}} :\n                        {{selectedPrice.lower}} -\n                        {{selectedPrice.upper}}</ion-label>\n                <ion-range dualKnobs="true" color="golden" min="{{min}}" max="{{max}}" [(ngModel)]="selectedPrice">\n                        <ion-label range-left>{{min}}</ion-label>\n                        <ion-label range-right>{{max}}</ion-label>\n                </ion-range>\n        </ion-item>\n\n\n        <ion-list>\n                <div class="categoriesGroup">\n                        <ion-list-header text-center>\n                                {{language.fnGetLanguage(\'title_categories\')}}\n                        </ion-list-header>\n                        <ion-item *ngFor="let category of categories">\n                                <ion-checkbox [(ngModel)]="category.isSelected" item-end color="golden"> </ion-checkbox>\n                                <ion-label>\n                                                <span *ngIf="category.category_name_lang[appLang]">\n                                                                <!-- {{category.category_name_lang}} -->\n                                                                {{category.category_name_lang[appLang]}}\n                                                        </span>\n                                                        <span *ngIf="!category.category_name_lang[appLang]">\n                                                                        <!-- {{category.category_name_lang}} -->\n\n                                                                {{ category.category_name }}\n                                                        </span>\n                                </ion-label>\n                        </ion-item>\n                </div>\n                <div class="sizeGroup" *ngIf="sizeArray && sizeArray.length > 0">\n                        <ion-list-header text-center>\n                                {{language.fnGetLanguage(\'title_size\')}}\n                        </ion-list-header>\n                        <ion-item *ngFor="let s of sizeArray">\n                                <ion-checkbox [(ngModel)]="s.isSelected" item-end color="golden"></ion-checkbox>\n                                <ion-label>{{s.size_name}}</ion-label>\n                        </ion-item>\n                </div>\n\n                <div class="otherFilterStuff">\n                        <ion-list-header text-center>\n                                {{language.fnGetLanguage(\'title_availabilities\')}}\n                        </ion-list-header>\n                        <ion-item>\n                                <ion-checkbox [(ngModel)]="is_special" item-end color="golden"></ion-checkbox>\n                                <ion-label>{{language.fnGetLanguage(\'special\')}}</ion-label>\n                        </ion-item>\n\n                        <ion-item>\n                                <ion-checkbox [(ngModel)]="in_stock" item-end color="golden"></ion-checkbox>\n                                <ion-label>{{language.fnGetLanguage(\'in_stock\')}}</ion-label>\n                        </ion-item>\n\n                        <ion-item>\n                                <ion-label style="color: #222">{{language.fnGetLanguage(\'item_code\')}}</ion-label>\n                                <ion-input [(ngModel)]="item_code" placeholder="{{language.fnGetLanguage(\'item_code\')}}"></ion-input>\n                        </ion-item>\n                </div>\n        </ion-list>\n</ion-content>\n<ion-footer>\n        <ion-row>\n                <ion-col col-6 color="light" text-center (click)="fnCancel()" style="font-weight: bold; margin: 0;letter-spacing: 2px !important;background: lightgray;padding: 17px">\n                        {{language.fnGetLanguage(\'cancel\')}}\n                </ion-col>\n                <ion-col col-6 text-center (click)="fnSearch()" style="font-weight:bold;margin: 0;letter-spacing: 2px !important;background: #9A772D;color: #fff;padding: 17px;">\n                        {{language.fnGetLanguage(\'search\')}}\n                </ion-col>\n        </ion-row>\n</ion-footer>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/filter/filter.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular_platform_platform__["a" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */]])
], FilterPage);

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PastOrdersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cart_cart__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__past_order_detail_past_order_detail__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the PastOrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let PastOrdersPage = class PastOrdersPage {
    constructor(navCtrl, navParams, language, event, cart, service, loadingCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.language = language;
        this.event = event;
        this.cart = cart;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.lazyLoadding = localStorage.getItem('lazyLoadding');
        this.width = (window.innerWidth - 52) / 2;
    }
    ionViewDidLoad() {
        this.cartItem = this.cart.fnGetCountOfCart();
        this.event.subscribe('cart:itemAdded', () => {
            this.cartItem = this.cart.fnGetCountOfCart();
        });
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.service.post('order/get_order_history', 'data').then((result) => {
            this.orders = result.order;
            loader.dismiss();
        }, (error) => {
            loader.dismiss();
            if (!error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
            }
        });
    }
    fnOpenCart() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__cart_cart__["a" /* CartPage */]);
    }
    fnOpenOrderPage(order, orderIndex) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__past_order_detail_past_order_detail__["a" /* PastOrderDetailPage */], { data: order });
    }
};
PastOrdersPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-past-orders',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/past-orders/past-orders.html"*/'<!--\n  Generated template for the PastOrdersPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n        <ion-navbar>\n                <button menuToggle ion-button icon-only>\n                        <ion-icon color="dark" name="menu"></ion-icon>\n                </button>\n                <ion-title>{{language.fnGetLanguage(\'title_my_orders\')}}</ion-title>\n                <ion-buttons end>\n                        <button (click)="fnOpenCart()" ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n\n                \n                \n        </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n        <div *ngIf="orders && orders.length>0">\n                <button ion-item *ngFor="let order of orders;let i = index" (click)="fnOpenOrderPage(order,i)">\n                        <div item-start class="dateFormat" color="golden">\n                                <span style="color: #9A772D;padding-right:3px; ">\n                                        {{order.order_date.split(\' \')[0].split(\'-\')[2] }}\n                                </span>\n                        </div>\n                        <span item-start style="margin:12px 0px 8px 0; color: #9A772D">\n                                <span>\n                                        <span [ngSwitch]="order.order_date.split(\' \')[0].split(\'-\')[1]">\n                                                <span *ngSwitchCase="\'01\'">\n                                                        {{language.fnGetLanguage(\'jan\')}}.\n                                                </span>\n                                                <span *ngSwitchCase="\'02\'">\n                                                        {{language.fnGetLanguage(\'feb\')}}.\n                                                </span>\n                                                <span *ngSwitchCase="\'03\'">\n                                                        {{language.fnGetLanguage(\'mar\')}}.\n                                                </span>\n                                                <span *ngSwitchCase="\'04\'">\n                                                        {{language.fnGetLanguage(\'apr\')}}.\n                                                </span>\n                                                <span *ngSwitchCase="\'05\'">\n                                                        {{language.fnGetLanguage(\'may\')}}.\n                                                </span>\n                                                <span *ngSwitchCase="\'06\'">\n                                                        {{language.fnGetLanguage(\'jun\')}}.\n                                                </span>\n                                                <span *ngSwitchCase="\'07\'">\n                                                        {{language.fnGetLanguage(\'jul\')}}.\n                                                </span>\n                                                <span *ngSwitchCase="\'08\'">\n                                                        {{language.fnGetLanguage(\'aug\')}}.\n                                                </span>\n                                                <span *ngSwitchCase="\'09\'">\n                                                        {{language.fnGetLanguage(\'sept\')}.\n                                                </span>\n                                                <span *ngSwitchCase="\'10\'">\n                                                        {{language.fnGetLanguage(\'oct\')}}.\n                                                </span>\n                                                <span *ngSwitchCase="\'11\'">\n                                                        {{language.fnGetLanguage(\'nov\')}}.\n                                                </span>\n                                                <span *ngSwitchCase="\'12\'">\n                                                        {{language.fnGetLanguage(\'dec\')}}.\n                                                </span>\n                                        </span>\n                                        <br> {{order.order_date.split(\' \')[0].split(\'-\')[0]}}\n                                </span>\n                        </span>\n                        <div class="details">\n                                <span>{{language.fnGetLanguage(\'order_No\')}}. {{ order.order_no }}\n                                        <br> ₹{{ order.total_amount.toLocaleString(\'en-IN\') }}\n                                        <br>\n                                        <span style="color: #666666">\n                                                {{ order.status }}\n                                        </span>\n                                </span>\n                                <span showWhen="android" class="android_icon">\n                                        <ion-icon name="arrow-forward"></ion-icon>\n                                </span>\n                        </div>\n                </button>\n        </div>\n        <div *ngIf="!orders || !(orders.length>0)"   style="margin: 10%; text-align: center; position: absolute; top: 30%" class="noitem">\n                <img src="assets/img_logo/empty-shopping-cart.png">\n                <h6>\n                        {{language.fnGetLanguage(\'no_order_found\')}}.\n                </h6>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/past-orders/past-orders.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
        __WEBPACK_IMPORTED_MODULE_2__providers_cart_cart__["a" /* CartProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_custome_custome__["a" /* CustomeProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], PastOrdersPage);

//# sourceMappingURL=past-orders.js.map

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PastOrderDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cart_cart__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__note_modal_note_modal__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_language_language__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jquery__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the PastOrderDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let PastOrderDetailPage = class PastOrderDetailPage {
    constructor(navCtrl, language, modalCtrl, event, navParams, cart, service, loadingCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.modalCtrl = modalCtrl;
        this.event = event;
        this.navParams = navParams;
        this.cart = cart;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.lazyLoadding = localStorage.getItem('lazyLoadding');
        this.width = (window.innerWidth - 52) / 2;
        this.appLang = localStorage.getItem('appLang');
    }
    ionViewDidLoad() {
        // this.event.unsubscribe('cart:itemAdded')
        this.cartItem = this.cart.fnGetCountOfCart();
        this.event.subscribe('cart:itemAdded', () => {
            this.cartItem = this.cart.fnGetCountOfCart();
        });
        this.event.subscribe('appLangChange', () => {
            this.appLang = this.language.fnGetSelectedAppLang();
        });
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        let form = {
            'order_id': this.navParams.get('data').order_id
        };
        this.order = this.navParams.get('data');
        this.service.post('order/get_order_details', form).then((result) => {
            this.items = result.order.items;
            this.orderData = result.order;
            loader.dismiss();
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
        });
    }
    fnOpenCart() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__cart_cart__["a" /* CartPage */]);
    }
    fnShowNotes(item) {
        let modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__note_modal_note_modal__["a" /* NoteModalPage */], { isCart: false, data: item });
        if (item && item.note) {
            modal.present();
        }
        else {
            this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('no_notes_for_this_product') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            }).present();
        }
    }
    fnLoadImage(elementID) {
        let id = "#" + elementID;
        __WEBPACK_IMPORTED_MODULE_7_jquery__(id).css("background", "none");
        __WEBPACK_IMPORTED_MODULE_7_jquery__(id).css("background", "#fff");
    }
};
PastOrderDetailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-past-order-detail',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/past-order-detail/past-order-detail.html"*/'<!--\n  Generated template for the PastOrderDetailPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n        <ion-navbar>\n                <ion-title>{{navParams.get(\'data\').order_no}}</ion-title>\n                <ion-buttons end>\n                        <button (click)="fnOpenCart()" ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{ cartItem }}\n                                </ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n        <ion-row class="cstHeader">\n                <ion-col col-6>\n                        <span style="color:#9C741E;font-weight: bold">\n                                {{language.fnGetLanguage(\'date\')}}\n                        </span>\n\n                        <br /> {{ navParams.get(\'data\').order_date.split(\' \')[0].split(\'-\')[2] }}/{{\n                        navParams.get(\'data\').order_date.split(\'\n                        \')[0].split(\'-\')[1] }}/{{ navParams.get(\'data\').order_date.split(\' \')[0].split(\'-\')[0] }}\n                </ion-col>\n                <ion-col col-6 style="text-align: right;">\n                        {{ navParams.get(\'data\').status }}\n                </ion-col>\n                <ion-col col-12 *ngIf="orderData && orderData.shipping_address">\n                        <span style="color:#9C741E;font-weight: bold">\n                                {{language.fnGetLanguage(\'shipping_to\')}}:\n                                <br>\n                        </span>\n                        {{orderData.shipping_address}}\n                </ion-col>\n        </ion-row>\n        <div *ngIf="items && items.length>0">\n                <ion-row style="border-bottom: 1px solid lightgray;" *ngFor="let item of items;let i = index"\n                        lazy-load-images>\n                        <ion-col col-4 class="setBackground" [style.width.px]="width" [style.minHeight.px]="width*4/3"\n                                id="prod_image_{{i}}_{{item.item_id}}">\n                                <img [attr.data-src]="item.item_image" style="height: auto;width: auto"\n                                        (load)="fnLoadImage(\'prod_image_\'+i+\'_\'+item.item_id)" *ngIf="lazyLoadding">\n                                <img [src]="item.item_image" style="height: auto;width: auto" *ngIf="!lazyLoadding">\n                        </ion-col>\n                        <ion-col col-8>\n                                <h6 style="font-weight: bold">\n                                        <span *ngIf="item.item_name_lang[appLang]">\n                                                {{item.item_name_lang[appLang]}}\n                                        </span>\n                                        <span *ngIf="!item.item_name_lang[appLang]">\n                                                {{ item.item_name }}\n                                        </span>\n                                </h6>\n                                <p>\n\n                                        {{language.fnGetLanguage(\'code\')}}\n                                        <span style="font-weight: bold">\n                                                {{item.item_code}}\n                                        </span>\n                                </p>\n                                <p *ngIf="item.size">\n\n                                        {{language.fnGetLanguage(\'size\')}}\n                                        <span style="font-weight: bold">\n                                                {{item.size}}\n                                        </span>\n                                </p>\n                                <p>\n\n                                        {{language.fnGetLanguage(\'rate\')}}\n                                        <span style="font-weight: bold">\n                                                {{ item.rate}}\n                                        </span>\n                                </p>\n                                <p>\n\n                                        {{language.fnGetLanguage(\'qty\')}}\n                                        <span style="font-weight: bold">\n                                                {{ item.qty}}\n                                        </span>\n                                </p>\n                                <p>\n                                        {{language.fnGetLanguage(\'total\')}}\n                                        <span style="font-weight: bold">\n                                                ₹{{ item.amount.toLocaleString(\'en-IN\') }}\n                                        </span>\n                                </p>\n                                <p *ngIf="item.note">\n                                        <button ion-button small clear\n                                                (click)="fnShowNotes(item)">{{language.fnGetLanguage(\'note\')}}</button>\n                                </p>\n                        </ion-col>\n\n                </ion-row>\n\n        </div>\n</ion-content>\n<ion-footer large style="text-align: center;background: #9A772D;padding: 10px">\n        <ion-row>\n                <ion-col style="padding: 10px; color: #fff;    font-size: 1.7rem;">\n                        {{language.fnGetLanguage(\'title_total\')}} :\n                        ₹{{ navParams.get(\'data\').total_amount.toLocaleString(\'en-IN\') }}\n                </ion-col>\n        </ion-row>\n</ion-footer>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/past-order-detail/past-order-detail.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */], __WEBPACK_IMPORTED_MODULE_6__providers_language_language__["a" /* LanguageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_cart_cart__["a" /* CartProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_custome_custome__["a" /* CustomeProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], PastOrderDetailPage);

//# sourceMappingURL=past-order-detail.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__category_category__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let ChangePasswordPage = class ChangePasswordPage {
    constructor(navCtrl, language, loadingCtrl, navParams, alertCtrl, service) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.service = service;
    }
    ionViewDidLoad() {
    }
    fnChangePassword() {
        if (!this.oldPassword || this.oldPassword.length < 6) {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('enter_valid_password'),
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        if (!this.newPassword || this.newPassword.length < 6) {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('password_length_must_be_minimum') + ' 6',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        if (this.newPassword != this.conformPassword) {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('new_password_and_conform_password_are_diffrent') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        let form = {
            'password': this.oldPassword,
            'new_password': this.conformPassword
        };
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.service.post('account/change_password', form).then((result) => {
            loader.dismiss();
            let msg = ('your_password_has_been_changed') + '.';
            if (result.message) {
                msg = result.message;
            }
            let alert = this.alertCtrl.create({
                subTitle: msg,
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            alert.onDidDismiss((result) => {
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__category_category__["a" /* CategoryPage */]);
            });
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
        });
    }
};
ChangePasswordPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-change-password',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/change-password/change-password.html"*/'<!--\n  Generated template for the ChangePasswordPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n        <ion-navbar>\n                <button menuToggle ion-button icon-only start>\n                        <ion-icon color="dark" name="menu"></ion-icon>\n                </button>\n                <ion-title>{{language.fnGetLanguage(\'title_change_password\')}}</ion-title>\n                <ion-buttons end style="opacity: 0;" showWhen="android">\n                        <button ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n        <ion-row>\n                <ion-col col-12 style="text-align: center">\n                        <img src="assets/img_logo/login-icon.png">\n                        <h4 style="letter-spacing:3px">\n                                {{language.fnGetLanguage(\'change_password\')}}\n                        </h4>\n                        <h6 style="font-weight: 100; ">\n                                {{language.fnGetLanguage(\'fill_up_the_bellow_to_change_your_password\')}}\n\n                        </h6>\n                </ion-col>\n        </ion-row>\n\n        <ion-row class="mt20px">\n                <span class="spanPadding">{{language.fnGetLanguage(\'old_password\')}}</span>\n                <ion-input col-12 type="password" placeholder="{{language.fnGetLanguage(\'enter_old_password\')}}" class="inputarea"\n                        [(ngModel)]="oldPassword"></ion-input>\n        </ion-row>\n\n        <ion-row class="mt20px">\n                <span class="spanPadding">{{language.fnGetLanguage(\'new_password\')}}</span>\n                <ion-input col-12 type="password" placeholder="{{language.fnGetLanguage(\'enter_new_password\')}}" class="inputarea"\n                        [(ngModel)]="newPassword"></ion-input>\n        </ion-row>\n        <ion-row class="mt20px">\n                <span class="spanPadding">{{language.fnGetLanguage(\'confirm_password\')}}</span>\n                <ion-input col-12 type="password" placeholder="{{language.fnGetLanguage(\'re_enter_your_password\')}}"\n                        class="inputarea" [(ngModel)]="conformPassword"></ion-input>\n        </ion-row>\n        <button ion-button large color="golden" block (click)="fnChangePassword()" class="mt20px">{{language.fnGetLanguage(\'submit\')}}</button>\n\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/change-password/change-password.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__["a" /* CustomeProvider */]])
], ChangePasswordPage);

//# sourceMappingURL=change-password.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(293);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 29:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shipping_address_shipping_address__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__product_product__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__user_detail_user_detail__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__note_modal_note_modal__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_language_language__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_jquery__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let CartPage = class CartPage {
    constructor(navCtrl, modalCtrl, language, menu, event, alertCtrl, navParams, cart) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.language = language;
        this.menu = menu;
        this.event = event;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.cart = cart;
        this.item = 10;
        this.j = 1;
        this.lazyLoadding = localStorage.getItem('lazyLoadding');
        this.width = (window.innerWidth - 52) / 2;
        this.appLang = localStorage.getItem('appLang');
    }
    ionViewDidLoad() {
        this.appLang = localStorage.getItem('appLang');
        if (!this.navCtrl.canGoBack()) {
            this.menu.enable(true);
        }
        if (this.j == 1) {
            this.event.subscribe('cart:itemAdded', () => {
                this.j = 0;
                this.cartItem = this.cart.fnGetCountOfCart();
                this.ionViewDidLoad();
            });
        }
        this.cartData = this.cart.fnGetCart();
        this.cartItem = this.cart.fnGetCountOfCart();
        let total = 0;
        if (this.cartData && this.cartData.length > 0) {
            for (let index = 0; index < this.cartData.length; index++) {
                total = parseInt(total) + this.cartData[index].qty * this.cartData[index].sale_price;
            }
            this.totalAmount = total;
        }
        this.event.subscribe('appLangChange', () => {
            this.appLang = this.language.fnGetSelectedAppLang();
        });
    }
    fnDecreaseQuantity(c, i) {
        if (c.qty > c.min_order_qty) {
            let qty = parseInt(this.cartData[i].qty) - 1;
            this.cart.fnChangeQty(i, qty);
            this.cartData = this.cart.fnGetCart();
            let total = 0;
            for (let index = 0; index < this.cartData.length; index++) {
                total = parseInt(total) + this.cartData[index].qty * this.cartData[index].sale_price;
            }
            this.totalAmount = total;
            this.event.publish('cart:itemAdded');
        }
        else {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('you_can_not_input_order_quantity_less_than_') + c.min_order_qty,
                enableBackdropDismiss: false,
                buttons: [this.language.fnGetLanguage('title_ok')],
            });
            alert.present();
        }
    }
    fnIncreaseQuantity(c, i) {
        let qty = 1 + parseInt(this.cartData[i].qty);
        this.cart.fnChangeQty(i, qty);
        this.cartData = this.cart.fnGetCart();
        let total = 0;
        for (let index = 0; index < this.cartData.length; index++) {
            // this.cartData[index].qty = this.cartData[index].min_order_qty
            total = parseInt(total) + this.cartData[index].qty * this.cartData[index].sale_price;
        }
        this.totalAmount = total;
        this.event.publish('cart:itemAdded');
    }
    fnPlaceOrder() {
        if (this.cartData && this.cartData.length > 0) {
            this.cart.isPlaceOrder = true;
            if (localStorage.getItem('token')) {
                if (!localStorage.getItem('name') || localStorage.getItem('name') == '' || localStorage.getItem('name') == 'null' || localStorage.getItem('name') == null) {
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__user_detail_user_detail__["a" /* UserDetailPage */]);
                }
                else {
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__shipping_address_shipping_address__["a" /* ShippingAddressPage */]);
                }
            }
            else {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
            }
        }
    }
    fnRemoveFromCart(c, i) {
        let f = 0;
        let alert = this.alertCtrl.create({
            subTitle: this.language.fnGetLanguage('are_you_sure_to_remove_an_item_from_cart') + '?',
            buttons: [
                {
                    text: this.language.fnGetLanguage('cancel'),
                    handler: () => {
                        f = 0;
                        return true;
                    }
                },
                {
                    text: this.language.fnGetLanguage('title_ok'),
                    handler: () => {
                        f = 1;
                        return true;
                    }
                }
            ]
        });
        alert.present();
        alert.onDidDismiss((result) => {
            if (f == 1) {
                this.cart.fnRemoveFromCart(i);
                this.event.publish('cart:itemAdded');
                this.event.publish('note:changed');
                this.cartData = this.cart.fnGetCart();
                this.cartItem = this.cart.fnGetCountOfCart();
                let total = 0;
                for (let index = 0; index < this.cartData.length; index++) {
                    // this.cartData[index].qty = this.cartData[index].min_order_qty
                    total = parseInt(total) + this.cartData[index].qty * this.cartData[index].sale_price;
                }
                this.totalAmount = total;
            }
        });
    }
    fnOpenProductPage(product) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__product_product__["a" /* ProductPage */], { data: product });
    }
    onQtyChange(c, i) {
        if (parseInt(c.qty) && parseInt(c.qty) > 0) {
            if (parseInt(c.qty) > parseInt(c.min_order_qty)) {
                this.cart.fnChangeQty(i, parseInt(this.cartData[i].qty));
                this.event.publish('cart:itemAdded');
                this.ionViewDidLoad();
            }
            else if (parseInt(c.qty) == parseInt(c.min_order_qty)) {
                this.cart.fnChangeQty(i, parseInt(c.qty));
                this.event.publish('cart:itemAdded');
                this.ionViewDidLoad();
            }
            else {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('you_can_not_input_order_quantity_less_than_') + c.min_order_qty,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                this.event.publish('cart:itemAdded');
                alert.onDidDismiss((result) => {
                    this.ionViewDidLoad();
                });
            }
        }
        else {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('please_enter_a_quantity_greater_than_or_equal_to_') + c.min_order_qty,
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            alert.onDidDismiss((result) => {
                this.event.publish('cart:itemAdded');
                this.ionViewDidLoad();
            });
        }
    }
    fnShowNotes(c, i) {
        let modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__note_modal_note_modal__["a" /* NoteModalPage */], { isCart: true, data: c });
        modal.present();
        modal.onDidDismiss((result) => {
            if (result.addToCart) {
                this.cartData[i].note = result.note;
                this.cart.fnNoteChange(i, result.note);
                this.event.publish('note:changed');
            }
            else {
            }
        });
    }
    fnLoadImage(elementID) {
        let id = "#" + elementID;
        __WEBPACK_IMPORTED_MODULE_9_jquery__(id).css("background", "none");
        __WEBPACK_IMPORTED_MODULE_9_jquery__(id).css("background", "#fff");
    }
};
CartPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-cart',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/cart/cart.html"*/'<!--\n  Generated template for the CartPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n        <ion-navbar>\n                <button menuToggle ion-button icon-only>\n                        <ion-icon color="dark" name="menu"></ion-icon>\n                </button>\n                <ion-title>{{language.fnGetLanguage(\'title_shopping_bag\')}}</ion-title>\n                <ion-buttons end>\n                        <button ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}\n                                </ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n        <div *ngIf="cartData && cartData.length>0">\n                <ion-row style="border-bottom: 1px solid lightgray;" *ngFor="let c of cartData;let i = index">\n                        <ion-col col-4 *ngIf="c.gallary && c.gallary.length>0" style="padding-bottom: 2px;"\n                                id="prod_image_{{i}}_{{c.item_id}}" class="setBackground" [style.width.px]="width"\n                                [style.minHeight.px]="width*4/3">\n                                <!-- <img src="{{ c.gallary[0].l }}" style="height: auto;width: auto"\n                                        (click)="fnOpenProductPage(c)"> -->\n                                <img [attr.data-src]="c.gallary[0].l" style="height: auto;width: auto"\n                                        (load)="fnLoadImage(\'prod_image_\'+i+\'_\'+c.item_id)"\n                                        (click)="fnOpenProductPage(c)" *ngIf="lazyLoadding">\n                                <img [src]="c.gallary[0].l" (click)="fnOpenProductPage(c)" *ngIf="!lazyLoadding">\n                        </ion-col>\n                        <ion-col col-5>\n                                <h6 style="font-weight: bold" *ngIf="c.item_name_lang[appLang]">\n                                        {{ c.item_name_lang[appLang] }}\n\n                                </h6>\n                                <h6 style="font-weight: bold" *ngIf="!c.item_name_lang[appLang]">\n                                        {{ c.item_name }}\n                                </h6>\n                                <p>\n                                        {{language.fnGetLanguage(\'code\')}}\n                                        <span style="font-weight: 600">\n                                                {{c.item_code}}\n                                        </span>\n                                </p>\n                                <p *ngIf="c.size">\n                                        {{language.fnGetLanguage(\'size\')}}\n                                        <span style="font-weight: 600">\n                                                {{ c.size}}\n                                        </span>\n                                </p>\n                                <p>\n                                        {{language.fnGetLanguage(\'rate\')}}\n                                        <span style="font-weight: 600">\n                                                ₹{{ (c.sale_price).toLocaleString(\'en-IN\') }}\n                                        </span>\n                                </p>\n                                <table>\n                                        <tr>\n                                                <td (click)="fnDecreaseQuantity(c,i)"> - </td>\n                                                <td>\n                                                        <ion-input type="number" [(ngModel)]="c.qty"\n                                                                (change)="onQtyChange(c,i)"\n                                                                style="width: 44px;text-align: center"></ion-input>\n                                                </td>\n                                                <td (click)="fnIncreaseQuantity(c,i)"> + </td>\n                                        </tr>\n                                </table>\n                        </ion-col>\n                        <ion-col col-3>\n                                <span class="delete_custome_position" (click)="fnRemoveFromCart(c,i)">\n                                        <ion-icon name="trash" style="font-size: 30px;"></ion-icon>\n                                </span>\n                                <span class="notes_icon_position" (click)="fnShowNotes(c,i)">\n                                        <ion-icon name="list-box" style="font-size: 25px"></ion-icon>\n                                </span>\n                                <span class="totalrs_custome_position" color="golden">\n                                        ₹{{ (c.qty*c.sale_price).toLocaleString(\'en-IN\') }}\n                                </span>\n                        </ion-col>\n                </ion-row>\n        </div>\n        <div *ngIf="!cartData || cartData.length==0"\n                style="margin: 10%; text-align: center; position: absolute; top: 30%" class="noitem">\n                <img src="assets/img_logo/empty-shopping-cart.png">\n                <h6>\n                        {{language.fnGetLanguage(\'no_items_in_your_cart\')}}\n                </h6>\n        </div>\n</ion-content>\n<ion-footer>\n        <ion-row style="background: #9A772D">\n                <ion-col col-6 style="padding: 10px;background: #eeeeee;" id="col">\n                        <div style="text-align: center;padding: 7px; font-weight:bold;" color="golden">\n                                {{language.fnGetLanguage(\'total\')}} :\n                                <span *ngIf="cartData && cartData.length>0">\n                                        ₹{{ totalAmount.toLocaleString(\'en-IN\') }}</span>\n                                <span *ngIf="!cartData || !(cartData.length>0)" style="font-weight:bold;">\n                                        ₹0.00\n                                </span>\n                        </div>\n                </ion-col>\n                <ion-col col-6 style="padding: 10px;height: auto; margin: auto; background: #9A772D"\n                        (click)="fnPlaceOrder()" id="col2">\n                        <div style="text-align: center;padding: 7px; color: #fff;cursor: pointer; font-weight:bold;">\n                                {{language.fnGetLanguage(\'place_order\')}}\n                        </div>\n                </ion-col>\n\n        </ion-row>\n</ion-footer>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/cart/cart.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_8__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_cart_cart__["a" /* CartProvider */]])
], CartPage);

//# sourceMappingURL=cart.js.map

/***/ }),

/***/ 293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_category_category__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_product_product__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_cart_cart__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_shipping_address_shipping_address__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_login_login__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_signup_signup__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_common_http__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_image_viewer_image_viewer__ = __webpack_require__(251);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_user_detail_user_detail__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_otp_verify_otp_verify__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_forgott_password_forgott_password__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_file_transfer__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_file__ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_camera__ = __webpack_require__(246);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_past_orders_past_orders__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_past_order_detail_past_order_detail__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_change_mobile_no_change_mobile_no__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_verify_change_mobile_verify_change_mobile__ = __webpack_require__(248);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_change_password_change_password__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_screen_orientation__ = __webpack_require__(456);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_network__ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__angular_platform_browser_animations__ = __webpack_require__(459);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_http__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_order_success_order_success__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_native_onesignal__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_native_badge__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_notofication_notofication__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_local_notifications__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_note_modal_note_modal__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__ionic_native_base64_to_gallery__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__ionic_native_market__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__ionic_native_photo_library__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__ionic_native_android_permissions__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_search_item_search_item__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__providers_language_language__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_filter_filter__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_settings_settings__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_splash_splash__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__ionic_native_in_app_browser__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__ionic_native_app_availability__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49_ngx_lazy_load_images__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49_ngx_lazy_load_images___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_49_ngx_lazy_load_images__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__Library_ionic_img_viewer_dist_es2015_src_module__ = __webpack_require__(256);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















































let AppModule = class AppModule {
};
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_category_category__["a" /* CategoryPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_product_product__["a" /* ProductPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_cart_cart__["a" /* CartPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_shipping_address_shipping_address__["a" /* ShippingAddressPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_image_viewer_image_viewer__["a" /* ImageViewerPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_user_detail_user_detail__["a" /* UserDetailPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_otp_verify_otp_verify__["a" /* OtpVerifyPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_forgott_password_forgott_password__["a" /* ForgottPasswordPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_past_orders_past_orders__["a" /* PastOrdersPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_change_password_change_password__["a" /* ChangePasswordPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_past_order_detail_past_order_detail__["a" /* PastOrderDetailPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_change_mobile_no_change_mobile_no__["a" /* ChangeMobileNoPage */],
            __WEBPACK_IMPORTED_MODULE_37__pages_note_modal_note_modal__["a" /* NoteModalPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_verify_change_mobile_verify_change_mobile__["a" /* VerifyChangeMobilePage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_order_success_order_success__["a" /* OrderSuccessPage */],
            __WEBPACK_IMPORTED_MODULE_42__pages_search_item_search_item__["a" /* SearchItemPage */],
            __WEBPACK_IMPORTED_MODULE_44__pages_filter_filter__["a" /* FilterPage */],
            __WEBPACK_IMPORTED_MODULE_35__pages_notofication_notofication__["a" /* NotoficationPage */],
            __WEBPACK_IMPORTED_MODULE_45__pages_settings_settings__["a" /* SettingsPage */],
            __WEBPACK_IMPORTED_MODULE_46__pages_splash_splash__["a" /* SplashPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */], __WEBPACK_IMPORTED_MODULE_49_ngx_lazy_load_images__["LazyLoadImagesModule"],
            __WEBPACK_IMPORTED_MODULE_14__angular_common_http__["b" /* HttpClientModule */], __WEBPACK_IMPORTED_MODULE_50__Library_ionic_img_viewer_dist_es2015_src_module__["a" /* IonicImageViewerModule */],
            __WEBPACK_IMPORTED_MODULE_30__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                links: []
            }),
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_category_category__["a" /* CategoryPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_product_product__["a" /* ProductPage */],
            __WEBPACK_IMPORTED_MODULE_37__pages_note_modal_note_modal__["a" /* NoteModalPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_cart_cart__["a" /* CartPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_shipping_address_shipping_address__["a" /* ShippingAddressPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_image_viewer_image_viewer__["a" /* ImageViewerPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_user_detail_user_detail__["a" /* UserDetailPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_otp_verify_otp_verify__["a" /* OtpVerifyPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_change_password_change_password__["a" /* ChangePasswordPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_forgott_password_forgott_password__["a" /* ForgottPasswordPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_past_orders_past_orders__["a" /* PastOrdersPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_past_order_detail_past_order_detail__["a" /* PastOrderDetailPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_change_mobile_no_change_mobile_no__["a" /* ChangeMobileNoPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_verify_change_mobile_verify_change_mobile__["a" /* VerifyChangeMobilePage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_order_success_order_success__["a" /* OrderSuccessPage */],
            __WEBPACK_IMPORTED_MODULE_35__pages_notofication_notofication__["a" /* NotoficationPage */],
            __WEBPACK_IMPORTED_MODULE_42__pages_search_item_search_item__["a" /* SearchItemPage */],
            __WEBPACK_IMPORTED_MODULE_44__pages_filter_filter__["a" /* FilterPage */],
            __WEBPACK_IMPORTED_MODULE_45__pages_settings_settings__["a" /* SettingsPage */],
            __WEBPACK_IMPORTED_MODULE_46__pages_splash_splash__["a" /* SplashPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_13__providers_custome_custome__["a" /* CustomeProvider */],
            __WEBPACK_IMPORTED_MODULE_48__ionic_native_app_availability__["a" /* AppAvailability */],
            __WEBPACK_IMPORTED_MODULE_15__providers_cart_cart__["a" /* CartProvider */],
            __WEBPACK_IMPORTED_MODULE_28__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_20__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_22__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_31__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_33__ionic_native_onesignal__["a" /* OneSignal */], __WEBPACK_IMPORTED_MODULE_39__ionic_native_market__["a" /* Market */], __WEBPACK_IMPORTED_MODULE_40__ionic_native_photo_library__["a" /* PhotoLibrary */],
            __WEBPACK_IMPORTED_MODULE_38__ionic_native_base64_to_gallery__["a" /* Base64ToGallery */],
            __WEBPACK_IMPORTED_MODULE_20__ionic_native_file_transfer__["b" /* FileTransferObject */],
            __WEBPACK_IMPORTED_MODULE_34__ionic_native_badge__["a" /* Badge */],
            __WEBPACK_IMPORTED_MODULE_29__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_47__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_21__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_36__ionic_native_local_notifications__["a" /* LocalNotifications */],
            __WEBPACK_IMPORTED_MODULE_41__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_43__providers_language_language__["a" /* LanguageProvider */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_category_category__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_cart_cart__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_past_orders_past_orders__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_user_detail_user_detail__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_change_password_change_password__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_jquery__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_onesignal__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_badge__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_language_language__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_in_app_browser__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_app_availability__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_market__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_settings_settings__ = __webpack_require__(145);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};





















let MyApp = class MyApp {
    constructor(platform, statusBar, LanguageService, event, iab, cart, alertCtrl, menuCtrl, badge, oneSignal, splashScreen, langService, appAvailability, language, actionSheetCtrl, service, market) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.LanguageService = LanguageService;
        this.event = event;
        this.iab = iab;
        this.cart = cart;
        this.alertCtrl = alertCtrl;
        this.menuCtrl = menuCtrl;
        this.badge = badge;
        this.oneSignal = oneSignal;
        this.splashScreen = splashScreen;
        this.langService = langService;
        this.appAvailability = appAvailability;
        this.language = language;
        this.actionSheetCtrl = actionSheetCtrl;
        this.service = service;
        this.market = market;
        this.languages = [];
        this.name = 'Brijwasi';
        this.pages = [];
        this.isLoggedIn = false;
        this.mobileNo = '';
        this.categories = [];
        this.appLang = '';
        this.j = 0;
        this.appUpdate = 0;
        this.initializeApp();
    }
    initializeApp() {
        this.platform.ready().then(() => {
            localStorage.setItem('lazyLoadding', 'true'); //true for active lazyloadding '' for off
            let appId = '75bd2dcc-eb89-49a6-8df9-2373d7b71614';
            let googleProjectId = '714052471595';
            this.event.unsubscribe('appLangChange');
            this.appLang = localStorage.getItem('appLang');
            this.event.subscribe('appLangChange', () => {
                this.appLang = this.language.fnGetSelectedAppLang();
            });
            this.oneSignal.startInit(appId, googleProjectId);
            this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
            // this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification)
            // 
            this.oneSignal.setSubscription(true);
            this.intialize_seprate();
            this.oneSignal.handleNotificationReceived().subscribe((data) => {
                this.badge.get().then((r) => {
                    if (r == 0) {
                        this.setBadges(1);
                    }
                    else if (r > 0) {
                        this.increaseBadges('1');
                    }
                    else {
                        this.setBadges(1);
                    }
                }, (e) => {
                });
                let item = JSON.stringify(JSON.parse(JSON.parse(data.payload.rawPayload).custom).a);
                item['isOpen'] = false;
                var notifications = [];
                if (localStorage.getItem('notiications')) {
                    notifications = JSON.parse(localStorage.getItem('notiications'));
                }
                if (notifications.length > 0) {
                    notifications.splice(0, 0, item);
                }
                else {
                    notifications.push(item);
                }
                localStorage.setItem('notiications', JSON.stringify(notifications));
            });
            this.oneSignal.handleNotificationOpened().subscribe((data) => {
                this.clearBadges();
                localStorage.setItem('notificationOpen', '1');
                if (data.notification.payload.additionalData) {
                    this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_category_category__["a" /* CategoryPage */], { data: data.notification.payload.additionalData });
                }
                else {
                    this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_category_category__["a" /* CategoryPage */], { data: JSON.parse(JSON.parse(data.notification.payload.rawPayload).custom).a });
                }
            });
            this.oneSignal.endInit();
            this.statusBar.styleDefault();
            this.splashScreenStuff();
            // this.splashScreen.hide();
        });
    }
    intialize_seprate() {
        this.event.unsubscribe('category:updated');
        this.event.unsubscribe('userloggedin');
        this.event.unsubscribe('userNotloggedin');
        this.event.unsubscribe('langSet');
        this.event.subscribe('langSet', () => {
            this.initializeApp();
        });
        this.cart.fnGetCart();
        this.event.subscribe('category:updated', (data) => {
            this.categories = JSON.parse(JSON.stringify(data));
            if (this.j == 0) {
                let pageIndex = 2;
                for (let index = 0; index < data.length; index++) {
                    let stringToBeInserted = {
                        title: data[index].category_name,
                        component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                        data: data[index],
                        parent: 'collections',
                        isCategory: true,
                        active: false
                    };
                    if (this.appLang && data[index].category_name_lang[this.appLang] && data[index].category_name_lang[this.appLang] != '' && data[index].category_name_lang[this.appLang] != {}) {
                        stringToBeInserted['title'] = data[index].category_name_lang[this.appLang];
                    }
                    this.pages.splice(pageIndex, 0, stringToBeInserted);
                    pageIndex = pageIndex + 1;
                }
                this.j = 1;
            }
        });
        this.event.subscribe('userloggedin', (data) => {
            this.initializeApp();
        });
        /****************************** */
        this.service.post('startup/get_lang', {}).then((result) => {
            let languages = result.language;
            this.languages = [];
            for (var k in languages) {
                this.languages.push({ key: k, value: languages[k] });
            }
            this.LanguageService.fnSetLanguageArray(this.languages);
            this.LanguageService.fnSetLangLabelArray(result.label);
            if (localStorage.getItem('token') && localStorage.getItem('token') != '') {
                this.mobileNo = localStorage.getItem('mobile_no');
                this.pages = [
                    {
                        title: this.language.fnGetLanguage('home'),
                        component: __WEBPACK_IMPORTED_MODULE_5__pages_category_category__["a" /* CategoryPage */],
                        data: '',
                        icon: 'home',
                        active: true
                    },
                    {
                        title: this.language.fnGetLanguage('our_collection'),
                        data: '',
                        hasChild: true,
                        icon: 'logo-buffer',
                        childSelector: 'ourCollections',
                        active: false
                    },
                    {
                        title: this.language.fnGetLanguage('cart'),
                        component: __WEBPACK_IMPORTED_MODULE_6__pages_cart_cart__["a" /* CartPage */],
                        data: '',
                        icon: 'cart',
                        active: false
                    },
                    {
                        title: this.language.fnGetLanguage('my_account'),
                        data: '',
                        hasChild: true,
                        icon: 'person',
                        childSelector: 'Profile',
                        active: false
                    },
                    {
                        title: this.language.fnGetLanguage('my_orders'),
                        component: __WEBPACK_IMPORTED_MODULE_9__pages_past_orders_past_orders__["a" /* PastOrdersPage */],
                        data: '',
                        parent: 'Profile',
                        active: false
                    },
                    {
                        title: this.language.fnGetLanguage('profile'),
                        component: __WEBPACK_IMPORTED_MODULE_10__pages_user_detail_user_detail__["a" /* UserDetailPage */],
                        data: true,
                        parent: 'Profile',
                        active: false
                    },
                    {
                        title: this.language.fnGetLanguage('change_password'),
                        component: __WEBPACK_IMPORTED_MODULE_11__pages_change_password_change_password__["a" /* ChangePasswordPage */],
                        data: true,
                        parent: 'Profile',
                        active: false
                    }
                ];
                this.j = 0;
                let data = {};
                this.service.post('category/get_category', data).then((result) => {
                    this.event.publish('category:updated', result.category);
                }, (error) => { });
                this.isLoggedIn = true;
                if (localStorage.getItem('name') && localStorage.getItem('name') != 'null') {
                    this.name = localStorage.getItem('name');
                }
                if (localStorage.getItem('mobile_no') && localStorage.getItem('mobile_no') != 'null') {
                    this.mobileNo = localStorage.getItem('mobile_no');
                }
            }
            else {
                this.pages = [
                    {
                        title: this.language.fnGetLanguage('home'),
                        component: __WEBPACK_IMPORTED_MODULE_5__pages_category_category__["a" /* CategoryPage */],
                        data: '',
                        icon: 'home',
                        active: true
                    },
                    {
                        title: this.language.fnGetLanguage('our_collection'),
                        data: '',
                        hasChild: true,
                        icon: 'logo-buffer',
                        childSelector: 'ourCollections'
                    },
                    {
                        title: this.language.fnGetLanguage('cart'),
                        component: __WEBPACK_IMPORTED_MODULE_6__pages_cart_cart__["a" /* CartPage */],
                        data: '',
                        icon: 'cart',
                        active: false
                    },
                    {
                        title: this.language.fnGetLanguage('login_and_signup'),
                        component: __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */],
                        data: '',
                        icon: 'person',
                        active: false
                    },
                ];
                this.j = 0;
                this.service.post('category/get_category', {}).then((result) => {
                    this.mobileNo = result.helpline_number;
                    localStorage.setItem('whatsapp_number', result.whatsapp_number);
                    this.event.publish('category:updated', result.category);
                    this.event.publish('userNotloggedin', result.helpline_number);
                }, (error) => { });
                this.isLoggedIn = false;
                this.name = 'Brijwasi';
            }
        }, (error) => {
        });
        /****************************** */
    }
    splashScreenStuff() {
        this.menuCtrl.enable(false);
        this.platform.ready().then(() => {
            if (this.cart.appUpdate == 0) {
                if (this.platform.is('ios')) {
                    this.service.post('startup/check_apple_version_update', { version: '1.0.3' })
                        .then((result) => {
                        this.splashScreen.hide();
                        if (result && result.maintenance && result.maintenance.apple) {
                            let alert = this.alertCtrl.create({
                                subTitle: result.maintenance.msg,
                                enableBackdropDismiss: false,
                                buttons: [
                                    {
                                        text: this.language.fnGetLanguage('close'),
                                        handler: () => {
                                            this.platform.exitApp();
                                        }
                                    }
                                ]
                            });
                            alert.present();
                            alert.onDidDismiss(() => {
                                this.platform.exitApp();
                            });
                        }
                        else if (result.update_status == 2 || result.update_status == '2') {
                            let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('please_update_an_app'),
                                enableBackdropDismiss: false,
                                buttons: [
                                    {
                                        text: this.language.fnGetLanguage('update'),
                                        handler: () => {
                                            this.market.open(result.package_name).then((r) => {
                                                // this.market.open()
                                            });
                                        }
                                    }
                                ]
                            });
                            alert.present();
                            alert.onDidDismiss(() => {
                                this.platform.exitApp();
                            });
                        }
                        else if (result.update_status == 1 || result.update_status == '1') {
                            this.cart.appUpdate = 1;
                            let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('new_update_available') + '.',
                                enableBackdropDismiss: false,
                                buttons: [
                                    {
                                        text: this.language.fnGetLanguage('close'),
                                        handler: () => {
                                            if (result.multy_language) {
                                                if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                                    this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                                    this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'));
                                                    this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_category_category__["a" /* CategoryPage */]);
                                                }
                                                else {
                                                    this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_20__pages_settings_settings__["a" /* SettingsPage */]);
                                                }
                                            }
                                            else {
                                                this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_category_category__["a" /* CategoryPage */]);
                                            }
                                        }
                                    },
                                    {
                                        text: this.language.fnGetLanguage('update'),
                                        handler: () => {
                                            this.market.open('com.brijwasibj').then((r) => {
                                                // open a play store
                                            }, (e) => {
                                            });
                                        }
                                    }
                                ]
                            });
                            alert.present();
                        }
                        else if (result.multy_language) {
                            if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'));
                                this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_category_category__["a" /* CategoryPage */]);
                            }
                            else {
                                this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_20__pages_settings_settings__["a" /* SettingsPage */]);
                            }
                        }
                        else {
                            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_category_category__["a" /* CategoryPage */]);
                        }
                    }, (error) => {
                        // error
                        this.splashScreen.hide();
                        if (error && error.msg) {
                            this.alertCtrl.create({
                                message: error.msg,
                                buttons: [
                                    {
                                        text: 'OK',
                                        handler: () => {
                                            this.platform.exitApp();
                                        }
                                    }
                                ]
                            }).present();
                        }
                    });
                }
                else if (this.platform.is('android')) {
                    this.service.post('startup/check_android_version_update', { version: '1.0.3' })
                        .then((result) => {
                        this.splashScreen.hide();
                        if (result && result.maintenance && result.maintenance.android) {
                            let alert = this.alertCtrl.create({
                                subTitle: result.maintenance.msg,
                                enableBackdropDismiss: false,
                                buttons: [
                                    {
                                        text: this.language.fnGetLanguage('close'),
                                        handler: () => {
                                            this.platform.exitApp();
                                        }
                                    }
                                ]
                            });
                            alert.present();
                            alert.onDidDismiss(() => {
                                this.platform.exitApp();
                            });
                        }
                        else if (result.update_status == 2 || result.update_status == '2') {
                            let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('please_update_an_app'),
                                enableBackdropDismiss: false,
                                buttons: [
                                    {
                                        text: this.language.fnGetLanguage('close'),
                                        handler: () => {
                                            this.platform.exitApp();
                                        }
                                    },
                                    {
                                        text: 'Update',
                                        handler: () => {
                                            this.market.open(result.package_name);
                                            this.platform.exitApp();
                                        }
                                    }
                                ]
                            });
                            alert.present();
                            alert.onDidDismiss(() => {
                                this.platform.exitApp();
                            });
                        }
                        else if (result.update_status == 1 || result.update_status == '1') {
                            this.cart.appUpdate = 1;
                            let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('new_update_available') + '.',
                                enableBackdropDismiss: false,
                                buttons: [
                                    {
                                        text: this.language.fnGetLanguage('cancel'),
                                        handler: () => {
                                            if (result.multy_language) {
                                                if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                                    this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                                    this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'));
                                                    this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_category_category__["a" /* CategoryPage */]);
                                                }
                                                else {
                                                    this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_20__pages_settings_settings__["a" /* SettingsPage */]);
                                                }
                                            }
                                            else {
                                                this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_category_category__["a" /* CategoryPage */]);
                                            }
                                        }
                                    },
                                    {
                                        text: this.language.fnGetLanguage('update'),
                                        handler: () => {
                                            this.market.open(result.package_name);
                                            this.platform.exitApp();
                                        }
                                    }
                                ]
                            });
                            alert.present();
                        }
                        else if (result.multy_language) {
                            if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'));
                                this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_category_category__["a" /* CategoryPage */]);
                            }
                            else {
                                this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_20__pages_settings_settings__["a" /* SettingsPage */]);
                            }
                        }
                        else {
                            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_category_category__["a" /* CategoryPage */]);
                        }
                    }, (error) => {
                        //error
                        this.splashScreen.hide();
                        if (error && error.msg) {
                            this.alertCtrl.create({
                                message: error.msg,
                                buttons: [
                                    {
                                        text: 'OK',
                                        handler: () => {
                                            this.platform.exitApp();
                                        }
                                    }
                                ]
                            }).present();
                        }
                        else {
                            this.alertCtrl.create({
                                message: 'Something happened wrong try again after sometimes.',
                                buttons: [
                                    {
                                        text: 'OK',
                                        handler: () => {
                                            this.platform.exitApp();
                                        }
                                    }
                                ]
                            }).present();
                        }
                    });
                }
            }
            else {
                this.cart.appUpdate = 1;
            }
        });
    }
    openPage(page) {
        if (page.hasChild) {
            __WEBPACK_IMPORTED_MODULE_12_jquery__("." + page.childSelector).fadeToggle(500);
        }
        else {
            for (let index = 0; index < this.pages.length; index++) {
                this.pages[index].active = false;
            }
            page.active = true;
            this.nav.setRoot(page.component, { 'data': page.data });
            this.menuCtrl.close();
        }
    }
    fnOpenCart() {
    }
    fnLogout() {
        let cart = localStorage.getItem('cart');
        let appLang = localStorage.getItem('appLang');
        let langArray = localStorage.getItem('langArray');
        let langList = localStorage.getItem('langList');
        localStorage.clear();
        localStorage.setItem('cart', cart);
        localStorage.setItem('appLang', appLang);
        localStorage.setItem('langArray', langArray);
        localStorage.setItem('langList', langList);
        this.event.publish('cart:itemAdded');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_category_category__["a" /* CategoryPage */]);
        this.menuCtrl.close();
        this.initializeApp();
        this.isLoggedIn = false;
    }
    menuOpened() {
        this.cart.isPlaceOrder = false;
    }
    requestPermission() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let hasPermission = yield this.badge.hasPermission();
                if (!hasPermission) {
                    yield this.badge.requestPermission();
                }
            }
            catch (e) {
            }
        });
    }
    setBadges(badgeNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.badge.set(badgeNumber);
            }
            catch (e) {
            }
        });
    }
    increaseBadges(badgeNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.badge.increase(Number(badgeNumber));
            }
            catch (e) {
            }
        });
    }
    clearBadges() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.badge.clear();
            }
            catch (e) {
            }
        });
    }
    fnGoToSettingPage() {
        let langList = JSON.parse(localStorage.getItem('langList'));
        let appLang = localStorage.getItem('appLang');
        let buttons = [];
        for (let index = 0; index < langList.length; index++) {
            buttons[index] = {
                text: langList[index].value,
                cssClass: appLang == langList[index].key ? 'selected' : 'notselected',
                icon: appLang == langList[index].key ? 'checkmark' : '',
                handler: () => {
                    if (appLang != langList[index].key) {
                        this.langService.fnSetAppLanguage(langList[index].key);
                        this.event.publish('appLangChange');
                        this.initializeApp();
                    }
                }
            };
        }
        let SortActionSheet = this.actionSheetCtrl.create({
            title: this.language.fnGetLanguage('chose_your_language'),
            buttons: buttons
        });
        SortActionSheet.present();
    }
    fnShareViaWhatsApp() {
        let sharenumber = localStorage.getItem('whatsapp_number');
        let app;
        if (this.platform.is('ios')) {
            app = 'whatsapp://';
        }
        else if (this.platform.is('android')) {
            app = 'com.whatsapp';
        }
        this.appAvailability.check(app)
            .then((yes) => {
            this.iab.create('whatsapp://send?phone=' + sharenumber, '_system');
        }, (no) => {
            this.alertCtrl.create({ title: this.language.fnGetLanguage('whatsapp_is_not_installed_in_your_device') }).present();
        });
        /**
                this.socialSharing.canShareVia('whatsapp').then((result: any) => {
                        let sharenumber = localStorage.getItem('whatsapp_number')
                        this.socialSharing.shareViaWhatsAppToReceiver(sharenumber, '\n')
                }, (error) => {
                        this.alertCtrl.create({ title: this.language.fnGetLanguage('whatsapp_is_not_installed_in_your_device')}).present()
                })
        */
    }
};
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/app/app.html"*/'<ion-menu [content]="content" (ionOpen)="menuOpened()">\n\n        <ion-content class="menucontent">\n                <div id="menu-material">\n\n                        <ion-item class="menu-header">\n                                <ion-avatar item-start style="background: #ffeabb;border-radius: 50%;" class="user-avatar">\n                                        <img [src]="chosenPicture || placeholder" onerror="this.src=\'assets/imgs/feather-icon.png\'" />\n                                </ion-avatar>\n                                <h2 style="color: #fff;">{{ name }}</h2>\n                                <p style="color: #fff"> {{ mobileNo }}</p>\n                        </ion-item>\n\n\n                </div>\n                <ion-list no-lines id="menulist" style="padding-top:10px; ">\n                        <div *ngFor="let p of pages" style="padding: 10px auto">\n                                <button ion-item detail-none (click)="openPage(p)" [ngClass]="{\'ourCollections\':p.parent == \'collections\',\'Profile\':p.parent == \'Profile\'}"\n                                        style="height: 4rem!important; min-height: 4rem">\n                                        <span [class.active]="p.hasChild">\n                                                <span *ngIf="p.icon">\n                                                        <ion-icon name="{{p.icon}}"></ion-icon> &nbsp;\n                                                </span>\n                                                {{ p.title }}\n                                        </span>\n                                </button>\n                        </div>\n                        <div style="padding: 10px auto">\n                                <button ion-item detail-none menuToggle (click)="fnGoToSettingPage()" style="height: 4rem!important; min-height: 3.5rem">\n                                        <ion-icon name="keypad"></ion-icon> &nbsp; {{language.fnGetLanguage(\'change_language\')}}\n                                </button>\n                        </div>\n                        <div style="padding: 10px auto">\n                                <button ion-item detail-none (click)="fnShareViaWhatsApp()" style="height: 4rem!important; min-height: 3.5rem">\n                                        <ion-icon name="logo-whatsapp" style="color: green"></ion-icon> &nbsp;  {{language.fnGetLanguage(\'whatsapp\')}}\n                                </button>\n                        </div>\n                        <div *ngIf="isLoggedIn" style="padding: 10px auto">\n                                <button ion-item detail-none (click)="fnLogout()" style="height: 4rem!important; min-height: 3.5rem">\n                                        <ion-icon name="md-log-out"></ion-icon>\n                                        &nbsp; {{language.fnGetLanguage(\'logout\')}}\n                                </button>\n                        </div>\n                </ion-list>\n        </ion-content>\n</ion-menu>\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="true"></ion-nav>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_16__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
        __WEBPACK_IMPORTED_MODULE_17__ionic_native_in_app_browser__["a" /* InAppBrowser */],
        __WEBPACK_IMPORTED_MODULE_13__providers_cart_cart__["a" /* CartProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_15__ionic_native_badge__["a" /* Badge */],
        __WEBPACK_IMPORTED_MODULE_14__ionic_native_onesignal__["a" /* OneSignal */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_16__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_18__ionic_native_app_availability__["a" /* AppAvailability */],
        __WEBPACK_IMPORTED_MODULE_16__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
        __WEBPACK_IMPORTED_MODULE_8__providers_custome_custome__["a" /* CustomeProvider */],
        __WEBPACK_IMPORTED_MODULE_19__ionic_native_market__["a" /* Market */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 347:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 348:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 349:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 350:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_module__ = __webpack_require__(256);
/* unused harmony reexport IonicImageViewerModule */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__src_image_viewer_component__ = __webpack_require__(141);
/* unused harmony reexport ImageViewerComponent */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__src_image_viewer_directive__ = __webpack_require__(257);
/* unused harmony reexport ImageViewerDirective */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__src_image_viewer_controller__ = __webpack_require__(136);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_3__src_image_viewer_controller__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__src_image_viewer__ = __webpack_require__(258);
/* unused harmony reexport ImageViewer */





//# sourceMappingURL=ionic-img-viewer.js.map

/***/ }),

/***/ 352:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageViewerImpl; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_operators__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__image_viewer_transitions__ = __webpack_require__(451);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();




var ImageViewerImpl = (function (_super) {
    __extends(ImageViewerImpl, _super);
    function ImageViewerImpl(app, component, opts, config) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, component, opts) || this;
        _this.app = app;
        config.setTransition('image-viewer-enter', __WEBPACK_IMPORTED_MODULE_3__image_viewer_transitions__["a" /* ImageViewerEnter */]);
        config.setTransition('image-viewer-leave', __WEBPACK_IMPORTED_MODULE_3__image_viewer_transitions__["b" /* ImageViewerLeave */]);
        _this.didLeave.subscribe(function () { return opts.onCloseCallback && opts.onCloseCallback(); });
        _this.willEnter.subscribe(function () {
            return _this.handleHighResImageLoad(opts.fullResImage);
        });
        return _this;
    }
    ImageViewerImpl.prototype.getTransitionName = function (direction) {
        return "image-viewer-" + (direction === 'back' ? 'leave' : 'enter');
    };
    ImageViewerImpl.prototype.present = function (navOptions) {
        if (navOptions === void 0) { navOptions = {}; }
        return this.app.present(this, navOptions);
    };
    ImageViewerImpl.prototype.handleHighResImageLoad = function (fullResImage) {
        var _this = this;
        if (!fullResImage) {
            return;
        }
        var image = new Image();
        image.src = fullResImage;
        if (!image.complete) {
            var onLoadObservable = __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].create(function (obs) {
                image.onload = function () {
                    obs.next(image);
                    obs.complete();
                };
            });
            // We want the animation to finish before replacing the pic
            // as the calculation has been done with the smaller image
            // AND, to avoid a flash if it loads "too quickly", wait at least 300ms after didEnter
            onLoadObservable
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_0_rxjs_operators__["zip"])(this.didEnter.pipe(Object(__WEBPACK_IMPORTED_MODULE_0_rxjs_operators__["delay"])(300))))
                .subscribe(function () {
                return _this.instance.updateImageSrcWithTransition(fullResImage);
            });
        }
        else {
            this.instance.updateImageSrc(fullResImage);
        }
    };
    return ImageViewerImpl;
}(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* ViewController */]));

//# sourceMappingURL=image-viewer-impl.js.map

/***/ }),

/***/ 451:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageViewerEnter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ImageViewerLeave; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(5);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var ImageViewerEnter = (function (_super) {
    __extends(ImageViewerEnter, _super);
    function ImageViewerEnter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ImageViewerEnter.prototype.init = function () {
        var css = this.plt.Css;
        var ele = this.enteringView.pageRef().nativeElement;
        var imgElement = ele.querySelector('img');
        var backdropElement = ele.querySelector('ion-backdrop');
        var fromPosition = this.enteringView.data.position;
        var toPosition = imgElement.getBoundingClientRect();
        var flipS = fromPosition.width / toPosition.width;
        var flipY = fromPosition.top - toPosition.top;
        var flipX = fromPosition.left - toPosition.left;
        var backdrop = new __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["c" /* Animation */](this.plt, backdropElement);
        var image = new __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["c" /* Animation */](this.plt, imgElement);
        // Using `Animation.beforeStyles()` here does not seems to work
        imgElement.style[css.transformOrigin] = '0 0';
        image.fromTo('translateY', flipY + "px", '0px')
            .fromTo('translateX', flipX + "px", '0px')
            .fromTo('scale', flipS, 1)
            .afterClearStyles([css.transformOrigin]);
        backdrop.fromTo('opacity', 0.01, 1);
        this.easing('ease-in-out')
            .duration(150)
            .add(backdrop)
            .add(image);
        var enteringPageEle = this.enteringView.pageRef().nativeElement;
        var enteringNavbarEle = enteringPageEle.querySelector('ion-navbar');
        var enteringBackBtnEle = enteringPageEle.querySelector('.back-button');
        var enteringNavBar = new __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["c" /* Animation */](this.plt, enteringNavbarEle);
        enteringNavBar.afterAddClass('show-navbar');
        this.add(enteringNavBar);
        var enteringBackButton = new __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["c" /* Animation */](this.plt, enteringBackBtnEle);
        this.add(enteringBackButton);
        enteringBackButton.afterAddClass('show-back-button');
    };
    return ImageViewerEnter;
}(__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["A" /* Transition */]));

var ImageViewerLeave = (function (_super) {
    __extends(ImageViewerLeave, _super);
    function ImageViewerLeave() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ImageViewerLeave.prototype.init = function () {
        var css = this.plt.Css;
        var ele = this.leavingView.pageRef().nativeElement;
        var imgElement = ele.querySelector('img');
        var backdropElement = ele.querySelector('ion-backdrop');
        var toPosition = this.leavingView.data.position;
        var fromPosition = imgElement.getBoundingClientRect();
        var flipS = toPosition.width / fromPosition.width;
        var flipY = toPosition.top - fromPosition.top;
        var flipX = toPosition.left - fromPosition.left;
        var backdropOpacity = backdropElement.style['opacity'];
        var backdrop = new __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["c" /* Animation */](this.plt, backdropElement);
        var image = new __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["c" /* Animation */](this.plt, imgElement);
        image.fromTo('translateY', 0 + "px", flipY + "px")
            .fromTo('translateX', "0px", flipX + "px")
            .fromTo('scale', 1, flipS)
            .beforeStyles((_a = {}, _a[css.transformOrigin] = '0 0', _a))
            .afterClearStyles([css.transformOrigin]);
        backdrop.fromTo('opacity', backdropOpacity, 0);
        this.easing('ease-in-out')
            .duration(150)
            .add(backdrop)
            .add(image);
        var _a;
    };
    return ImageViewerLeave;
}(__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["A" /* Transition */]));

//# sourceMappingURL=image-viewer-transitions.js.map

/***/ }),

/***/ 452:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageViewerSrcAnimation; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(5);

var ImageViewerSrcAnimation = (function () {
    function ImageViewerSrcAnimation(platform, image) {
        this.element = image.nativeElement;
        this.imageAnimation = new __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["c" /* Animation */](platform, image);
    }
    ImageViewerSrcAnimation.prototype.scaleFrom = function (lowResImgWidth) {
        var highResImgWidth = this.element.clientWidth;
        var imageTransition = this.imageAnimation
            .fromTo('scale', lowResImgWidth / highResImgWidth, 1)
            .duration(100)
            .easing('ease-in-out')
            .play();
    };
    return ImageViewerSrcAnimation;
}());

//# sourceMappingURL=image-viewer-src-animation.js.map

/***/ }),

/***/ 453:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageViewerTransitionGesture; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular_util_dom__ = __webpack_require__(17);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();


var HAMMER_THRESHOLD = 10;
var MAX_ATTACK_ANGLE = 45;
var DRAG_THRESHOLD = 70;
var ImageViewerTransitionGesture = (function (_super) {
    __extends(ImageViewerTransitionGesture, _super);
    function ImageViewerTransitionGesture(platform, component, domCtrl, renderer, cb) {
        var _this = _super.call(this, platform, component.getNativeElement(), {
            maxAngle: MAX_ATTACK_ANGLE,
            threshold: HAMMER_THRESHOLD,
            gesture: component._gestureCtrl.createGesture({ name: 'image-viewer' }),
            direction: 'y',
            domController: domCtrl
        }) || this;
        _this.component = component;
        _this.renderer = renderer;
        _this.cb = cb;
        _this.translationY = 0;
        _this.imageContainer = component.getNativeElement().querySelector('.image');
        _this.backdrop = component.getNativeElement().querySelector('ion-backdrop');
        _this.listen();
        return _this;
    }
    // As we handle both pinch and drag, we have to make sure we don't drag when we are trying to pinch
    // As we handle both pinch and drag, we have to make sure we don't drag when we are trying to pinch
    ImageViewerTransitionGesture.prototype.isPinching = 
    // As we handle both pinch and drag, we have to make sure we don't drag when we are trying to pinch
    function (ev) {
        return ev.touches && ev.touches.length > 1;
    };
    ImageViewerTransitionGesture.prototype.onDragStart = function (ev) {
        ev.preventDefault();
        if (this.isPinching(ev)) {
            this.abort(ev);
        }
        var coord = Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular_util_dom__["f" /* pointerCoord */])(ev);
        this.startY = coord.y;
        return true;
    };
    ImageViewerTransitionGesture.prototype.canStart = function (ev) {
        return !this.component.isZoomed && !this.isPinching(ev);
    };
    ImageViewerTransitionGesture.prototype.onDragMove = function (ev) {
        var _this = this;
        if (this.isPinching(ev)) {
            this.abort(ev);
        }
        var coord = Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular_util_dom__["f" /* pointerCoord */])(ev);
        this.translationY = coord.y - this.startY;
        this.opacity = Math.max(1 - Math.abs(this.translationY) / (10 * DRAG_THRESHOLD), .5);
        this.plt.raf(function () {
            _this.renderer.setElementStyle(_this.imageContainer, _this.plt.Css.transform, "translateY(" + _this.translationY + "px)");
            _this.renderer.setElementStyle(_this.backdrop, 'opacity', _this.opacity.toString());
        });
        return true;
    };
    ImageViewerTransitionGesture.prototype.onDragEnd = function (ev) {
        if (Math.abs(this.translationY) > DRAG_THRESHOLD) {
            this.cb();
        }
        else {
            var imageContainerAnimation = new __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["c" /* Animation */](this.plt, this.imageContainer);
            var backdropAnimation = new __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["c" /* Animation */](this.plt, this.backdrop);
            backdropAnimation.fromTo('opacity', this.opacity, '1');
            imageContainerAnimation.fromTo('translateY', this.translationY + "px", '0px');
            new __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["c" /* Animation */](this.plt)
                .easing('ease-in')
                .duration(150)
                .add(backdropAnimation)
                .add(imageContainerAnimation)
                .play();
        }
        return true;
    };
    return ImageViewerTransitionGesture;
}(__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["w" /* PanGesture */]));

//# sourceMappingURL=image-viewer-transition-gesture.js.map

/***/ }),

/***/ 454:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageViewerZoomGesture; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular_gestures_hammer__ = __webpack_require__(234);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();


var MAX_SCALE = 4;
var ImageViewerZoomGesture = (function (_super) {
    __extends(ImageViewerZoomGesture, _super);
    function ImageViewerZoomGesture(component, element, platform, renderer) {
        var _this = _super.call(this, element.nativeElement) || this;
        _this.component = component;
        _this.platform = platform;
        _this.renderer = renderer;
        _this.adjustScale = 1;
        _this.adjustDeltaX = 0;
        _this.adjustDeltaY = 0;
        _this.currentScale = 1;
        _this.currentDeltaX = 0;
        _this.currentDeltaY = 0;
        _this.allowedXMargin = 0;
        _this.allowedYMargin = 0;
        // Force both directions after super to avoid override allowing only one direction
        // Force both directions after super to avoid override allowing only one direction
        _this.options({ direction: __WEBPACK_IMPORTED_MODULE_1_ionic_angular_gestures_hammer__["a" /* DIRECTION_HORIZONTAL */] | __WEBPACK_IMPORTED_MODULE_1_ionic_angular_gestures_hammer__["b" /* DIRECTION_VERTICAL */] });
        _this.listen();
        _this.on('pinch', function (e) { return _this.onPinch(e); });
        _this.on('pinchend', function (e) { return _this.onPinchEnd(e); });
        _this.on('panstart', function (e) { return _this.onPanStart(e); });
        _this.on('pan', function (e) { return _this.onPan(e); });
        _this.on('panend', function (e) { return _this.onPanEnd(e); });
        _this.on('doubletap', function (e) { return _this.onDoubleTap(e); });
        return _this;
    }
    ImageViewerZoomGesture.prototype.onPinch = function (event) {
        this.component.dragGesture.abort(event);
        this.currentScale = Math.max(1, Math.min(MAX_SCALE, this.adjustScale * event.scale));
        this.currentDeltaX = this.adjustDeltaX + (event.deltaX / this.currentScale);
        this.currentDeltaY = this.adjustDeltaY + (event.deltaY / this.currentScale);
        this.setImageContainerTransform();
    };
    ImageViewerZoomGesture.prototype.onPinchEnd = function (event) {
        this.component.isZoomed = (this.currentScale !== 1);
        if (!this.component.isZoomed) {
            new __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["c" /* Animation */](this.platform, this.element)
                .fromTo('translateX', this.currentDeltaX + "px", '0')
                .fromTo('translateY', this.currentDeltaY + "px", '0')
                .easing('ease-in')
                .duration(50)
                .play();
            this.currentDeltaX = 0;
            this.currentDeltaY = 0;
        }
        // Saving the final transforms for adjustment next time the user interacts.
        this.adjustScale = this.currentScale;
        this.adjustDeltaX = this.currentDeltaX;
        this.adjustDeltaY = this.currentDeltaY;
    };
    ImageViewerZoomGesture.prototype.onPanStart = function (event) {
        if (!this.component.isZoomed) {
            return;
        }
        var originalImageWidth = this.element.offsetWidth;
        var originalImageHeight = this.element.offsetHeight;
        this.allowedXMargin = ((originalImageWidth * this.currentScale) - originalImageWidth) / 4;
        this.allowedYMargin = ((originalImageHeight * this.currentScale) - originalImageHeight) / 4;
    };
    ImageViewerZoomGesture.prototype.onPan = function (event) {
        if (!this.component.isZoomed) {
            return;
        }
        this.currentDeltaX = this.boundAdjustement(Math.floor(this.adjustDeltaX + event.deltaX / this.currentScale), this.allowedXMargin);
        this.currentDeltaY = this.boundAdjustement(Math.floor(this.adjustDeltaY + event.deltaY / this.currentScale), this.allowedYMargin);
        this.setImageContainerTransform();
    };
    ImageViewerZoomGesture.prototype.boundAdjustement = function (adjustement, bound) {
        if (adjustement > bound || adjustement < -bound) {
            return Math.min(bound, Math.max(adjustement, -bound));
        }
        return adjustement;
    };
    ImageViewerZoomGesture.prototype.onPanEnd = function (event) {
        if (!this.component.isZoomed) {
            return;
        }
        this.adjustDeltaX = this.currentDeltaX;
        this.adjustDeltaY = this.currentDeltaY;
    };
    ImageViewerZoomGesture.prototype.onDoubleTap = function (event) {
        this.component.isZoomed = !this.component.isZoomed;
        if (this.component.isZoomed) {
            this.currentScale = 2;
        }
        else {
            this.currentScale = 1;
            this.adjustDeltaX = this.currentDeltaX = 0;
            this.adjustDeltaY = this.currentDeltaY = 0;
        }
        this.adjustScale = this.currentScale;
        this.setImageContainerTransform();
    };
    ImageViewerZoomGesture.prototype.setImageContainerTransform = function () {
        var transforms = [];
        transforms.push("scale(" + this.currentScale + ")");
        transforms.push("translate(" + this.currentDeltaX + "px, " + this.currentDeltaY + "px)");
        this.renderer.setElementStyle(this.element, this.platform.Css.transform, transforms.join(' '));
    };
    return ImageViewerZoomGesture;
}(__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["j" /* Gesture */]));

//# sourceMappingURL=image-viewer-zoom-gesture.js.map

/***/ }),

/***/ 461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SplashPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_platform_platform__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_app_menu_controller__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_market__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__settings_settings__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__category_category__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let SplashPage = class SplashPage {
    constructor(navCtrl, navParams, platform, language, menuCtrl, alertCtrl, service, cart, market, langService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.platform = platform;
        this.language = language;
        this.menuCtrl = menuCtrl;
        this.alertCtrl = alertCtrl;
        this.service = service;
        this.cart = cart;
        this.market = market;
        this.langService = langService;
    }
    ionViewDidLoad() {
        this.menuCtrl.enable(false);
        this.platform.ready().then(() => {
            if (localStorage.getItem('notificationOpen') == '1') {
                this.fnGoToProductPageForNotification();
            }
            else {
                if (this.cart.appUpdate == 0) {
                    if (this.platform.is('ios')) {
                        this.service.post('startup/check_apple_version_update', { version: '1.0.3' })
                            .then((result) => {
                            if (result && result.maintenance && result.maintenance.apple) {
                                let alert = this.alertCtrl.create({
                                    subTitle: result.maintenance.msg,
                                    enableBackdropDismiss: false,
                                    buttons: [
                                        {
                                            text: this.language.fnGetLanguage('close'),
                                            handler: () => {
                                                this.platform.exitApp();
                                            }
                                        }
                                    ]
                                });
                                alert.present();
                                alert.onDidDismiss(() => {
                                    this.platform.exitApp();
                                });
                            }
                            else if (result.update_status == 2 || result.update_status == '2') {
                                let alert = this.alertCtrl.create({
                                    subTitle: this.language.fnGetLanguage('please_update_an_app'),
                                    enableBackdropDismiss: false,
                                    buttons: [
                                        {
                                            text: this.language.fnGetLanguage('update'),
                                            handler: () => {
                                                this.market.open(result.package_name).then((r) => {
                                                    // this.market.open()
                                                });
                                            }
                                        }
                                    ]
                                });
                                alert.present();
                                alert.onDidDismiss(() => {
                                    this.platform.exitApp();
                                });
                            }
                            else if (result.update_status == 1 || result.update_status == '1') {
                                this.cart.appUpdate = 1;
                                let alert = this.alertCtrl.create({
                                    subTitle: this.language.fnGetLanguage('new_update_available') + '.',
                                    enableBackdropDismiss: false,
                                    buttons: [
                                        {
                                            text: this.language.fnGetLanguage('close'),
                                            handler: () => {
                                                if (result.multy_language) {
                                                    if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                                        this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                                        this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'));
                                                        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__category_category__["a" /* CategoryPage */]);
                                                    }
                                                    else {
                                                        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__settings_settings__["a" /* SettingsPage */]);
                                                    }
                                                }
                                                else {
                                                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__category_category__["a" /* CategoryPage */]);
                                                }
                                            }
                                        },
                                        {
                                            text: this.language.fnGetLanguage('update'),
                                            handler: () => {
                                                this.market.open('com.brijwasibj').then((r) => {
                                                    // open a play store
                                                }, (e) => {
                                                });
                                            }
                                        }
                                    ]
                                });
                                alert.present();
                            }
                            else if (result.multy_language) {
                                if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                    this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                    this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'));
                                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__category_category__["a" /* CategoryPage */]);
                                }
                                else {
                                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__settings_settings__["a" /* SettingsPage */]);
                                }
                            }
                            else {
                                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__category_category__["a" /* CategoryPage */]);
                            }
                        }, (error) => {
                            // error
                            if (error && error.msg) {
                                this.alertCtrl.create({
                                    message: error.msg,
                                    buttons: [
                                        {
                                            text: 'OK',
                                            handler: () => {
                                                this.platform.exitApp();
                                            }
                                        }
                                    ]
                                }).present();
                            }
                        });
                    }
                    else if (this.platform.is('android')) {
                        this.service.post('startup/check_android_version_update', { version: '1.0.3' })
                            .then((result) => {
                            if (result && result.maintenance && result.maintenance.android) {
                                let alert = this.alertCtrl.create({
                                    subTitle: result.maintenance.msg,
                                    enableBackdropDismiss: false,
                                    buttons: [
                                        {
                                            text: this.language.fnGetLanguage('close'),
                                            handler: () => {
                                                this.platform.exitApp();
                                            }
                                        }
                                    ]
                                });
                                alert.present();
                                alert.onDidDismiss(() => {
                                    this.platform.exitApp();
                                });
                            }
                            else if (result.update_status == 2 || result.update_status == '2') {
                                let alert = this.alertCtrl.create({
                                    subTitle: this.language.fnGetLanguage('please_update_an_app'),
                                    enableBackdropDismiss: false,
                                    buttons: [
                                        {
                                            text: this.language.fnGetLanguage('close'),
                                            handler: () => {
                                                this.platform.exitApp();
                                            }
                                        },
                                        {
                                            text: 'Update',
                                            handler: () => {
                                                this.market.open(result.package_name);
                                                this.platform.exitApp();
                                            }
                                        }
                                    ]
                                });
                                alert.present();
                                alert.onDidDismiss(() => {
                                    this.platform.exitApp();
                                });
                            }
                            else if (result.update_status == 1 || result.update_status == '1') {
                                this.cart.appUpdate = 1;
                                let alert = this.alertCtrl.create({
                                    subTitle: this.language.fnGetLanguage('new_update_available') + '.',
                                    enableBackdropDismiss: false,
                                    buttons: [
                                        {
                                            text: this.language.fnGetLanguage('cancel'),
                                            handler: () => {
                                                if (result.multy_language) {
                                                    if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                                        this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                                        this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'));
                                                        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__category_category__["a" /* CategoryPage */]);
                                                    }
                                                    else {
                                                        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__settings_settings__["a" /* SettingsPage */]);
                                                    }
                                                }
                                                else {
                                                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__category_category__["a" /* CategoryPage */]);
                                                }
                                            }
                                        },
                                        {
                                            text: this.language.fnGetLanguage('update'),
                                            handler: () => {
                                                this.market.open(result.package_name);
                                                this.platform.exitApp();
                                            }
                                        }
                                    ]
                                });
                                alert.present();
                            }
                            else if (result.multy_language) {
                                if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                    this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                    this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'));
                                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__category_category__["a" /* CategoryPage */]);
                                }
                                else {
                                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__settings_settings__["a" /* SettingsPage */]);
                                }
                            }
                            else {
                                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__category_category__["a" /* CategoryPage */]);
                            }
                        }, (error) => {
                            //error
                            if (error && error.msg) {
                                this.alertCtrl.create({
                                    message: error.msg,
                                    buttons: [
                                        {
                                            text: 'OK',
                                            handler: () => {
                                                this.platform.exitApp();
                                            }
                                        }
                                    ]
                                }).present();
                            }
                            else {
                                this.alertCtrl.create({
                                    message: 'Something happened wrong try again after sometimes.',
                                    buttons: [
                                        {
                                            text: 'OK',
                                            handler: () => {
                                                this.platform.exitApp();
                                            }
                                        }
                                    ]
                                }).present();
                            }
                        });
                    }
                }
                else {
                    this.cart.appUpdate = 1;
                }
            }
        });
    }
    fnGoToProductPageForNotification() {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__category_category__["a" /* CategoryPage */], { 'data': this.navParams.get('data') });
    }
};
SplashPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-splash',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/splash/splash.html"*/'<!--\n  Generated template for the SplashPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n\n<ion-content>\n<div class="background">\n\n</div>\n</ion-content>\n'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/splash/splash.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular_platform_platform__["a" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_app_menu_controller__["a" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_custome_custome__["a" /* CustomeProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_cart_cart__["a" /* CartProvider */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_native_market__["a" /* Market */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */]])
], SplashPage);

//# sourceMappingURL=splash.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__category_category__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(246);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__change_mobile_no_change_mobile_no__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shipping_address_shipping_address__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__cart_cart__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the UserDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let UserDetailPage = class UserDetailPage {
    constructor(navCtrl, navParams, language, alertCtrl, service, transfer, camera, cart, loadingCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.language = language;
        this.alertCtrl = alertCtrl;
        this.service = service;
        this.transfer = transfer;
        this.camera = camera;
        this.cart = cart;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.name = '';
        this.address_line_1 = '';
        this.address_line_2 = '';
        this.phone_no = '';
        this.city = '';
        this.state = '';
        this.country = '';
        this.email = '';
        this.pincode = '';
        this.changeMobile = false;
        this.mobileno = '';
    }
    ionViewDidLoad() {
        this.navBar.backButtonClick = (e) => {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__cart_cart__["a" /* CartPage */]);
        };
        if (localStorage.getItem('mobile_no')) {
            this.mobileno = localStorage.getItem('mobile_no');
        }
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.changeMobile = this.navParams.get('data');
        this.service.post('account/get_account_details', {})
            .then((result) => {
            if (result.user.name) {
                this.name = result.user.name;
            }
            if (result.user.phone_no) {
                this.phone_no = result.user.phone_no;
            }
            if (result.user.mobile_no) {
                this.mobileno = result.user.mobile_no;
            }
            if (result.user.photo) {
                this.imageFileName = result.user.photo;
            }
            if (result.user.address_line_1) {
                this.address_line_1 = result.user.address_line_1;
            }
            if (result.user.address_line_2) {
                this.address_line_2 = result.user.address_line_2;
            }
            if (result.user.city) {
                this.city = result.user.city;
            }
            if (result.user.state) {
                this.state = result.user.state;
            }
            if (result.user.country) {
                this.country = result.user.country;
            }
            if (result.user.email_id) {
                this.email = result.user.email_id;
            }
            if (result.user.pincode) {
                this.pincode = result.user.pincode;
            }
            loader.dismiss();
        }, (error) => {
            loader.dismiss();
        });
    }
    fnSubmitUserData() {
        if (!this.name || this.name == '') {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('enter_valid_name') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        if (this.email) {
            let re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            if (!re.test(this.email)) {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('enter_valid_email') + '.',
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
        }
        if (this.phone_no) {
            var reg = /^\d+$/;
            if (!reg.test(this.phone_no)) {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('enter_valid_phone_no') + '.',
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
        }
        let form = {
            'name': this.name,
            'address_line_1': this.address_line_1,
            'address_line_2': this.address_line_2,
            'city': this.city,
            'state': this.state,
            'country': this.country,
            'email_id': this.email,
            'phone_no': this.phone_no,
            'pincode': this.pincode,
        };
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.service.post('account/change_account_details', form).then((result) => {
            loader.dismiss();
            localStorage.setItem('customer_id', result.user.customer_id);
            localStorage.setItem('name', result.user.name);
            localStorage.setItem('mobile_no', result.user.mobile_no);
            localStorage.setItem('address_line_1', result.user.address_line_1);
            localStorage.setItem('address_line_2', result.user.address_line_2);
            localStorage.setItem('city', result.user.city);
            localStorage.setItem('state', result.user.state);
            localStorage.setItem('country', result.user.country);
            localStorage.setItem('email_id', result.user.email_id);
            localStorage.setItem('phone_no', result.user.phone_no);
            localStorage.setItem('photo', result.user.photo);
            localStorage.setItem('created_on', result.user.created_on);
            localStorage.setItem('status', result.user.status);
            if (this.cart.isPlaceOrder) {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__shipping_address_shipping_address__["a" /* ShippingAddressPage */]);
            }
            else {
                if (result.message) {
                    let alert = this.alertCtrl.create({
                        subTitle: result.message,
                        buttons: [this.language.fnGetLanguage('title_ok')]
                    });
                    alert.present();
                    alert.onDidDismiss(() => {
                        if (this.cart.isPlaceOrder) {
                            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__shipping_address_shipping_address__["a" /* ShippingAddressPage */]);
                        }
                        else {
                            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__category_category__["a" /* CategoryPage */]);
                        }
                    });
                }
            }
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
            }
            else {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
            }
        });
    }
    getImage() {
        const options = {
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
        };
        this.camera.getPicture(options).then((imageData) => {
            this.imageURI = imageData;
            this.uploadFile();
        }, (err) => {
            this.presentToast(err);
        });
    }
    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(() => {
        });
        toast.present();
    }
    uploadFile() {
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        const fileTransfer = this.transfer.create();
        let options = {
            chunkedMode: false,
            fileKey: 'userfile',
            fileName: 'userfile',
            mimeType: "image/jpeg",
            headers: {
                'authorization': localStorage.getItem('token')
            }
        };
        let form = new FormData();
        form.append('userfile', this.imageURI);
        fileTransfer.upload(this.imageURI, this.service.baseUrl + 'account/image_upload', options)
            .then((data) => {
            let response = JSON.parse(data.response);
            if (response.status == 1) {
                this.imageFileName = response.img;
                loader.dismiss();
                this.presentToast(this.language.fnGetLanguage("image_uploaded_successfully"));
            }
            else {
                loader.dismiss();
                if (response.error) {
                    let alert = this.alertCtrl.create({
                        subTitle: response.error,
                        buttons: [this.language.fnGetLanguage('title_ok')]
                    });
                    alert.present();
                }
                else {
                    let alert = this.alertCtrl.create({
                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                        buttons: [this.language.fnGetLanguage('title_ok')]
                    });
                    alert.present();
                }
            }
        }, (err) => {
            loader.dismiss();
            this.presentToast(err);
        });
    }
    fnchangeMobileNo() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__change_mobile_no_change_mobile_no__["a" /* ChangeMobileNoPage */]);
    }
};
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Navbar */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Navbar */])
], UserDetailPage.prototype, "navBar", void 0);
UserDetailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-user-detail',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/user-detail/user-detail.html"*/'<!--\n  Generated template for the UserDetailPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n        <ion-navbar>\n                <button menuToggle ion-button icon-only>\n                        <ion-icon color="dark" name="menu"></ion-icon>\n                </button>\n                <ion-title>{{language.fnGetLanguage(\'title_profile\')}}</ion-title>\n                <ion-buttons end showWhen="android" style="opacity: 0;">\n                        <button ion-button icon-only id="notification-button" style="opacity: 0;">\n                                <ion-icon color="dark" name="cart" style="opacity: 0;"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0" style="opacity: 0;">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n        <ion-row>\n                <ion-col col-4>\n\n                </ion-col>\n\n                <ion-col style="text-align: center" class="profile_photo" col-4 [ngStyle]="{\'background-image\': \'url(\' + imageFileName + \')\'}">\n                        <button ion-button small color="light" (click)="getImage()" class="upload_image">\n                                <ion-icon name="camera"></ion-icon>\n                        </button>\n\n                        <img src="assets/img_logo/create-profile.png" *ngIf="!imageFileName" alt="Ionic File" style="border-radius: 50%;" />\n                </ion-col>\n                <ion-col col-4>\n\n                </ion-col>\n        </ion-row>\n        <ion-list>\n\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'name\')}}*</span>\n                        <ion-input col-12 type="text" placeholder="{{language.fnGetLanguage(\'name\')}}" class="inputarea" [(ngModel)]="name"></ion-input>\n                </ion-row>\n                <ion-row class="mt20px" *ngIf="changeMobile">\n                        <span col-12 class="spanPadding">{{language.fnGetLanguage(\'mobile_no\')}}.</span>\n                        <ion-input col-6 type="tel" class="inputarea inputareamobile " [(ngModel)]="mobileno" style="max-width:100%!important"\n                                disabled></ion-input>\n                        <a col-6 small href="javascript:void(0)" (click)="fnchangeMobileNo()" style="padding-left: 10px; padding-top: 15px;text-decoration: none">\n                                {{language.fnGetLanguage(\'change_mobile_no\')}}.\n                        </a>\n                </ion-row>\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'email_id\')}}</span>\n                        <ion-input col-12 type="text" placeholder="{{language.fnGetLanguage(\'your_email_id\')}}" class="inputarea"\n                                [(ngModel)]="email"></ion-input>\n                </ion-row>\n\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'address_line\')}} 1</span>\n                        <ion-input col-12 type="text" placeholder="{{language.fnGetLanguage(\'enter_address_line\')}} 1"\n                                class="inputarea" [(ngModel)]="address_line_1"></ion-input>\n                </ion-row>\n\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'address_line\')}} 2</span>\n                        <ion-input col-12 type="text" placeholder="{{language.fnGetLanguage(\'enter_address_line\')}} 2"\n                                class="inputarea" [(ngModel)]="address_line_2"></ion-input>\n                </ion-row>\n\n\n\n\n                <ion-row class="mt20px">\n                        <ion-col col-6>\n                                <span class="spanPadding">{{language.fnGetLanguage(\'city\')}}</span>\n                                <ion-input col-12 type="text" placeholder="{{language.fnGetLanguage(\'enter_city\')}}"\n                                        class="inputarea" [(ngModel)]="city"></ion-input>\n                        </ion-col>\n                        <ion-col col-6>\n                                <span class="spanPadding">{{language.fnGetLanguage(\'pincode\')}}</span>\n                                <ion-input col-12 type="tel" placeholder="{{language.fnGetLanguage(\'enter_pincode\')}} "\n                                        class="inputarea" [(ngModel)]="pincode"></ion-input>\n                        </ion-col>\n\n                </ion-row>\n\n                <ion-row class="mt20px">\n                        <ion-col col-6>\n                                <span class="spanPadding">{{language.fnGetLanguage(\'state\')}}</span>\n                                <ion-input col-12 type="text" placeholder="{{language.fnGetLanguage(\'enter_state\')}} "\n                                        class="inputarea" [(ngModel)]="state"></ion-input>\n                        </ion-col>\n                        <ion-col col-6>\n                                <span class="spanPadding">{{language.fnGetLanguage(\'country\')}}</span>\n                                <ion-input col-12 type="text" placeholder="{{language.fnGetLanguage(\'enter_country\')}} "\n                                        class="inputarea" [(ngModel)]="country"></ion-input>\n                        </ion-col>\n\n                </ion-row>\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'phone_number\')}}</span>\n                        <ion-input col-12 type="tel" placeholder="{{language.fnGetLanguage(\'enter_phone_no\')}}." class="inputarea"\n                                [(ngModel)]="phone_no"></ion-input>\n                </ion-row>\n        </ion-list>\n\n</ion-content>\n<ion-footer>\n        <button ion-button block color="golden" large (click)="fnSubmitUserData()" style="margin: 0">\n                <span *ngIf="!cart.isPlaceOrder">\n                        {{language.fnGetLanguage(\'submit\')}}\n                </span>\n                <span *ngIf="cart.isPlaceOrder">\n                        {{language.fnGetLanguage(\'title_next\')}}\n                </span>\n        </button>\n</ion-footer>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/user-detail/user-detail.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_10__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__["a" /* CustomeProvider */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_8__providers_cart_cart__["a" /* CartProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */]])
], UserDetailPage);

//# sourceMappingURL=user-detail.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cart_cart__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__image_viewer_image_viewer__ = __webpack_require__(251);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__category_category__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let ProductPage = class ProductPage {
    constructor(navCtrl, navParams, toast, event, platform, language, service, cart, modal, menu, loadingCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toast = toast;
        this.event = event;
        this.platform = platform;
        this.language = language;
        this.service = service;
        this.cart = cart;
        this.modal = modal;
        this.menu = menu;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.lazyLoadding = localStorage.getItem('lazyLoadding');
        this.height = window.innerHeight * 6 / 10;
        this.appLang = localStorage.getItem('appLang');
        this.inputWidth = 65;
        this.showLoader = false;
    }
    ionViewDidLoad() {
        this.appLang = localStorage.getItem('appLang');
        if (localStorage.getItem('notificationOpen') == '1') {
            this.menu.enable(false);
            this.navBar.backButtonClick = (e) => {
                this.menu.enable(true);
                this.navCtrl.pop();
            };
            localStorage.removeItem('notificationOpen');
        }
        this.cartItem = this.cart.fnGetCountOfCart();
        this.event.subscribe('cart:itemAdded', () => {
            this.cartItem = this.cart.fnGetCountOfCart();
        });
        this.event.subscribe('appLangChange', () => {
            this.appLang = this.language.fnGetSelectedAppLang();
        });
        this.event.subscribe('note:changed', () => {
            var data = this.cart.fnGetCart();
            let jj = 1;
            if (data) {
                for (let index = 0; index < data.length; index++) {
                    if (data[index].item_id == this.item.item_id) {
                        this.item.note = data[index].note;
                        jj = 0;
                    }
                }
            }
            if (jj) {
                this.item.note = '';
            }
        });
        let item = this.navParams.get('data');
        let form = {
            'item_id': item.item_id
        };
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.service.post('item/get_item_details', form).then((result) => {
            this.item = result.item;
            this.item.note = '';
            this.item.qty = parseInt(this.item.min_order_qty);
            console.log(result);
            var data = this.cart.fnGetCart();
            if (data) {
                for (let index = 0; index < data.length; index++) {
                    if (data[index].item_id == result.item.item_id) {
                        this.item.note = data[index].note;
                    }
                }
            }
            this.showLoader = true;
            loader.dismiss();
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
        });
    }
    fnOpenCart() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__cart_cart__["a" /* CartPage */]);
    }
    fnGotoNotificationPage() {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__category_category__["a" /* CategoryPage */]);
    }
    fnAddToCart() {
        var data = this.cart.fnGetCart();
        var j = 1;
        var selecdItemIndex;
        if (data) {
            for (let index = 0; index < data.length; index++) {
                if (data[index].item_id == this.item.item_id) {
                    j = 0;
                    selecdItemIndex = index;
                }
            }
        }
        if (j == 1) {
            this.cart.fnAddToCart(this.item);
            this.event.publish('cart:itemAdded');
            this.toast.create({
                message: this.language.fnGetLanguage("item_is_added_in_cart") + '.',
                duration: 3000,
                position: 'bottom',
                cssClass: 'cstToast'
            }).present();
        }
        else {
            let flag = 0;
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('item_is_already_in_cart') + '.' + this.language.fnGetLanguage('do_you_want_to_proceed') + '?',
                buttons: [
                    {
                        text: this.language.fnGetLanguage('cancel'),
                        handler: () => {
                            flag = 0;
                            return true;
                        }
                    },
                    {
                        text: this.language.fnGetLanguage('title_ok'),
                        handler: () => {
                            flag = 1;
                            return true;
                        }
                    }
                ]
            });
            alert.present();
            alert.onDidDismiss((result) => {
                if (flag == 1) {
                    data[selecdItemIndex].qty = parseInt(data[selecdItemIndex].qty) + parseInt(this.item.qty);
                    let item = JSON.parse(JSON.stringify(data[selecdItemIndex]));
                    item.note = this.item.note;
                    this.cart.fnRemoveFromCart(selecdItemIndex);
                    item.min_order_qty = parseInt(item.min_order_qty);
                    this.cart.fnAddToCart(item);
                    this.event.publish('cart:itemAdded');
                }
            });
        }
    }
    fnOpenImageViewer(data) {
        this.modal.create(__WEBPACK_IMPORTED_MODULE_5__image_viewer_image_viewer__["a" /* ImageViewerPage */], { data: this.item }).present().then((result) => {
        }, (error) => {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
        }).catch((error) => {
        });
    }
    increment() {
        this.item.qty++;
    }
    fnQtyChange() {
        if (this.item && this.item.qty && this.item.qty > 0) {
            if (parseInt(this.item.qty) < parseInt(this.item.min_order_qty)) {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('you_can_not_input_order_quantity_less_than_') + this.item.min_order_qty,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                alert.onDidDismiss((result) => {
                    this.item.qty = JSON.parse(JSON.stringify(this.item.min_order_qty));
                });
            }
        }
        else {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('please_enter_a_quantity_greater_than_or_equal_to_') + this.item.min_order_qty,
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            alert.onDidDismiss((result) => {
                this.item.qty = JSON.parse(JSON.stringify(this.item.min_order_qty));
            });
        }
    }
    decrement() {
        if (this.item.qty > this.item.min_order_qty) {
            this.item.qty--;
        }
        else {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('you_can_not_order_less_than_minimum_order_quantity'),
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
        }
    }
};
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Navbar */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Navbar */])
], ProductPage.prototype, "navBar", void 0);
ProductPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-product',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/product/product.html"*/'<!--\n  Generated template for the ProductPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n        <ion-navbar style="color: black;">\n                <button menuToggle ion-button icon-only *ngIf="navCtrl.canGoBack()">\n                        <ion-icon color="dark" name="menu"></ion-icon>\n                </button>\n                <button (click)="fnGotoNotificationPage()" ion-button icon-only clear *ngIf="!navCtrl.canGoBack()">\n                        <ion-icon color="dark" name="arrow-back"></ion-icon>\n                </button>\n                <ion-title>\n                        <div *ngIf="item && item.category_name">\n                                <span *ngIf="item.category_name_lang[appLang]">\n                                        {{item.category_name_lang[appLang]}}\n                                </span>\n                                <span *ngIf="!item.category_name_lang[appLang]">\n                                        {{ item.category_name }}\n                                </span>\n                        </div>\n                </ion-title>\n                <ion-buttons end>\n                        <button (click)="fnOpenCart()" ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{ cartItem }}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n        <div *ngIf="showLoader && item">\n\n                <div *ngIf="item && item.gallary && item.gallary.length>0">\n                        <div class="mt20">\n                                <ion-slides pager="true">\n                                        <!-- <ion-slide *ngFor="let img of item.gallary;let i=index" (click)="fnOpenImageViewer(item.gallary)">\n                                                <img src="{{img.l}}" [style.height.px]="height" id="img{{i}}">\n                                        </ion-slide> -->\n\n                                        <ion-slide *ngFor="let img of item.gallary;let i=index" (click)="fnOpenImageViewer(item.gallary)" lazy-load-images>\n                                                <img [attr.data-src]="img.l" [style.height.px]="height" id="img{{i}}" *ngIf="lazyLoadding">\n                                                <img [src]="img.l" [style.height.px]="height" id="img{{i}}" *ngIf="!lazyLoadding">\n                                               \n                                        </ion-slide>\n                                </ion-slides>\n                        </div>\n                </div>\n                <ion-row *ngIf="item && item.item_name" class="mt10">\n                        <ion-col>\n                                <p style="text-align: center;font-size: 20px; margin: 0" *ngIf="item.item_name_lang[appLang]">\n                                        {{item.item_name_lang[appLang]}}\n                                </p>\n                                <p style="text-align: center;font-size: 20px; margin: 0" *ngIf="!item.item_name_lang[appLang]">\n                                        {{ item.item_name }}\n                                </p>\n                        </ion-col>\n                </ion-row>\n                <div *ngIf="item" class="mt10">\n                        <ion-list>\n                                <ion-item class="customBorder">\n                                        <ion-label>\n                                                {{language.fnGetLanguage(\'code\')}}\n                                        </ion-label>\n                                        <div item-content>\n                                                {{ item.item_code }}\n                                        </div>\n                                </ion-item>\n\n                                <ion-item *ngIf="item.size">\n                                        <ion-label>\n                                                {{language.fnGetLanguage(\'size\')}}\n                                        </ion-label>\n                                        <div item-content>\n                                                {{ item.size }}\n                                        </div>\n                                </ion-item>\n\n\n\n\n                                <ion-item>\n                                        <ion-label>\n                                                {{language.fnGetLanguage(\'min\')}}{{language.fnGetLanguage(\'order_qty\')}}\n                                        </ion-label>\n                                        <div item-content>\n                                                {{ item.min_order_qty }} {{item.unit_name}}\n                                        </div>\n                                </ion-item>\n\n                                <ion-item>\n                                        <ion-label>\n                                                {{language.fnGetLanguage(\'rate\')}}\n                                        </ion-label>\n                                        <div item-content>\n                                                ₹{{ item.sale_price.toLocaleString(\'en-IN\') }}\n                                        </div>\n                                </ion-item>\n                                <ion-item>\n                                        <ion-label>\n                                                {{language.fnGetLanguage(\'quantity\')}}\n                                        </ion-label>\n                                        <div item-content style="border: 1px solid lightgray" class="abosInput"\n                                                item-right end>\n                                                <ion-icon name="remove-circle" (click)="decrement()" class="iconCst"></ion-icon>\n                                                <span class="iconCst bh">\n                                                        <ion-input type="number" (change)="fnQtyChange()" [(ngModel)]="item.qty"\n                                                                pattern="[0-9]*" [style.width.px]="inputWidth" required="true"\n                                                                clearInput=true min="{{ item.min_order_qty }}" class="forInput"></ion-input>\n                                                </span>\n                                                <ion-icon name="add-circle" (click)="increment()" class="iconCst"></ion-icon>\n                                        </div>\n                                </ion-item>\n                                <ion-item>\n                                        <ion-label>\n                                                {{language.fnGetLanguage(\'total_amount\')}}\n                                        </ion-label>\n                                        <div item-content style="font-weight:bold">\n                                                ₹{{ (item.qty*item.sale_price).toLocaleString(\'en-IN\') }}\n                                        </div>\n                                </ion-item>\n                                <ion-item *ngIf="item" class="remove-bottom-line border-top">\n                                        {{language.fnGetLanguage(\'notes\')}}\n                                </ion-item>\n                                <ion-item *ngIf="item" class="remove-bottom-line">\n                                        <ion-textarea style="border: 1px solid lightgray;margin: 5px auto" [(ngModel)]="item.note"\n                                                rows="4" placeholder="{{language.fnGetLanguage(\'notes\')}}...."></ion-textarea>\n                                </ion-item>\n                        </ion-list>\n                </div>\n        </div>\n        <canvas id="canvas" style="display: none"></canvas>\n</ion-content>\n<ion-footer>\n        <ion-row>\n                <ion-col text-center style="margin: 0;letter-spacing: 2px !important;background: #9A772D;color: #fff;padding-top: 15px !important; font-weight: bold;padding-bottom: 15px !important"\n                        *ngIf="item && item.in_stock==\'1\'" (click)="fnAddToCart()">\n                        <ion-icon style="color: #fff" name="cart"></ion-icon>\n                        {{language.fnGetLanguage(\'title_add_to_cart\')}}\n                </ion-col>\n                <ion-col text-center style="margin: 0;letter-spacing: 2px !important;background: firebrick;color: #fff;padding-top: 15px !important; font-weight: bold;padding-bottom: 15px !important"\n                        *ngIf="item && item.in_stock == \'0\'">\n                        <ion-icon style="color: green" name="logo-whatsapp"></ion-icon>\n                        {{language.fnGetLanguage(\'title_out_of_stock\')}}\n                </ion-col>\n        </ion-row>\n</ion-footer>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/product/product.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_7__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__["a" /* CustomeProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_cart_cart__["a" /* CartProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], ProductPage);

//# sourceMappingURL=product.js.map

/***/ }),

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShippingAddressPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__cart_cart__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__order_success_order_success__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








let ShippingAddressPage = class ShippingAddressPage {
    constructor(navCtrl, language, menu, event, alertCtrl, loadingCtrl, navParams, service, cart) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.menu = menu;
        this.event = event;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.service = service;
        this.cart = cart;
        this.address_line_1 = localStorage.getItem('address_line_1');
        this.address_line_2 = localStorage.getItem('address_line_2');
        this.flag = false;
        this.sameAsbillingAddress = true;
        this.billingAddress = '';
        this.shippingAddress = '';
    }
    ionViewDidLoad() {
        this.navBar.backButtonClick = (e) => {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__cart_cart__["a" /* CartPage */]);
        };
        if (localStorage.getItem('address_line_1') && localStorage.getItem('address_line_1') != '' && localStorage.getItem('address_line_1') != 'null' && localStorage.getItem('address_line_1') != null && localStorage.getItem('address_line_1') != undefined && localStorage.getItem('address_line_1') != 'undefined') {
            this.billingAddress = this.billingAddress + this.address_line_1 + ',\n';
        }
        if (localStorage.getItem('address_line_2') && localStorage.getItem('address_line_2') != '' && localStorage.getItem('address_line_2') != 'null' && localStorage.getItem('address_line_2') != null && localStorage.getItem('address_line_2') != undefined && localStorage.getItem('address_line_2') != 'undefined') {
            this.billingAddress = this.billingAddress + localStorage.getItem('address_line_2') + ',\n';
        }
        if (localStorage.getItem('city') && localStorage.getItem('city') != '' && localStorage.getItem('city') != 'null' && localStorage.getItem('city') != null && localStorage.getItem('city') != undefined && localStorage.getItem('city') != 'undefined') {
            this.billingAddress = this.billingAddress + localStorage.getItem('city');
            if (localStorage.getItem('pincode') && localStorage.getItem('pincode') != '' && localStorage.getItem('pincode') != 'null' && localStorage.getItem('pincode') != null && localStorage.getItem('pincode') != undefined && localStorage.getItem('pincode') != 'undefined') {
                this.billingAddress = this.billingAddress + ' - ' + localStorage.getItem('pincode');
            }
            this.billingAddress = this.billingAddress + ',\n';
        }
        if (localStorage.getItem('state') && localStorage.getItem('state') != '' && localStorage.getItem('state') != 'null' && localStorage.getItem('state') != null && localStorage.getItem('state') != undefined && localStorage.getItem('state') != 'undefined') {
            this.billingAddress = this.billingAddress + localStorage.getItem('state') + ',\n';
        }
        if (localStorage.getItem('country') && localStorage.getItem('country') != '' && localStorage.getItem('country') != 'null' && localStorage.getItem('country') != null && localStorage.getItem('country') != 'undefined' && localStorage.getItem('country') != undefined) {
            this.billingAddress = this.billingAddress + localStorage.getItem('country') + ',\n';
        }
        if (this.sameAsbillingAddress == true) {
            this.shippingAddress = this.billingAddress;
        }
    }
    fnSameAsBillingAddress() {
        if (this.sameAsbillingAddress == true) {
            this.shippingAddress = this.billingAddress;
        }
    }
    fnChangeBillingAddress() {
        if (this.sameAsbillingAddress == true) {
            this.shippingAddress = this.billingAddress;
        }
    }
    fnPlaceOrder() {
        let data = this.cart.fnGetCart();
        let cartItem = [];
        if (data && data.length > 0) {
            for (let index = 0; index < data.length; index++) {
                cartItem.push({
                    'item_id': data[index].item_id,
                    'qty': data[index].qty,
                    'note': data[index].note
                });
            }
        }
        let form = {
            'cart': JSON.stringify(cartItem)
        };
        if (!this.billingAddress || this.billingAddress == '') {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('enter_a_billing_address'),
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        form['billing_detail'] = this.billingAddress;
        if (this.sameAsbillingAddress) {
            form['billing_as_shipping'] = '1';
        }
        else {
            if (!this.billingAddress || this.billingAddress == '') {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('enter_a_billing_address'),
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            form['shipping_detail'] = this.shippingAddress;
        }
        let loader = this.loadingCtrl.create({
            content: 'Loading...'
        });
        loader.present();
        this.service.post('order/create_order', form).then((result) => {
            localStorage.removeItem('cart');
            this.cart.fnGetCart();
            this.event.publish('cart:itemAdded');
            loader.dismiss();
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__order_success_order_success__["a" /* OrderSuccessPage */]);
        }, (error) => {
            loader.dismiss();
            if (error && error.status == 4) {
                let alert = this.alertCtrl.create({
                    subTitle: this.language.fnGetLanguage('enter_a_billing_address'),
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                alert.onDidDismiss(() => {
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
                });
            }
            else {
                if (error.message) {
                    let alert = this.alertCtrl.create({
                        subTitle: error.message,
                        buttons: [this.language.fnGetLanguage('title_ok')]
                    });
                    alert.present();
                }
                else {
                    let alert = this.alertCtrl.create({
                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                        buttons: [this.language.fnGetLanguage('title_ok')]
                    });
                    alert.present();
                }
            }
        });
    }
    fnGotoCartPage() {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__cart_cart__["a" /* CartPage */]);
    }
};
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Navbar */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Navbar */])
], ShippingAddressPage.prototype, "navBar", void 0);
ShippingAddressPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-shipping-address',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/shipping-address/shipping-address.html"*/'<!--\n  Generated template for the ShippingAddressPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n        <ion-navbar>\n\n\n                <ion-title>{{language.fnGetLanguage(\'title_billing_and_shipping\')}}</ion-title>\n                <ion-buttons end style="opacity: 0;" showWhen="android">\n                        <button ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n        <div>\n                <ion-row>\n                        <ion-col col-4>\n\n                        </ion-col>\n                        <ion-col col-4>\n                                <img src="assets/img_logo/shipping.png">\n                        </ion-col>\n                        <ion-col col-4>\n\n                        </ion-col>\n                </ion-row>\n                <ion-row>\n                        <ion-col col-12 style="text-align: center">\n                                <h4 style="letter-spacing:3px">\n                                        {{language.fnGetLanguage(\'title_billing_and_shipping\')}}\n                                </h4>\n                                <h6 style="font-weight: 100; ">\n                                        {{language.fnGetLanguage(\'fill_up_the_bellow_to_set_your_billing_and_shipping_address\')}}\n\n                                </h6>\n                        </ion-col>\n                </ion-row>\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'billing_address\')}}</span>\n                        <ion-textarea col-12 type="phone" placeholder="{{language.fnGetLanguage(\'enter_your_billing_address\')}}"\n                                class="inputarea" [(ngModel)]="billingAddress" (ionChange)="fnChangeBillingAddress()"\n                                rows="7"></ion-textarea>\n                </ion-row>\n                <ion-row style="padding-top:10px ">\n                        <ion-checkbox [(ngModel)]="sameAsbillingAddress" (ionChange)="fnSameAsBillingAddress()"></ion-checkbox>\n                        <ion-label class="spanPadding" style="margin: 0 0 0 5px;">{{language.fnGetLanguage(\'shipping_address_same_as_billing_address\')}}</ion-label>\n                </ion-row>\n                <ion-row class="mt20px" style="    margin-top: 30px;">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'shipping_address\')}}</span>\n                        <ion-textarea col-12 type="phone" placeholder="{{language.fnGetLanguage(\'enter_your_shipping_address\')}}"\n                                class="inputarea" [(ngModel)]="shippingAddress" rows="7" [disabled]="sameAsbillingAddress"></ion-textarea>\n                </ion-row>\n        </div>\n</ion-content>\n<ion-footer>\n        <button ion-button large block color="golden" (click)="fnPlaceOrder()" style="margin: 0">{{language.fnGetLanguage(\'place_order\')}}</button>\n</ion-footer>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/shipping-address/shipping-address.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_7__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__["a" /* CustomeProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_cart_cart__["a" /* CartProvider */]])
], ShippingAddressPage);

//# sourceMappingURL=shipping-address.js.map

/***/ }),

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_cart_cart__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__category_category__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__signup_signup__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__forgott_password_forgott_password__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shipping_address_shipping_address__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_language_language__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let LoginPage = class LoginPage {
    constructor(navCtrl, navParams, cart, language, loadingCtrl, event, menu, service, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.cart = cart;
        this.language = language;
        this.loadingCtrl = loadingCtrl;
        this.event = event;
        this.menu = menu;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.phoneNo = '';
        this.password = '';
        if (localStorage.getItem('token')) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__category_category__["a" /* CategoryPage */]);
        }
    }
    ionViewDidLoad() {
        if (!this.navCtrl.canGoBack()) {
            this.menu.enable(true);
        }
        this.navBar.backButtonClick = (e) => {
            this.cart.isPlaceOrder = false;
            this.navCtrl.pop();
        };
    }
    fnLogin() {
        let reg = /^\d+$/;
        if (!this.phoneNo || this.phoneNo == '' || this.phoneNo.length != 10 || !reg.test(this.phoneNo)) {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('enter_valid_mobile_no.') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        if (!this.password || this.password == '') {
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('enter_valid_password') + '.',
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
            return false;
        }
        let form;
        form = {
            'mobile_no': this.phoneNo,
            'password': this.password
        };
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.service.post('login/verify_login', form).then((result) => {
            loader.dismiss();
            if (result.status == 1) {
                localStorage.setItem('token', result.auth_token);
                localStorage.setItem('customer_id', result.account_details.customer_id);
                localStorage.setItem('name', result.account_details.name);
                localStorage.setItem('mobile_no', result.account_details.mobile_no);
                localStorage.setItem('address_line_1', result.account_details.address_line_1);
                localStorage.setItem('address_line_2', result.account_details.address_line_2);
                localStorage.setItem('city', result.account_details.city);
                localStorage.setItem('state', result.account_details.state);
                localStorage.setItem('country', result.account_details.country);
                localStorage.setItem('email_id', result.account_details.email_id);
                localStorage.setItem('phone_no', result.account_details.phone_no);
                localStorage.setItem('photo', result.account_details.photo);
                localStorage.setItem('created_on', result.account_details.created_on);
                localStorage.setItem('status', result.account_details.status);
                this.event.publish('userloggedin', result.account_details.mobile_no);
                if (this.navCtrl.canGoBack()) {
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__shipping_address_shipping_address__["a" /* ShippingAddressPage */]);
                }
                else {
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__category_category__["a" /* CategoryPage */]);
                }
            }
            else {
                let alert = this.alertCtrl.create({
                    subTitle: result.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
        }, (error) => {
            loader.dismiss();
            if (error.message) {
                let alert = this.alertCtrl.create({
                    subTitle: error.message,
                    buttons: [this.language.fnGetLanguage('title_ok')]
                });
                alert.present();
                return false;
            }
            let alert = this.alertCtrl.create({
                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                buttons: [this.language.fnGetLanguage('title_ok')]
            });
            alert.present();
        });
    }
    fnRegister() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__signup_signup__["a" /* SignupPage */]);
    }
    fnGoToForgottPasswordPage() {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__forgott_password_forgott_password__["a" /* ForgottPasswordPage */]);
    }
};
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Navbar */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* Navbar */])
], LoginPage.prototype, "navBar", void 0);
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-login',template:/*ion-inline-start:"/Volumes/DATA/app/BRIJWASI/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n        <ion-navbar>\n                <button menuToggle ion-button icon-only>\n                        <ion-icon color="dark" name="menu"></ion-icon>\n                </button>\n                <ion-title>{{language.fnGetLanguage(\'title_login\')}}</ion-title>\n                <ion-buttons end style="opacity: 0;" showWhen="android">\n                        <button ion-button icon-only id="notification-button">\n                                <ion-icon color="dark" name="cart"></ion-icon>\n                                <ion-badge small id="notifications-badge" *ngIf="cartItem && cartItem>0">{{cartItem}}</ion-badge>\n                        </button>\n                </ion-buttons>\n        </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n        <div>\n                <ion-row>\n                        <ion-col col-12 style="text-align: center">\n                                <img src="assets/img_logo/login-icon.png">\n                                <h4 style="letter-spacing:3px">\n                                        {{language.fnGetLanguage(\'title_login\')}}\n                                </h4>\n                                <h6 style="font-weight: 100; ">\n\n                                        {{language.fnGetLanguage(\'fill_up_the_bellow_to_login_your_account\')}}\n                                </h6>\n                        </ion-col>\n                </ion-row>\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'mobile_no\')}}.</span>\n                        <ion-input col-12 type="tel" placeholder="{{language.fnGetLanguage(\'enter_your_mobile_no\')}}"\n                                class="inputarea" [(ngModel)]="phoneNo"></ion-input>\n                </ion-row>\n                <ion-row class="mt20px">\n                        <span class="spanPadding">{{language.fnGetLanguage(\'password\')}}</span>\n                        <ion-input col-12 type="password" placeholder="{{language.fnGetLanguage(\'enter_your_password\')}}"\n                                class="inputarea" [(ngModel)]="password"></ion-input>\n                </ion-row>\n                <ion-row class="text_float_right">\n                        <a href="javascript:void(0)" (click)="fnGoToForgottPasswordPage()" style="color: #6b4d0f;text-decoration: none; padding: 5px">\n                                {{language.fnGetLanguage(\'forget_your_password\')}}?\n                        </a>\n                </ion-row>\n                <button ion-button large color="golden" block (click)="fnLogin()">{{language.fnGetLanguage(\'login\')}}</button>\n                <ion-row style="padding: 5px">\n                        <ion-col col-12 style="text-align: center; ">\n                                {{language.fnGetLanguage(\'dont_have_an_account\')}}?\n                        </ion-col>\n                </ion-row>\n                <button ion-button large outline color="golden" block (click)="fnRegister()" style="text-align: center; ">\n                        {{language.fnGetLanguage(\'create_new_account\')}}\n                </button>\n        </div>\n</ion-content>'/*ion-inline-end:"/Volumes/DATA/app/BRIJWASI/src/pages/login/login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_cart_cart__["a" /* CartProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_custome_custome__["a" /* CustomeProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ })

},[285]);
//# sourceMappingURL=main.js.map