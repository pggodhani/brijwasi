import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CategoryPage } from '../pages/category/category';
import { ProductPage } from '../pages/product/product';
import { CartPage } from '../pages/cart/cart';
import { ShippingAddressPage } from '../pages/shipping-address/shipping-address';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { CustomeProvider } from '../providers/custome/custome';
import { HttpClientModule } from '@angular/common/http';
import { CartProvider } from '../providers/cart/cart';
import { ImageViewerPage } from '../pages/image-viewer/image-viewer';
import { UserDetailPage } from '../pages/user-detail/user-detail';
import { OtpVerifyPage } from '../pages/otp-verify/otp-verify';
import { ForgottPasswordPage } from '../pages/forgott-password/forgott-password';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { PastOrdersPage } from '../pages/past-orders/past-orders';
import { PastOrderDetailPage } from '../pages/past-order-detail/past-order-detail';
import { ChangeMobileNoPage } from '../pages/change-mobile-no/change-mobile-no';
import { VerifyChangeMobilePage } from '../pages/verify-change-mobile/verify-change-mobile';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Network } from '@ionic-native/network';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP } from '@ionic-native/http';
import { OrderSuccessPage } from '../pages/order-success/order-success';
import { OneSignal } from '@ionic-native/onesignal';
import { Badge } from '@ionic-native/badge';
import { NotoficationPage } from '../pages/notofication/notofication';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { NoteModalPage } from '../pages/note-modal/note-modal';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery';
import { Market } from '@ionic-native/market';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { SearchItemPage } from '../pages/search-item/search-item';
import { LanguageProvider } from '../providers/language/language';
import { FilterPage } from '../pages/filter/filter';
import { SettingsPage } from '../pages/settings/settings';
import { SplashPage } from '../pages/splash/splash';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AppAvailability } from '@ionic-native/app-availability';
import { LazyLoadImagesModule } from 'ngx-lazy-load-images';
import { IonicImageViewerModule } from '../Library/ionic-img-viewer/dist/es2015/src/module';
@NgModule({
        declarations: [
                MyApp,
                HomePage,
                CategoryPage,
                ProductPage,
                CartPage,
                LoginPage,
                ShippingAddressPage,
                SignupPage,
                ImageViewerPage,
                UserDetailPage,
                OtpVerifyPage,
                ForgottPasswordPage,
                PastOrdersPage,
                ChangePasswordPage,
                PastOrderDetailPage,
                ChangeMobileNoPage,
                NoteModalPage,
                VerifyChangeMobilePage,
                OrderSuccessPage,
                SearchItemPage,
                FilterPage,
                NotoficationPage,
                SettingsPage,
                SplashPage
        ],
        imports: [
                BrowserModule,LazyLoadImagesModule,
                HttpClientModule,IonicImageViewerModule,
                BrowserAnimationsModule,
                IonicModule.forRoot(MyApp),
        ],
        bootstrap: [IonicApp],
        entryComponents: [
                MyApp,
                HomePage,
                LoginPage,
                SignupPage,
                CategoryPage,
                ProductPage,
                NoteModalPage,
                CartPage,
                ShippingAddressPage,
                ImageViewerPage,
                UserDetailPage,
                OtpVerifyPage,
                ChangePasswordPage,
                ForgottPasswordPage,
                PastOrdersPage,
                PastOrderDetailPage,
                ChangeMobileNoPage,
                VerifyChangeMobilePage,
                OrderSuccessPage,
                NotoficationPage,
                SearchItemPage,
                FilterPage,
                SettingsPage,
                SplashPage
        ],
        providers: [
                StatusBar,
                SplashScreen,
                { provide: ErrorHandler, useClass: IonicErrorHandler },
                CustomeProvider,
                AppAvailability,
                CartProvider,
                ScreenOrientation,
                FileTransfer,
                Camera,
                HTTP,
                OneSignal, Market,PhotoLibrary,
                Base64ToGallery,
                FileTransferObject,
                Badge,
                Network,
                InAppBrowser,
                File,
                LocalNotifications,
                AndroidPermissions,
                LanguageProvider
        ]
})
export class AppModule { }