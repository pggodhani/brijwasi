import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, MenuController, AlertController, ActionSheetController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { CategoryPage } from '../pages/category/category';
import { CartPage } from '../pages/cart/cart';
import { LoginPage } from '../pages/login/login';
import { CustomeProvider } from '../providers/custome/custome';
import { PastOrdersPage } from '../pages/past-orders/past-orders';
import { UserDetailPage } from '../pages/user-detail/user-detail';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import * as $ from 'jquery';
import { CartProvider } from '../providers/cart/cart';
import { OneSignal } from '@ionic-native/onesignal';
import { Badge } from '@ionic-native/badge';
import { LanguageProvider } from '../providers/language/language';
import { SplashPage } from '../pages/splash/splash';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AppAvailability } from '@ionic-native/app-availability';
import { Market } from '@ionic-native/market';
import { SettingsPage } from '../pages/settings/settings';
@Component({
        templateUrl: 'app.html'
})
export class MyApp {
        @ViewChild(Nav) nav: Nav;
        rootPage: any;
        languages = []
        lang: any;
        name = 'Brijwasi'
        pages: any = [];
        isLoggedIn = false;
        mobileNo = '';
        categories = []
        appLang = ''
        constructor(public platform: Platform,
                public statusBar: StatusBar,
                public LanguageService: LanguageProvider,
                public event: Events,
                private iab: InAppBrowser,
                private cart: CartProvider,
                private alertCtrl: AlertController,
                public menuCtrl: MenuController,
                private badge: Badge,
                private oneSignal: OneSignal,
                public splashScreen: SplashScreen,
                public langService: LanguageProvider,
                private appAvailability: AppAvailability,
                private language: LanguageProvider,
                public actionSheetCtrl: ActionSheetController,
                private service: CustomeProvider,
                private market: Market) {
                this.initializeApp();

        }
        j = 0;
        appUpdate = 0

        initializeApp() {
                this.platform.ready().then(() => {
                        localStorage.setItem('lazyLoadding', 'true');//true for active lazyloadding '' for off
                        let appId = '75bd2dcc-eb89-49a6-8df9-2373d7b71614';
                        let googleProjectId = '714052471595';
                        this.event.unsubscribe('appLangChange')
                        this.appLang = localStorage.getItem('appLang')
                        this.event.subscribe('appLangChange', () => {
                                this.appLang = this.language.fnGetSelectedAppLang()
                        });
                        this.oneSignal.startInit(appId, googleProjectId);
                        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification)
                        // this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification)
                        // 
                        this.oneSignal.setSubscription(true);
                        this.intialize_seprate();
                        this.oneSignal.handleNotificationReceived().subscribe((data) => {

                                this.badge.get().then((r) => {
                                        if (r == 0) {
                                                this.setBadges(1)
                                        } else if (r > 0) {
                                                this.increaseBadges('1')
                                        } else {
                                                this.setBadges(1)
                                        }
                                }, (e) => {
                                })

                                let item = JSON.stringify(JSON.parse(JSON.parse(data.payload.rawPayload).custom).a)

                                item['isOpen'] = false;
                                var notifications = [];
                                if (localStorage.getItem('notiications')) {
                                        notifications = JSON.parse(localStorage.getItem('notiications'))
                                }
                                if (notifications.length > 0) {
                                        notifications.splice(0, 0, item);
                                } else {
                                        notifications.push(item)
                                }
                                localStorage.setItem('notiications', JSON.stringify(notifications))
                        });
                        this.oneSignal.handleNotificationOpened().subscribe((data) => {
                                this.clearBadges()
                                localStorage.setItem('notificationOpen', '1')
                                if (data.notification.payload.additionalData) {
                                        this.nav.setRoot(CategoryPage, { data: data.notification.payload.additionalData })
                                } else {
                                        this.nav.setRoot(CategoryPage, { data: JSON.parse(JSON.parse(data.notification.payload.rawPayload).custom).a })
                                }
                        });
                        this.oneSignal.endInit();
                        this.statusBar.styleDefault();
                        this.splashScreenStuff()
                        // this.splashScreen.hide();
                })
        }

        intialize_seprate() {
                this.event.unsubscribe('category:updated')
                this.event.unsubscribe('userloggedin')
                this.event.unsubscribe('userNotloggedin')
                this.event.unsubscribe('langSet')
                this.event.subscribe('langSet', () => {
                        this.initializeApp()
                })
                this.cart.fnGetCart()

                this.event.subscribe('category:updated', (data) => {
                        this.categories = JSON.parse(JSON.stringify(data))
                        if (this.j == 0) {
                                let pageIndex = 2
                                for (let index = 0; index < data.length; index++) {
                                        let stringToBeInserted: any = {
                                                title: data[index].category_name,
                                                component: HomePage,
                                                data: data[index],
                                                parent: 'collections',
                                                isCategory: true,
                                                active: false
                                        };
                                        if (this.appLang && data[index].category_name_lang[this.appLang] && data[index].category_name_lang[this.appLang] != '' && data[index].category_name_lang[this.appLang] != {}) {
                                                stringToBeInserted['title'] = data[index].category_name_lang[this.appLang]
                                        }
                                        this.pages.splice(pageIndex, 0, stringToBeInserted);
                                        pageIndex = pageIndex + 1
                                }
                                this.j = 1
                        }
                });
                this.event.subscribe('userloggedin', (data) => {
                        this.initializeApp()
                })
                /****************************** */
                this.service.post('startup/get_lang', {}).then((result: any) => {
                        let languages = result.language
                        this.languages = []
                        for (var k in languages) {
                                this.languages.push({ key: k, value: languages[k] })
                        }
                        this.LanguageService.fnSetLanguageArray(this.languages)
                        this.LanguageService.fnSetLangLabelArray(result.label)
                        if (localStorage.getItem('token') && localStorage.getItem('token') != '') {
                                this.mobileNo = localStorage.getItem('mobile_no')
                                this.pages = [
                                        {
                                                title: this.language.fnGetLanguage('home'),
                                                component: CategoryPage,
                                                data: '',
                                                icon: 'home',
                                                active: true
                                        },
                                        {
                                                title: this.language.fnGetLanguage('our_collection'),
                                                data: '',
                                                hasChild: true,
                                                icon: 'logo-buffer',
                                                childSelector: 'ourCollections',
                                                active: false
                                        },
                                        {
                                                title: this.language.fnGetLanguage('cart'),
                                                component: CartPage,
                                                data: '',
                                                icon: 'cart',
                                                active: false
                                        },
                                        {
                                                title: this.language.fnGetLanguage('my_account'),
                                                data: '',
                                                hasChild: true,
                                                icon: 'person',
                                                childSelector: 'Profile',
                                                active: false
                                        },
                                        {
                                                title: this.language.fnGetLanguage('my_orders'),
                                                component: PastOrdersPage,
                                                data: '',
                                                parent: 'Profile',
                                                active: false
                                        },
                                        {
                                                title: this.language.fnGetLanguage('profile'),
                                                component: UserDetailPage,
                                                data: true,
                                                parent: 'Profile',
                                                active: false
                                        },
                                        {
                                                title: this.language.fnGetLanguage('change_password'),
                                                component: ChangePasswordPage,
                                                data: true,
                                                parent: 'Profile',
                                                active: false
                                        }
                                ]
                                this.j = 0
                                let data = {}
                                this.service.post('category/get_category', data).then((result: any) => {
                                        this.event.publish('category:updated', result.category);
                                }, (error: any) => { })
                                this.isLoggedIn = true;
                                if (localStorage.getItem('name') && localStorage.getItem('name') != 'null') {
                                        this.name = localStorage.getItem('name')
                                }
                                if (localStorage.getItem('mobile_no') && localStorage.getItem('mobile_no') != 'null') {
                                        this.mobileNo = localStorage.getItem('mobile_no')
                                }
                        } else {
                                this.pages = [
                                        {
                                                title: this.language.fnGetLanguage('home'),
                                                component: CategoryPage,
                                                data: '',
                                                icon: 'home',
                                                active: true
                                        },
                                        {
                                                title: this.language.fnGetLanguage('our_collection'),
                                                data: '',
                                                hasChild: true,
                                                icon: 'logo-buffer',
                                                childSelector: 'ourCollections'
                                        },
                                        {
                                                title: this.language.fnGetLanguage('cart'),
                                                component: CartPage,
                                                data: '',
                                                icon: 'cart',
                                                active: false
                                        },
                                        {
                                                title: this.language.fnGetLanguage('login_and_signup'),
                                                component: LoginPage,
                                                data: '',
                                                icon: 'person',
                                                active: false
                                        },
                                ];
                                this.j = 0

                                this.service.post('category/get_category', {}).then((result: any) => {
                                        this.mobileNo = result.helpline_number
                                        localStorage.setItem('whatsapp_number', result.whatsapp_number)
                                        this.event.publish('category:updated', result.category);
                                        this.event.publish('userNotloggedin', result.helpline_number);
                                }, (error: any) => { })
                                this.isLoggedIn = false
                                this.name = 'Brijwasi'
                        }
                }, (error: any) => {

                })
                /****************************** */
        }

        splashScreenStuff() {
                this.menuCtrl.enable(false);
                this.platform.ready().then(() => {

                        if (this.cart.appUpdate == 0) {
                                if (this.platform.is('ios')) {
                                        this.service.post('startup/check_apple_version_update', { version: '1.0.3' })
                                                .then((result: any) => {
                                                        this.splashScreen.hide();

                                                        if (result && result.maintenance && result.maintenance.apple) {
                                                                let alert = this.alertCtrl.create({
                                                                        subTitle: result.maintenance.msg,
                                                                        enableBackdropDismiss: false,

                                                                        buttons: [
                                                                                {
                                                                                        text: this.language.fnGetLanguage('close'),
                                                                                        handler: () => {
                                                                                                this.platform.exitApp();
                                                                                        }
                                                                                }
                                                                        ]
                                                                });
                                                                alert.present();
                                                                alert.onDidDismiss(() => {
                                                                        this.platform.exitApp();
                                                                })
                                                        } else if (result.update_status == 2 || result.update_status == '2') {
                                                                let alert = this.alertCtrl.create({
                                                                        subTitle: this.language.fnGetLanguage('please_update_an_app'),
                                                                        enableBackdropDismiss: false,
                                                                        buttons: [
                                                                                {
                                                                                        text: this.language.fnGetLanguage('update'),
                                                                                        handler: () => {
                                                                                                this.market.open(result.package_name).then((r) => {
                                                                                                        // this.market.open()
                                                                                                })

                                                                                        }
                                                                                }
                                                                        ]
                                                                });
                                                                alert.present();
                                                                alert.onDidDismiss(() => {
                                                                        this.platform.exitApp();
                                                                })
                                                        } else if (result.update_status == 1 || result.update_status == '1') {
                                                                this.cart.appUpdate = 1;
                                                                let alert = this.alertCtrl.create({
                                                                        subTitle: this.language.fnGetLanguage('new_update_available') + '.',
                                                                        enableBackdropDismiss: false,
                                                                        buttons: [
                                                                                {
                                                                                        text: this.language.fnGetLanguage('close'),
                                                                                        handler: () => {
                                                                                                if (result.multy_language) {
                                                                                                        if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                                                                                                this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                                                                                                this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'))
                                                                                                                this.nav.setRoot(CategoryPage)
                                                                                                        } else {
                                                                                                                this.nav.setRoot(SettingsPage)
                                                                                                        }
                                                                                                } else {
                                                                                                        this.nav.setRoot(CategoryPage)

                                                                                                }
                                                                                        }
                                                                                },
                                                                                {
                                                                                        text: this.language.fnGetLanguage('update'),
                                                                                        handler: () => {
                                                                                                this.market.open('com.brijwasibj').then((r) => {
                                                                                                        // open a play store
                                                                                                }, (e) => {

                                                                                                })
                                                                                        }
                                                                                }
                                                                        ]
                                                                });
                                                                alert.present();
                                                        } else if (result.multy_language) {
                                                                if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                                                        this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                                                        this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'))
                                                                        this.nav.setRoot(CategoryPage)
                                                                } else {
                                                                        this.nav.setRoot(SettingsPage)
                                                                }
                                                        } else {
                                                                this.nav.setRoot(CategoryPage)
                                                        }
                                                }, (error: any) => {
                                                        // error
                                                        this.splashScreen.hide();
                                                        if (error && error.msg) {
                                                                this.alertCtrl.create({
                                                                        message: error.msg,
                                                                        buttons: [
                                                                                {
                                                                                        text: 'OK',
                                                                                        handler: () => {
                                                                                                this.platform.exitApp()
                                                                                        }
                                                                                }
                                                                        ]
                                                                }).present()
                                                        }

                                                })
                                } else if (this.platform.is('android')) {
                                        this.service.post('startup/check_android_version_update', { version: '1.0.3' })
                                                .then((result: any) => {
                                                        this.splashScreen.hide();
                                                        if (result && result.maintenance && result.maintenance.android) {
                                                                let alert = this.alertCtrl.create({
                                                                        subTitle: result.maintenance.msg,
                                                                        enableBackdropDismiss: false,

                                                                        buttons: [
                                                                                {
                                                                                        text: this.language.fnGetLanguage('close'),
                                                                                        handler: () => {
                                                                                                this.platform.exitApp();
                                                                                        }
                                                                                }
                                                                        ]
                                                                });
                                                                alert.present();
                                                                alert.onDidDismiss(() => {
                                                                        this.platform.exitApp();
                                                                })
                                                        } else if (result.update_status == 2 || result.update_status == '2') {
                                                                let alert = this.alertCtrl.create({
                                                                        subTitle: this.language.fnGetLanguage('please_update_an_app'),
                                                                        enableBackdropDismiss: false,

                                                                        buttons: [
                                                                                {
                                                                                        text: this.language.fnGetLanguage('close'),
                                                                                        handler: () => {
                                                                                                this.platform.exitApp();
                                                                                        }
                                                                                },
                                                                                {
                                                                                        text: 'Update',
                                                                                        handler: () => {
                                                                                                this.market.open(result.package_name);
                                                                                                this.platform.exitApp();
                                                                                        }
                                                                                }
                                                                        ]
                                                                });
                                                                alert.present();
                                                                alert.onDidDismiss(() => {
                                                                        this.platform.exitApp();
                                                                })
                                                        } else if (result.update_status == 1 || result.update_status == '1') {
                                                                this.cart.appUpdate = 1;
                                                                let alert = this.alertCtrl.create({
                                                                        subTitle: this.language.fnGetLanguage('new_update_available') + '.',
                                                                        enableBackdropDismiss: false,

                                                                        buttons: [
                                                                                {
                                                                                        text: this.language.fnGetLanguage('cancel'),
                                                                                        handler: () => {
                                                                                                if (result.multy_language) {
                                                                                                        if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                                                                                                this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                                                                                                this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'))
                                                                                                                this.nav.setRoot(CategoryPage)
                                                                                                        } else {
                                                                                                                this.nav.setRoot(SettingsPage)
                                                                                                        }
                                                                                                } else {
                                                                                                        this.nav.setRoot(CategoryPage)

                                                                                                }
                                                                                        }
                                                                                },
                                                                                {
                                                                                        text: this.language.fnGetLanguage('update'),
                                                                                        handler: () => {
                                                                                                this.market.open(result.package_name);
                                                                                                this.platform.exitApp();
                                                                                        }
                                                                                }
                                                                        ]
                                                                });
                                                                alert.present();
                                                        } else if (result.multy_language) {
                                                                if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                                                        this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                                                        this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'))
                                                                        this.nav.setRoot(CategoryPage)
                                                                } else {
                                                                        this.nav.setRoot(SettingsPage)
                                                                }
                                                        } else {
                                                                this.nav.setRoot(CategoryPage)
                                                        }
                                                }, (error: any) => {
                                                        //error
                                                        this.splashScreen.hide();
                                                        if (error && error.msg) {
                                                                this.alertCtrl.create({
                                                                        message: error.msg,
                                                                        buttons: [
                                                                                {
                                                                                        text: 'OK',
                                                                                        handler: () => {
                                                                                                this.platform.exitApp()
                                                                                        }
                                                                                }
                                                                        ]
                                                                }).present()
                                                        } else {
                                                                this.alertCtrl.create({
                                                                        message: 'Something happened wrong try again after sometimes.',
                                                                        buttons: [
                                                                                {
                                                                                        text: 'OK',
                                                                                        handler: () => {
                                                                                                this.platform.exitApp()
                                                                                        }
                                                                                }
                                                                        ]
                                                                }).present()
                                                        }
                                                })
                                }
                        } else {
                                this.cart.appUpdate = 1
                        }

                })
        }

        openPage(page) {
                if (page.hasChild) {
                        $("." + page.childSelector).fadeToggle(500);
                } else {
                        for (let index = 0; index < this.pages.length; index++) {
                                this.pages[index].active = false;
                        }
                        page.active = true;
                        this.nav.setRoot(page.component, { 'data': page.data });
                        this.menuCtrl.close();
                }
        }

        fnOpenCart() {
        }

        fnLogout() {
                let cart = localStorage.getItem('cart')
                let appLang = localStorage.getItem('appLang')
                let langArray = localStorage.getItem('langArray')
                let langList = localStorage.getItem('langList')
                localStorage.clear()
                localStorage.setItem('cart', cart)
                localStorage.setItem('appLang', appLang)
                localStorage.setItem('langArray', langArray)
                localStorage.setItem('langList', langList)
                this.event.publish('cart:itemAdded');
                this.nav.setRoot(CategoryPage)
                this.menuCtrl.close();
                this.initializeApp()
                this.isLoggedIn = false
        }

        menuOpened() {
                this.cart.isPlaceOrder = false;
        }

        async requestPermission() {
                try {
                        let hasPermission = await this.badge.hasPermission();
                        if (!hasPermission) {
                                await this.badge.requestPermission();
                        }
                } catch (e) {
                }
        }

        async setBadges(badgeNumber: number) {
                try {
                        await this.badge.set(badgeNumber);
                } catch (e) {
                }
        }

        async increaseBadges(badgeNumber: string) {
                try {
                        await this.badge.increase(Number(badgeNumber));
                } catch (e) {
                }
        }

        async clearBadges() {
                try {
                        await this.badge.clear();
                }
                catch (e) {
                }
        }


        fnGoToSettingPage() {
                let langList = JSON.parse(localStorage.getItem('langList'))
                let appLang = localStorage.getItem('appLang')
                let buttons = []
                for (let index = 0; index < langList.length; index++) {
                        buttons[index] = {
                                text: langList[index].value,
                                cssClass: appLang == langList[index].key ? 'selected' : 'notselected',
                                icon: appLang == langList[index].key ? 'checkmark' : '',
                                handler: () => {
                                        if (appLang != langList[index].key) {
                                                this.langService.fnSetAppLanguage(langList[index].key)
                                                this.event.publish('appLangChange')
                                                this.initializeApp()
                                        }
                                }
                        }
                }
                let SortActionSheet = this.actionSheetCtrl.create({
                        title: this.language.fnGetLanguage('chose_your_language'),
                        buttons: buttons
                });
                SortActionSheet.present();
        }

        fnShareViaWhatsApp() {
                let sharenumber = localStorage.getItem('whatsapp_number')
                let app;
                if (this.platform.is('ios')) {
                        app = 'whatsapp://';
                } else if (this.platform.is('android')) {
                        app = 'com.whatsapp';
                }
                this.appAvailability.check(app)
                        .then((yes: boolean) => {
                                this.iab.create('whatsapp://send?phone=' + sharenumber, '_system')
                        }, (no: boolean) => {
                                this.alertCtrl.create({ title: this.language.fnGetLanguage('whatsapp_is_not_installed_in_your_device') }).present()
                        });
                /**
                        this.socialSharing.canShareVia('whatsapp').then((result: any) => {
                                let sharenumber = localStorage.getItem('whatsapp_number')
                                this.socialSharing.shareViaWhatsAppToReceiver(sharenumber, '\n')
                        }, (error) => {
                                this.alertCtrl.create({ title: this.language.fnGetLanguage('whatsapp_is_not_installed_in_your_device')}).present()
                        })
                */
        }
}
