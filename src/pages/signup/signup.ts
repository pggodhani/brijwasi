import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { CustomeProvider } from '../../providers/custome/custome';
import { UserDetailPage } from '../user-detail/user-detail';
import { OtpVerifyPage } from '../otp-verify/otp-verify';
import { LanguageProvider } from '../../providers/language/language';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-signup',
        templateUrl: 'signup.html',
})
export class SignupPage {

        constructor(public navCtrl: NavController,
                private language:LanguageProvider, public loadingCtrl: LoadingController, public navParams: NavParams, private alertCtrl: AlertController, private service: CustomeProvider) {
        }
        phoneNo;
        password = '';
        conirmPassword = '';
        otp = '';
        firstStap = true;
        resenedOtpFlag = true;
        second = 60;
        ionViewDidLoad() {
        }

        fnRegister() {
                var reg = /^\d+$/;
                if (!this.phoneNo || this.phoneNo == '' || this.phoneNo.length != 10 || !reg.test(this.phoneNo)) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('enter_valid_mobile_no')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                }
                let form = {
                        'mobile_no': this.phoneNo
                };
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present()
                this.service.post('login/create_account', form).then((result: any) => {
                        this.navCtrl.push(OtpVerifyPage, { 'mobile_no': this.phoneNo })

                        loader.dismiss();

                }, (error: any) => {
                        loader.dismiss()
                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        } else {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                        }
                })
        }

        fnRegisterSecondStap() {
                var reg = /^\d+$/;
                if (!this.otp || this.otp == '' || !reg.test(this.phoneNo)) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('enter_valid_otp'),
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                }
                if (!this.password || this.password == '' || this.password.length < 6) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('enter_valid_password')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                }
                if (this.password != this.conirmPassword) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('password_and_confirm_password_does_not_match')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                }
                let form = {
                        'otp': this.otp,
                        'password': this.password,
                        'mobile_no': this.phoneNo
                }
                this.service.post('login/create_password', form).then((result: any) => {
                        // localStorage.clear();
                        localStorage.setItem('token', result.auth_token)
                        this.service.token = result.auth_token
                        this.service.post('account/get_account_details', form).then((result: any) => {
                                localStorage.setItem('customer_id', result.user.customer_id)
                                localStorage.setItem('name', result.user.name)
                                localStorage.setItem('mobile_no', result.user.mobile_no)
                                localStorage.setItem('address_line_1', result.user.address_line_1)
                                localStorage.setItem('address_line_2', result.user.address_line_2)
                                localStorage.setItem('city', result.user.city)
                                localStorage.setItem('state', result.user.state)
                                localStorage.setItem('country', result.user.country)
                                localStorage.setItem('email_id', result.user.email_id)
                                localStorage.setItem('phone_no', result.user.phone_no)
                                localStorage.setItem('photo', result.user.photo)
                                localStorage.setItem('created_on', result.user.created_on)
                                localStorage.setItem('status', result.user.status)
                                this.navCtrl.setRoot(UserDetailPage)
                        }, (error: any) => {
                                this.navCtrl.setRoot(UserDetailPage)
                        })
                }, (error: any) => {
                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        } else {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                        }
                })
        }


        fnResendOtp() {
                this.second = 60
                this.resenedOtpFlag = true

                let form = {
                        'mobile_no': this.phoneNo
                }

                this.service.post('login/resend_otp', form).then((result: any) => {

                        let interval = setInterval(() => {
                                if (this.second > 0) {
                                        this.second--;
                                } else {
                                        clearInterval(interval)
                                        this.resenedOtpFlag = false

                                }
                        }, 1000)
                }, (error: any) => {
                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        } else {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                        }
                })
        }
}
