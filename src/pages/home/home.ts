import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Events, AlertController, LoadingController, Content, ModalController, ActionSheetController } from 'ionic-angular';
import { CategoryPage } from '../category/category';
import { ProductPage } from '../product/product';
import { CustomeProvider } from '../../providers/custome/custome';
import { CartPage } from '../cart/cart';
import { CartProvider } from '../../providers/cart/cart';
import * as $ from 'jquery';
import { LanguageProvider } from '../../providers/language/language';
import { FilterPage } from '../filter/filter';
@Component({
        selector: 'page-home',
        templateUrl: 'home.html'
})
export class HomePage {

        pagination: any;
        cartItem: number;
        showFilter = false;
        Special = false;
        itemCode = '';
        size = '';
        data
        showHeaderFilterBar = true
        width = (window.innerWidth - 52) / 2
        items: any = [];
        selectedSub: any = 'all';
        subCategorys;
        sortBy: string = 'default'
        mainCategoryId: string;
        selectedSubCategory: any = '';
        selectedSize = '';
        isSpecial = false;
        inStock = false;
        price: { lower: string; upper: string; } = { upper: '0', lower: '0' }
        selectedPrice: { lower: string; upper: string; } = { upper: '0', lower: '0' }
        sizeArray = []
        appLang = localStorage.getItem('appLang')
        lazyLoadding = localStorage.getItem('lazyLoadding');
        @ViewChild(Content) content: Content;
        constructor(
                public navCtrl: NavController,
                private language: LanguageProvider,
                private loadingCtrl: LoadingController,
                public navParams: NavParams,
                public actionSheetCtrl: ActionSheetController,
                public event: Events,
                private modalCtrl: ModalController,
                public alertCtrl: AlertController,
                private service: CustomeProvider,
                private cart: CartProvider
        ) { }

        ionViewDidLoad() {
                this.appLang = localStorage.getItem('appLang')
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present();
                this.cartItem = this.cart.fnGetCountOfCart();
                this.event.subscribe('cart:itemAdded', () => {
                        this.cartItem = this.cart.fnGetCountOfCart();
                });
                this.event.subscribe('appLangChange', () => {
                        this.appLang = this.language.fnGetSelectedAppLang()
                });
                let category = this.navParams.get('data')
                this.data = category
                let form = {
                        'main_category_id': category.category_id,
                        'is_main': '1'
                }
                this.service.post('item/get_item_list', form)
                        .then((result: any) => {
                                this.items = result.item_list
                                console.log(this.items)
                                this.subCategorys = result.sub_category
                                this.pagination = result.pagination
                                this.price.lower = '0'
                                this.price.upper = result.price_max.toString()
                                this.selectedPrice.lower = '0'
                                this.selectedPrice.upper = result.price_max.toString()
                                this.sizeArray = result.size
                                this.scrollToTop();
                                loader.dismiss()
                        }, (error: any) => {
                                loader.dismiss()
                                if (error.message) {
                                        let alert = this.alertCtrl.create({
                                                subTitle: error.message,
                                                buttons: [this.language.fnGetLanguage('title_ok')]
                                        });
                                        alert.present();
                                        return false;
                                }
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                        })

        }

        scrollToTop() {
                this.content.scrollToTop();
        }

        fnCategary() {
                this.navCtrl.push(CategoryPage)
        }

        subCategoryChange() {
                this.itemCode = '';
                this.size = '';
                this.Special = false;
                if (this.selectedSub == 'all') {
                        this.ionViewDidLoad();
                } else if (this.selectedSub == 'special') {
                        let category = this.navParams.get('data')
                        let form = {
                                'main_category_id': this.mainCategoryId,
                                'category_id': category.category_id,
                                'is_main': '1',
                                'is_special': '1'
                        }
                        this.service.post('item/get_item_list', form).then((result: any) => {
                                this.items.length = 0
                                this.items = result.item_list
                                this.scrollToTop();
                        }, (error: any) => {
                        })
                } else {
                        let form = {
                                'category_id': this.selectedSub,
                                'is_main': '0'
                        }
                        this.service.post('item/get_item_list', form).then((result: any) => {
                                this.items.length = 0
                                this.items = result.item_list
                                this.scrollToTop();
                        }, (error: any) => {
                        })
                }
        }

        fnOpenCart() {
                this.navCtrl.push(CartPage)
        }

        fnOpenProductPage(item) {
                this.navCtrl.push(ProductPage, { 'data': item })
        }

        fnFilter() {
                let form = {}
                if (this.selectedSubCategory && this.selectedSubCategory != '') {
                        form['category_id'] = this.selectedSubCategory;
                        form['is_main'] = '0';
                } else {
                        form['is_main'] = '1';
                }
                if (this.selectedSize && this.selectedSize != '') {
                        form['size'] = this.selectedSize;
                }
                if (this.itemCode && this.itemCode != '') {
                        form['item_code'] = this.itemCode;
                }
                if (this.isSpecial) {
                        form['is_special'] = '1';
                }
                if (this.inStock) {
                        form['in_stock'] = '1';
                }
                form['main_category_id'] = this.data.category_id
                form['lower'] = this.selectedPrice.lower
                form['upper'] = this.selectedPrice.upper
                if (this.sortBy == 'lth') {
                        form['sort_order'] = 'asc';
                        form['sort_by'] = 'price';
                }
                if (this.sortBy == 'htl') {
                        form['sort_order'] = 'desc';
                        form['sort_by'] = 'price';
                }
                let loader = this.loadingCtrl.create({ content: 'Loadding...' })
                loader.present();
                this.service.post('item/get_item_list', form)
                        .then((result: any) => {
                                loader.dismiss();
                                this.items = result.item_list
                                this.pagination = result.pagination
                                this.showFilter = false
                                this.scrollToTop()
                        }, (error: any) => {
                                loader.dismiss();
                                if (error.message) {
                                        let alert = this.alertCtrl.create({
                                                subTitle: error.message,
                                                buttons: [this.language.fnGetLanguage('title_ok')]
                                        });
                                        alert.present();
                                        return false;
                                }
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                        })
        }

        fnLoadMoreData(e) {
                let form = {}
                if (this.selectedSubCategory && this.selectedSubCategory != '') {
                        form['category_id'] = this.selectedSubCategory;
                        form['is_main'] = '0';
                } else {
                        form['is_main'] = '1';
                }
                if (this.selectedSize && this.selectedSize != '') {
                        form['size'] = this.selectedSize;
                }
                if (this.itemCode && this.itemCode != '') {
                        form['item_code'] = this.itemCode;
                }
                if (this.isSpecial) {
                        form['is_special'] = '1';
                }
                if (this.inStock) {
                        form['in_stock'] = '1';
                }
                form['main_category_id'] = this.data.category_id
                form['lower'] = this.selectedPrice.lower
                form['upper'] = this.selectedPrice.upper
                if (this.sortBy == 'lth') {
                        form['sort_order'] = 'asc';
                        form['sort_by'] = 'price';
                }
                if (this.sortBy == 'htl') {
                        form['sort_order'] = 'desc';
                        form['sort_by'] = 'price';
                }
                form['page'] = this.pagination.next


                this.service.post('item/get_item_list', form)
                        .then((result: any) => {
                                this.items = this.items.concat(result.item_list)
                                this.pagination = result.pagination
                                e.complete();
                        }, (error: any) => {
                                e.complete();
                        })
        }

        fnImageNotFound(event) {
                event.target.src = "assets/img_logo/default-product.jpg"
        }

        onScroll(e) {
                if (e.directionY === 'up') {
                        this.showHeaderFilterBar = true;
                        $('.selector').fadeIn()
                        if (this.showFilter)
                                $('.filters').fadeIn()

                } else {
                        if (e.deltaY < 80 && e.scrollTop < 80) {
                                this.showHeaderFilterBar = true;
                                $('.selector').fadeIn()
                                if (this.showFilter) {
                                        $('.filters').fadeIn()
                                }
                        } else {
                                this.showHeaderFilterBar = false;
                                $('.selector').fadeOut()
                                $('.filters').fadeOut()
                        }
                }
        }

        fnOpenFilterModal() {
                let data = {
                        category_id: this.selectedSubCategory,
                        size: this.size,
                        item_code: this.itemCode,
                        is_special: this.isSpecial,
                        in_stock: this.inStock,
                        selectedPrice: this.selectedPrice,
                        subCategorys: this.subCategorys,
                        sizeArray: this.sizeArray,
                        price: this.price,
                        appLang:this.appLang
                }
                let searchModal = this.modalCtrl.create(FilterPage, { 'data': data }, { enableBackdropDismiss: false })
                searchModal.present()
                searchModal.onDidDismiss((data) => {
                        if (!data.data.isCancelled && data.data.filterData) {
                                this.selectedSubCategory = data.data.filterData.category_id
                                this.size = data.data.filterData.size
                                this.itemCode = data.data.filterData.item_code
                                this.isSpecial = data.data.filterData.is_special
                                this.inStock = data.data.filterData.in_stock
                                this.selectedPrice = data.data.filterData.selectedPrice
                                this.items = [];
                                this.fnFilter()
                        }
                })
        }

        fnSort() {
                let SortActionSheet = this.actionSheetCtrl.create({
                        title: this.language.fnGetLanguage('sort_by'),
                        buttons: [
                                {
                                        text: this.language.fnGetLanguage('new_arrival'),
                                        cssClass: this.sortBy == 'default' ? 'selected' : 'notselected',
                                        icon: this.sortBy == 'default' ? 'checkmark' : '',
                                        handler: () => {
                                                if (this.sortBy != 'default') {
                                                        this.sortBy = 'default'
                                                        this.items = []
                                                        this.ionViewDidLoad()
                                                }
                                        }
                                },
                                {
                                        text: this.language.fnGetLanguage('price_low_to_high'),
                                        cssClass: this.sortBy == 'lth' ? 'selected' : 'notselected',
                                        icon: this.sortBy == 'lth' ? 'checkmark' : '',
                                        handler: () => {
                                                if (this.sortBy != 'lth') {
                                                        this.sortBy = 'lth'
                                                        this.items = []
                                                        this.fnFilter()
                                                }
                                        }
                                },
                                {
                                        text: this.language.fnGetLanguage('price_high_to_low'),
                                        cssClass: this.sortBy == 'htl' ? 'selected' : 'notselected',
                                        icon: this.sortBy == 'htl' ? 'checkmark' : '',
                                        handler: () => {
                                                if (this.sortBy != 'htl') {
                                                        this.sortBy = 'htl'
                                                        this.items = []
                                                        this.fnFilter()
                                                }
                                        }
                                }
                        ]
                });
                SortActionSheet.present();
        }
        fnLoadImage(elementID) {
                let id = "#"+elementID
                $(id).css("background", "none");
                $(id).css("background", "#fff");
        }

}
