import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Events } from 'ionic-angular';
import { CustomeProvider } from '../../providers/custome/custome';
import { CartPage } from '../cart/cart';
import { ProductPage } from '../product/product';
import { CartProvider } from '../../providers/cart/cart';
import { CategoryPage } from '../category/category';
import { Badge } from '@ionic-native/badge';
import { LanguageProvider } from '../../providers/language/language';
import * as $ from 'jquery';
/**
 * Generated class for the NotoficationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-notofication',
        templateUrl: 'notofication.html',
})
export class NotoficationPage {
        items: any
        width = (window.innerWidth - 52) / 2
        lazyLoadding = localStorage.getItem('lazyLoadding');
        constructor(public navCtrl: NavController,
                private language: LanguageProvider,
                private badge: Badge,
                public event: Events,
                public navParams: NavParams,
                private cart: CartProvider,
                private loadingCtrl: LoadingController,
                private servics: CustomeProvider,
                public alertCtrl: AlertController) {
        }
        cartItem: any
        lastTime: any;
        currentTime;
        unseen = false;
        seenData = [];
        newData = [];
        appLang = localStorage.getItem('appLang')
        ionViewDidLoad() {

                this.event.unsubscribe('cart:itemAdded')
                this.event.publish('notification:vieved')

                this.cartItem = this.cart.fnGetCountOfCart();
                this.event.subscribe('cart:itemAdded', () => {
                        this.cartItem = this.cart.fnGetCountOfCart();
                });

                this.event.subscribe('appLangChange', () => {
                        this.appLang = this.language.fnGetSelectedAppLang()
                });

                let loader = this.loadingCtrl.create({
                        content: 'Loading...'
                });
                loader.present()
                this.servics.post('notification/get_item_list', {}).then((result) => {
                        loader.dismiss();
                        let d = result.items;
                        this.lastTime = parseInt(localStorage.getItem('notificationTime'))
                        var j = 0;
                        var i = 0;
                        if (result.items) {
                                for (let index = 0; index < d.length; index++) {
                                        if (parseInt(d[index].created_on) > parseInt(this.lastTime)) {
                                                this.newData.splice(j, 0, d[index]);
                                                j++;
                                        } else {
                                                this.seenData.splice(i, 0, d[index]);
                                                i++;

                                        }
                                }
                                this.items = result.items;
                        }
                        localStorage.removeItem('notificationTime')
                        localStorage.setItem('notificationTime', result.time)

                }, (error) => {
                        loader.dismiss();
                        if (error.time) {
                                localStorage.setItem('notificationTime', error.time)
                        }
                        let alert = this.alertCtrl.create({
                                subTitle: error.msg,
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();

                })

        }

        fnGotoHomePage() {
                this.navCtrl.setRoot(CategoryPage)
        }

        fnOpenCart() {
                this.navCtrl.push(CartPage)
        }

        fnOpenProductPage(item) {
                this.navCtrl.push(ProductPage, { 'data': item })
        }

        fnImageNotFound(event) {
                event.target.src = "assets/img_logo/default-product.jpg"
        }

        async clearBadges() {
                try {
                       await this.badge.clear();
                }
                catch (e) {
                }
        }
        fnLoadImage(elementID) {
                let id = "#"+elementID
                $(id).css("background", "none");
                $(id).css("background", "#fff");
        }
}
