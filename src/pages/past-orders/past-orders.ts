import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Events, LoadingController } from 'ionic-angular';
import { CartProvider } from '../../providers/cart/cart';
import { CartPage } from '../cart/cart';
import { CustomeProvider } from '../../providers/custome/custome';
import { PastOrderDetailPage } from '../past-order-detail/past-order-detail';
import { LanguageProvider } from '../../providers/language/language';
import * as $ from 'jquery';
/**
 * Generated class for the PastOrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-past-orders',
        templateUrl: 'past-orders.html',
})
export class PastOrdersPage {
        cartItem
        orders;
        lazyLoadding = localStorage.getItem('lazyLoadding');
        width = (window.innerWidth - 52) / 2
        constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private language:LanguageProvider,
                public event: Events,
                public cart: CartProvider, private service: CustomeProvider, private loadingCtrl: LoadingController, private alertCtrl: AlertController) {
        }

        ionViewDidLoad() {
                this.cartItem = this.cart.fnGetCountOfCart();
                this.event.subscribe('cart:itemAdded', () => {
                        this.cartItem = this.cart.fnGetCountOfCart();
                });
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                })
                loader.present();
                this.service.post('order/get_order_history', 'data').then((result: any) => {
                        this.orders = result.order
                        loader.dismiss()
                }, (error) => {
                        loader.dismiss()
                        if (!error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle:this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                        }

                })
        }
        fnOpenCart() {
                this.navCtrl.push(CartPage)
        }
        fnOpenOrderPage(order, orderIndex) {
                this.navCtrl.push(PastOrderDetailPage, { data: order })
        }

}
