import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ToastController, Navbar } from 'ionic-angular';
import { CustomeProvider } from '../../providers/custome/custome';
import { CategoryPage } from '../category/category';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ChangeMobileNoPage } from '../change-mobile-no/change-mobile-no';
import { ShippingAddressPage } from '../shipping-address/shipping-address';
import { CartProvider } from '../../providers/cart/cart';
import { CartPage } from '../cart/cart';
import { LanguageProvider } from '../../providers/language/language';
/**
 * Generated class for the UserDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-user-detail',
        templateUrl: 'user-detail.html',
})
export class UserDetailPage {
        
        name = ''
        address_line_1 = ''
        address_line_2 = ''
        phone_no = ''
        city = ''
        state = ''
        country = ''
        email = ''
        imageURI: any;
        imageFileName: any;
        pincode = '';
        changeMobile = false;
        mobileno='';

        constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private language:LanguageProvider,
                private alertCtrl: AlertController,
                private service: CustomeProvider,
                private transfer: FileTransfer,
                private camera: Camera,
                private cart:CartProvider,
                public loadingCtrl: LoadingController,
                public toastCtrl: ToastController) {
        }

        @ViewChild(Navbar) navBar: Navbar;
        ionViewDidLoad() {
                this.navBar.backButtonClick = (e:UIEvent)=>{
                        this.navCtrl.setRoot(CartPage)
                }
                if (localStorage.getItem('mobile_no')) {
                        this.mobileno = localStorage.getItem('mobile_no')
                }
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present();
                this.changeMobile = this.navParams.get('data')
                this.service.post('account/get_account_details', {})
                .then((result: any) => {
                                if (result.user.name) {
                                        this.name = result.user.name
                                }
                                if (result.user.phone_no) {
                                        this.phone_no = result.user.phone_no
                                }
                                if (result.user.mobile_no) {
                                        this.mobileno = result.user.mobile_no
                                }
                                if (result.user.photo) {
                                        this.imageFileName = result.user.photo
                                }
                                if (result.user.address_line_1) {
                                        this.address_line_1 = result.user.address_line_1
                                }
                                if (result.user.address_line_2) {
                                        this.address_line_2 = result.user.address_line_2
                                }
                                if (result.user.city) {
                                        this.city = result.user.city
                                }
                                if (result.user.state) {
                                        this.state = result.user.state
                                }
                                if (result.user.country) {
                                        this.country = result.user.country
                                }
                                if (result.user.email_id) {
                                        this.email = result.user.email_id
                                }
                                if(result.user.pincode){
                                        this.pincode = result.user.pincode
                                }
                                loader.dismiss();
                        }, (error: any) => {
                                loader.dismiss();

                        })
        }

        fnSubmitUserData() {
                if (!this.name || this.name == '' ) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('enter_valid_name')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                }
                if (this.email) {
                        let re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
                        if (!re.test(this.email)) {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('enter_valid_email')+'.',
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        }
                }
                if (this.phone_no) {
                        var reg = /^\d+$/;
                        if (!reg.test(this.phone_no)) {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('enter_valid_phone_no')+'.',
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        }
                }
                let form = {
                        'name': this.name,
                        'address_line_1': this.address_line_1,
                        'address_line_2': this.address_line_2,
                        'city': this.city,
                        'state': this.state,
                        'country': this.country,
                        'email_id': this.email,
                        'phone_no': this.phone_no,
                        'pincode': this.pincode,
                }
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present()
                this.service.post('account/change_account_details', form).then((result: any) => {
                        loader.dismiss()
                        localStorage.setItem('customer_id', result.user.customer_id)
                        localStorage.setItem('name', result.user.name)
                        localStorage.setItem('mobile_no', result.user.mobile_no)
                        localStorage.setItem('address_line_1', result.user.address_line_1)
                        localStorage.setItem('address_line_2', result.user.address_line_2)
                        localStorage.setItem('city', result.user.city)
                        localStorage.setItem('state', result.user.state)
                        localStorage.setItem('country', result.user.country)
                        localStorage.setItem('email_id', result.user.email_id)
                        localStorage.setItem('phone_no', result.user.phone_no)
                        localStorage.setItem('photo', result.user.photo)
                        localStorage.setItem('created_on', result.user.created_on)
                        localStorage.setItem('status', result.user.status)

                        if(this.cart.isPlaceOrder){

                                this.navCtrl.push(ShippingAddressPage)
                                
                        }else{
                                if (result.message) {
                                        let alert = this.alertCtrl.create({
                                                subTitle: result.message,
                                                buttons: [this.language.fnGetLanguage('title_ok')]
                                        });
                                        alert.present()
                                        alert.onDidDismiss(() => {
        
                                                if (this.cart.isPlaceOrder) {
                                                        this.navCtrl.push(ShippingAddressPage)
                                                } else {
                                                        this.navCtrl.setRoot(CategoryPage)
                                                }
        
                                        })
                                }
                        }

                }, (error: any) => {
                        loader.dismiss()
                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                        }else{
                                let alert = this.alertCtrl.create({
                                        subTitle:this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                        }
                })
        }

        getImage() {
                const options: CameraOptions = {
                        destinationType: this.camera.DestinationType.FILE_URI,
                        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
                }

                this.camera.getPicture(options).then((imageData) => {
                        this.imageURI = imageData;
                        this.uploadFile()
                }, (err) => {
                        this.presentToast(err);
                });
        }

        presentToast(msg) {
                let toast = this.toastCtrl.create({
                        message: msg,
                        duration: 3000,
                        position: 'bottom'
                });

                toast.onDidDismiss(() => {
                });

                toast.present();
        }

        uploadFile() {

                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present();
                const fileTransfer: FileTransferObject = this.transfer.create();

                let options: FileUploadOptions = {
                        chunkedMode: false,
                        fileKey: 'userfile',
                        fileName: 'userfile',
                        mimeType: "image/jpeg",
                        headers: {
                                'authorization': localStorage.getItem('token')
                        }
                }
                let form = new FormData();
                form.append('userfile', this.imageURI)
             
                fileTransfer.upload(this.imageURI, this.service.baseUrl + 'account/image_upload', options)
                        .then((data) => {
                                let response = JSON.parse(data.response)
                                if (response.status == 1) {
                                        this.imageFileName = response.img
                                        loader.dismiss();
                                        this.presentToast(this.language.fnGetLanguage("image_uploaded_successfully"));
                                } else {
                                        loader.dismiss();
                                        if(response.error){
                                                let alert = this.alertCtrl.create({
                                                        subTitle: response.error,
                                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                                });
                                                alert.present()
                                        }else{
                                                let alert = this.alertCtrl.create({
                                                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                                });
                                                alert.present()
                                        }
                                }
                        }, (err) => {
                                loader.dismiss();
                                this.presentToast(err);
                        });
        }

        fnchangeMobileNo() {
                this.navCtrl.push(ChangeMobileNoPage)
        }

}
