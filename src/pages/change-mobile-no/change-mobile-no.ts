import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { CustomeProvider } from '../../providers/custome/custome';
import { VerifyChangeMobilePage } from '../verify-change-mobile/verify-change-mobile';
import { LanguageProvider } from '../../providers/language/language';

/**
 * Generated class for the ChangeMobileNoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-change-mobile-no',
        templateUrl: 'change-mobile-no.html',
})
export class ChangeMobileNoPage {
        phoneNo;
        constructor(public navCtrl: NavController,private language:LanguageProvider, public alertCtrl: AlertController, public navParams: NavParams, private loadingCtrl: LoadingController, private service: CustomeProvider) {
        }

        ionViewDidLoad() {
        }
        fnChangeMobileNo() {
                var reg = /^\d+$/;
                if (!this.phoneNo || this.phoneNo == '' || this.phoneNo.length != 10 || !reg.test(this.phoneNo)) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('enter_valid_mobile_no')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                }
                let form = {
                        'mobile_no':this.phoneNo
                }
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present()
                this.service.post('account/change_mobile', form).then((result: any) => {
                        loader.dismiss()
                        this.navCtrl.push(VerifyChangeMobilePage, { 'data': this.phoneNo })
                }, (error: any) => {
                        loader.dismiss()
                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        }
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                })
        }

}
