import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, ToastController, Events, LoadingController, MenuController, Navbar, Platform, Loading } from 'ionic-angular';
import { CustomeProvider } from '../../providers/custome/custome';
import { CartProvider } from '../../providers/cart/cart';
import { CartPage } from '../cart/cart';
import { ImageViewerPage } from '../image-viewer/image-viewer';
import { CategoryPage } from '../category/category';
import { LanguageProvider } from '../../providers/language/language';
import * as $ from 'jquery';
/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-product',
        templateUrl: 'product.html',
})
export class ProductPage {
        loader: Loading;
        lazyLoadding = localStorage.getItem('lazyLoadding');
        height = window.innerHeight * 6 / 10;
        constructor(
                public navCtrl: NavController,
                public navParams: NavParams,
                public toast: ToastController,
                public event: Events,
                private platform: Platform,
                private language: LanguageProvider,
                private service: CustomeProvider,
                private cart: CartProvider,
                private modal: ModalController,
                private menu: MenuController,
                private loadingCtrl: LoadingController,
                private alertCtrl: AlertController
        ) {

        }
        appLang = localStorage.getItem('appLang')
        canvas;
        item: any;
        inputWidth = 65;
        cartItem: number;
        showLoader = false;
        @ViewChild(Navbar) navBar: Navbar;
        clientHeight: number;
        clientWidth: number;
        ionViewDidLoad() {
                this.appLang = localStorage.getItem('appLang')
                if (localStorage.getItem('notificationOpen') == '1') {
                        this.menu.enable(false)
                        this.navBar.backButtonClick = (e: UIEvent) => {
                                this.menu.enable(true)
                                this.navCtrl.pop();
                        }
                        localStorage.removeItem('notificationOpen')
                }
                this.cartItem = this.cart.fnGetCountOfCart();
                this.event.subscribe('cart:itemAdded', () => {
                        this.cartItem = this.cart.fnGetCountOfCart();

                });
                this.event.subscribe('appLangChange', () => {
                        this.appLang = this.language.fnGetSelectedAppLang()
                });
                this.event.subscribe('note:changed', () => {
                        var data = this.cart.fnGetCart()
                        let jj = 1;
                        if (data) {
                                for (let index = 0; index < data.length; index++) {
                                        if (data[index].item_id == this.item.item_id) {
                                                this.item.note = data[index].note
                                                jj = 0;
                                        }

                                }
                        }
                        if (jj) {
                                this.item.note = '';
                        }
                })
                let item = this.navParams.get('data')
                let form = {
                        'item_id': item.item_id
                }
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present();
                this.service.post('item/get_item_details', form).then((result: any) => {
                        this.item = result.item
                        this.item.note = ''
                        this.item.qty = parseInt(this.item.min_order_qty)
                        console.log(result)
                        var data = this.cart.fnGetCart()
                        if (data) {
                                for (let index = 0; index < data.length; index++) {
                                        if (data[index].item_id == result.item.item_id) {
                                                this.item.note = data[index].note
                                        }
                                }
                        }
                        this.showLoader = true;
                        loader.dismiss()
                }, (error: any) => {
                        loader.dismiss()
                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        }
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                })
        }

        fnOpenCart() {
                this.navCtrl.push(CartPage)
        }

        fnGotoNotificationPage() {
                this.navCtrl.setRoot(CategoryPage)
        }

        fnAddToCart() {
                var data = this.cart.fnGetCart()
                var j = 1;
                var selecdItemIndex
                if (data) {
                        for (let index = 0; index < data.length; index++) {
                                if (data[index].item_id == this.item.item_id) {
                                        j = 0
                                        selecdItemIndex = index;
                                }

                        }
                }
                if (j == 1) {
                        this.cart.fnAddToCart(this.item)
                        this.event.publish('cart:itemAdded');
                        this.toast.create({
                                message: this.language.fnGetLanguage("item_is_added_in_cart") + '.',
                                duration: 3000,
                                position: 'bottom',
                                cssClass: 'cstToast'
                        }).present()


                } else {
                        let flag = 0;
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('item_is_already_in_cart') + '.' + this.language.fnGetLanguage('do_you_want_to_proceed') + '?',
                                buttons: [
                                        {
                                                text: this.language.fnGetLanguage('cancel'),
                                                handler: () => {
                                                        flag = 0;
                                                        return true
                                                }
                                        },
                                        {
                                                text: this.language.fnGetLanguage('title_ok'),
                                                handler: () => {
                                                        flag = 1;
                                                        return true;
                                                }
                                        }
                                ]
                        });
                        alert.present()
                        alert.onDidDismiss((result) => {
                                if (flag == 1) {
                                        data[selecdItemIndex].qty = parseInt(data[selecdItemIndex].qty) + parseInt(this.item.qty)
                                        let item = JSON.parse(JSON.stringify(data[selecdItemIndex]))
                                        item.note = this.item.note
                                        this.cart.fnRemoveFromCart(selecdItemIndex)
                                        item.min_order_qty = parseInt(item.min_order_qty)
                                        this.cart.fnAddToCart(item)
                                        this.event.publish('cart:itemAdded');
                                }
                        })
                }

        }

        fnOpenImageViewer(data) {
                this.modal.create(ImageViewerPage, { data: this.item }).present().then((result) => {

                }, (error) => {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                }).catch((error) => {
                })
        }

        increment() {
                this.item.qty++;
        }

        fnQtyChange() {
                if (this.item && this.item.qty && this.item.qty > 0) {
                        if (parseInt(this.item.qty) < parseInt(this.item.min_order_qty)) {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('you_can_not_input_order_quantity_less_than_') + this.item.min_order_qty,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present()
                                alert.onDidDismiss((result) => {
                                        this.item.qty = JSON.parse(JSON.stringify(this.item.min_order_qty))
                                })
                        }
                } else {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('please_enter_a_quantity_greater_than_or_equal_to_') + this.item.min_order_qty,
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present()
                        alert.onDidDismiss((result) => {
                                this.item.qty = JSON.parse(JSON.stringify(this.item.min_order_qty))
                        })
                }
        }

        decrement() {
                if (this.item.qty > this.item.min_order_qty) {
                        this.item.qty--;
                } else {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('you_can_not_order_less_than_minimum_order_quantity'),
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                }
        }
}