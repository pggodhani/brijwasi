
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Events, Navbar, MenuController } from 'ionic-angular';
import { CustomeProvider } from '../../providers/custome/custome';
import { CartProvider } from '../../providers/cart/cart';
import { CategoryPage } from '../category/category';
import { SignupPage } from '../signup/signup';
import { ForgottPasswordPage } from '../forgott-password/forgott-password';
import { ShippingAddressPage } from '../shipping-address/shipping-address';
import { LanguageProvider } from '../../providers/language/language';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-login',
        templateUrl: 'login.html',
})
export class LoginPage {
        @ViewChild(Navbar) navBar: Navbar;
        constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public cart: CartProvider,
                private language:LanguageProvider,
                public loadingCtrl: LoadingController,
                public event: Events,
                public menu: MenuController,
                public service: CustomeProvider,
                private alertCtrl: AlertController) {
                if (localStorage.getItem('token')) {
                        this.navCtrl.setRoot(CategoryPage)
                }
        }
        phoneNo = '';
        password = '';
        ionViewDidLoad() {

                if (!this.navCtrl.canGoBack()) {
                        this.menu.enable(true);
                }
                this.navBar.backButtonClick = (e: UIEvent) => {
                        this.cart.isPlaceOrder = false;
                        this.navCtrl.pop();
                }
        }
        fnLogin() {
                let reg = /^\d+$/;
                if (!this.phoneNo || this.phoneNo == '' || this.phoneNo.length != 10 || !reg.test(this.phoneNo)) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('enter_valid_mobile_no.')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                }
                if (!this.password || this.password == '') {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('enter_valid_password')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                }

                let form;
                form = {
                        'mobile_no': this.phoneNo,
                        'password': this.password
                }
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present()
                this.service.post('login/verify_login', form).then((result: any) => {
                        loader.dismiss()
                        if (result.status == 1) {
                                localStorage.setItem('token', result.auth_token)
                                localStorage.setItem('customer_id', result.account_details.customer_id)
                                localStorage.setItem('name', result.account_details.name)
                                localStorage.setItem('mobile_no', result.account_details.mobile_no)
                                localStorage.setItem('address_line_1', result.account_details.address_line_1)
                                localStorage.setItem('address_line_2', result.account_details.address_line_2)
                                localStorage.setItem('city', result.account_details.city)
                                localStorage.setItem('state', result.account_details.state)
                                localStorage.setItem('country', result.account_details.country)
                                localStorage.setItem('email_id', result.account_details.email_id)
                                localStorage.setItem('phone_no', result.account_details.phone_no)
                                localStorage.setItem('photo', result.account_details.photo)
                                localStorage.setItem('created_on', result.account_details.created_on)
                                localStorage.setItem('status', result.account_details.status)
                                this.event.publish('userloggedin',result.account_details.mobile_no)
                                if (this.navCtrl.canGoBack()) {

                                        this.navCtrl.push(ShippingAddressPage)
                                } else {

                                        this.navCtrl.setRoot(CategoryPage)
                                }
                        } else {
                                let alert = this.alertCtrl.create({
                                        subTitle: result.message,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        }
                }, (error: any) => {
                        loader.dismiss()

                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        }
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                })
        }

        fnRegister() {
                this.navCtrl.push(SignupPage)
        }
        fnGoToForgottPasswordPage() {
                this.navCtrl.push(ForgottPasswordPage)
        }

}
