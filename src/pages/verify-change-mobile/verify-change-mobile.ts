import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { CustomeProvider } from '../../providers/custome/custome';
import { UserDetailPage } from '../user-detail/user-detail';
import { LanguageProvider } from '../../providers/language/language';

/**
 * Generated class for the VerifyChangeMobilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-verify-change-mobile',
        templateUrl: 'verify-change-mobile.html',
})
export class VerifyChangeMobilePage {

        constructor(public navCtrl: NavController,
                private language:LanguageProvider, private alertCtrl: AlertController, private service: CustomeProvider, private loadingCtrl: LoadingController, public navParams: NavParams) {
        }
        otp;
        phoneNo;
        second = 60;
        resenedOtpFlag = true;
        mobileNo;
        ionViewDidLoad() {
                this.phoneNo = this.navParams.get('data')
                if (this.phoneNo) {
                        let interval = setInterval(() => {
                                if (this.second > 0) {
                                        this.second--;
                                } else {
                                        this.resenedOtpFlag = false
                                        clearInterval(interval)
                                }
                        }, 1000)
                }
        }
        fnChangeMobileNo() {
                var reg = /^\d+$/;
                if (!this.otp || this.otp == '' || !reg.test(this.phoneNo)) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('enter_valid_otp')+'.',
                                buttons: [this.language.fnGetLanguage('title_OK')]
                        });
                        alert.present();
                        return false;
                }

                let form = {
                        'otp': this.otp,
                        'mobile_no': this.phoneNo
                }
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present()
                this.service.post('account/verify_change_mobile', form).then((result: any) => {
                        loader.dismiss()
                        localStorage.setItem('mobile_no',this.phoneNo)
                        if (result.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: result.message,
                                        buttons: [this.language.fnGetLanguage('title_OK')]
                                });
                                alert.present();
                                alert.onDidDismiss(() => {
                                        this.navCtrl.setRoot(UserDetailPage,{'data':true})
                                })

                        } else {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('your_mobile_no')+'.'+this.language.fnGetLanguage('has_been_changed')+'.',
                                        buttons: [this.language.fnGetLanguage('title_OK')]
                                });
                                alert.present();
                                alert.onDidDismiss(() => {
                                        this.navCtrl.setRoot(UserDetailPage,{'data':true})
                                })
                        }
                }, (error: any) => {
                        loader.dismiss()
                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_OK')]
                                });
                                alert.present();
                                return false;
                        } else {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_OK')]
                                });
                                alert.present();
                        }
                })
        }
        fnResendOtp() {
                this.second = 60
                this.resenedOtpFlag = true
                let form = {
                        'mobile_no': this.phoneNo,
                        'old_mobile_no':localStorage.getItem('mobile_no')
                }
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present()
                this.service.post('login/resend_otp_for_change_number', form).then((result: any) => {
                        loader.dismiss()
                        this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('otp_has_been_sent_on_your_mobile_no')+'.',
                                buttons: [this.language.fnGetLanguage('title_OK')]
                        }).present()
                        let interval = setInterval(() => {
                                if (this.second > 0) {
                                        this.second--;
                                } else {
                                        clearInterval(interval)
                                        this.resenedOtpFlag = false
                                }
                        }, 1000)
                }, (error: any) => {
                        loader.dismiss()
                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_OK')]
                                });
                                alert.present();
                                return false;
                        } else {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_OK')]
                                });
                                alert.present();
                        }
                })
        }
        fnRoutetoUSerDeatilPage() {
                this.service.post('account/get_account_details', 'nodata')
                        .then((result: any) => {
                                localStorage.setItem('customer_id', result.account_details.customer_id)
                                localStorage.setItem('name', result.account_details.name)
                                localStorage.setItem('mobile_no', result.account_details.mobile_no)
                                localStorage.setItem('address_line_1', result.account_details.address_line_1)
                                localStorage.setItem('address_line_2', result.account_details.address_line_2)
                                localStorage.setItem('city', result.account_details.city)
                                localStorage.setItem('state', result.account_details.state)
                                localStorage.setItem('country', result.account_details.country)
                                localStorage.setItem('email_id', result.account_details.email_id)
                                localStorage.setItem('phone_no', result.account_details.phone_no)
                                localStorage.setItem('photo', result.account_details.photo)
                                localStorage.setItem('created_on', result.account_details.created_on)
                                localStorage.setItem('status', result.account_details.status)
                                this.navCtrl.setRoot(UserDetailPage)                                
                        }, () => {
                                this.navCtrl.setRoot(UserDetailPage)

                        })
        }
}
