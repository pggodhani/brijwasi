import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { OtpVerifyPage } from '../otp-verify/otp-verify';
import { CustomeProvider } from '../../providers/custome/custome';
import { LanguageProvider } from '../../providers/language/language';

/**
 * Generated class for the ForgottPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-forgott-password',
        templateUrl: 'forgott-password.html',
})
export class ForgottPasswordPage {

        constructor(public navCtrl: NavController,private language:LanguageProvider,public loadingCtrl:LoadingController, public navParams: NavParams, public alertCtrl: AlertController, private service: CustomeProvider) {
        }
        phoneNo;
        ionViewDidLoad() {
        }
        fnForgottPassword() {
                var reg = /^\d+$/;
                if (!this.phoneNo || this.phoneNo == '' || this.phoneNo.length != 10 || !reg.test(this.phoneNo)) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('enter_valid_mobile_no')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                }
                let form = {
                        'mobile_no':this.phoneNo
                }
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present()
                this.service.post('login/forget_password', form).then((result: any) => {
                        this.navCtrl.push(OtpVerifyPage, { 'mobile_no': this.phoneNo })
                        loader.dismiss()
                }, (error: any) => {
                        loader.dismiss()                        
                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        } else {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                        }
                })
        }

}
