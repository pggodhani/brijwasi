import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Events, MenuController, ModalController } from 'ionic-angular';
import { ShippingAddressPage } from '../shipping-address/shipping-address';
import { CartProvider } from '../../providers/cart/cart';
import { LoginPage } from '../login/login';
import { ProductPage } from '../product/product';
import { UserDetailPage } from '../user-detail/user-detail';
import { NoteModalPage } from '../note-modal/note-modal';
import { LanguageProvider } from '../../providers/language/language';
import * as $ from 'jquery';
/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-cart',
        templateUrl: 'cart.html',
})
export class CartPage {
        item = 10
        cartData
        totalAmount;
        cartItem
        j = 1;
        lazyLoadding = localStorage.getItem('lazyLoadding');
        width = (window.innerWidth - 52) / 2
        appLang = localStorage.getItem('appLang')
        constructor(
                public navCtrl: NavController,
                public modalCtrl: ModalController,
                private language: LanguageProvider,
                public menu: MenuController,
                private event: Events,
                private alertCtrl: AlertController,
                public navParams: NavParams,
                public cart: CartProvider
        ) { }

        ionViewDidLoad() {
                this.appLang = localStorage.getItem('appLang')
                if (!this.navCtrl.canGoBack()) {
                        this.menu.enable(true);
                }
                if (this.j == 1) {
                        this.event.subscribe('cart:itemAdded', () => {
                                this.j = 0
                                this.cartItem = this.cart.fnGetCountOfCart();

                                this.ionViewDidLoad();
                        });
                }
                this.cartData = this.cart.fnGetCart();
                this.cartItem = this.cart.fnGetCountOfCart();
                let total: any = 0;
                if (this.cartData && this.cartData.length > 0) {
                        for (let index = 0; index < this.cartData.length; index++) {
                                total = parseInt(total) + this.cartData[index].qty * this.cartData[index].sale_price
                        }
                        this.totalAmount = total;
                }
                this.event.subscribe('appLangChange', () => {
                        this.appLang = this.language.fnGetSelectedAppLang()
                });
        }

        fnDecreaseQuantity(c, i) {
                if (c.qty > c.min_order_qty) {
                        let qty = parseInt(this.cartData[i].qty) - 1
                        this.cart.fnChangeQty(i, qty)
                        this.cartData = this.cart.fnGetCart()
                        let total: any = 0;
                        for (let index = 0; index < this.cartData.length; index++) {
                                total = parseInt(total) + this.cartData[index].qty * this.cartData[index].sale_price
                        }
                        this.totalAmount = total;
                        this.event.publish('cart:itemAdded')
                } else {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('you_can_not_input_order_quantity_less_than_') + c.min_order_qty,
                                enableBackdropDismiss: false,
                                buttons: [this.language.fnGetLanguage('title_ok')],
                        });
                        alert.present();
                }

        }

        fnIncreaseQuantity(c, i) {
                let qty = 1 + parseInt(this.cartData[i].qty)
                this.cart.fnChangeQty(i, qty)
                this.cartData = this.cart.fnGetCart()
                let total: any = 0;
                for (let index = 0; index < this.cartData.length; index++) {
                        // this.cartData[index].qty = this.cartData[index].min_order_qty
                        total = parseInt(total) + this.cartData[index].qty * this.cartData[index].sale_price
                }
                this.totalAmount = total;
                this.event.publish('cart:itemAdded')

        }

        fnPlaceOrder() {
                if (this.cartData && this.cartData.length > 0) {
                        this.cart.isPlaceOrder = true;
                        if (localStorage.getItem('token')) {
                                if (  !localStorage.getItem('name') || localStorage.getItem('name') == '' || localStorage.getItem('name') == 'null' || localStorage.getItem('name') == null) {
                                        this.navCtrl.push(UserDetailPage)

                                } else {
                                        this.navCtrl.push(ShippingAddressPage)

                                }
                        } else {
                                this.navCtrl.push(LoginPage)
                        }
                }
        }

        fnRemoveFromCart(c, i) {
                let f = 0;
                let alert = this.alertCtrl.create({
                        subTitle: this.language.fnGetLanguage('are_you_sure_to_remove_an_item_from_cart') + '?',
                        buttons: [
                                {
                                        text: this.language.fnGetLanguage('cancel'),
                                        handler: () => {
                                                f = 0;
                                                return true
                                        }
                                },
                                {
                                        text: this.language.fnGetLanguage('title_ok'),
                                        handler: () => {
                                                f = 1;
                                                return true;
                                        }
                                }
                        ]
                });
                alert.present()
                alert.onDidDismiss((result) => {
                        if (f == 1) {
                                this.cart.fnRemoveFromCart(i)
                                this.event.publish('cart:itemAdded')
                                this.event.publish('note:changed')
                                this.cartData = this.cart.fnGetCart();
                                this.cartItem = this.cart.fnGetCountOfCart();
                                let total: any = 0;
                                for (let index = 0; index < this.cartData.length; index++) {
                                        // this.cartData[index].qty = this.cartData[index].min_order_qty
                                        total = parseInt(total) + this.cartData[index].qty * this.cartData[index].sale_price
                                }
                                this.totalAmount = total;
                        }
                })



        }

        fnOpenProductPage(product) {
                this.navCtrl.push(ProductPage, { data: product })
        }

        onQtyChange(c, i) {
                if (parseInt(c.qty) && parseInt(c.qty) > 0) {
                        if (parseInt(c.qty) > parseInt(c.min_order_qty)) {
                                this.cart.fnChangeQty(i, parseInt(this.cartData[i].qty))
                                this.event.publish('cart:itemAdded')
                                this.ionViewDidLoad();
                        } else if (parseInt(c.qty) == parseInt(c.min_order_qty)) {
                                this.cart.fnChangeQty(i, parseInt(c.qty))
                                this.event.publish('cart:itemAdded')
                                this.ionViewDidLoad();
                        } else {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('you_can_not_input_order_quantity_less_than_') + c.min_order_qty,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present()
                                this.event.publish('cart:itemAdded')
                                alert.onDidDismiss((result) => {
                                        this.ionViewDidLoad();
                                })
                        }
                } else {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('please_enter_a_quantity_greater_than_or_equal_to_') + c.min_order_qty,
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present()
                        alert.onDidDismiss((result) => {
                                this.event.publish('cart:itemAdded')
                                this.ionViewDidLoad();
                        })

                }

        }

        fnShowNotes(c, i) {
                let modal = this.modalCtrl.create(NoteModalPage, { isCart: true, data: c })
                modal.present()
                modal.onDidDismiss((result) => {
                        if (result.addToCart) {
                                this.cartData[i].note = result.note
                                this.cart.fnNoteChange(i, result.note)
                                this.event.publish('note:changed')
                        } else {
                        }
                })
        }

        fnLoadImage(elementID) {
                let id = "#"+elementID
                $(id).css("background", "none");
                $(id).css("background", "#fff");
        }
}


