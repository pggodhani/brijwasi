import { Component } from '@angular/core';
import { NavController, NavParams, Events, LoadingController, AlertController, Platform, MenuController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { CustomeProvider } from '../../providers/custome/custome';
import { CartPage } from '../cart/cart';
import { CartProvider } from '../../providers/cart/cart';
import { NotoficationPage } from '../notofication/notofication';
import { ProductPage } from '../product/product';
import { SearchItemPage } from '../search-item/search-item';
import { LanguageProvider } from '../../providers/language/language';
import * as $ from 'jquery';

/**
 * Generated class for the CategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-category',
        templateUrl: 'category.html',
})
export class CategoryPage {
        categary = "all"
        categories;
        banners: any;
        cartItem: number;
        bank_detail: any;
        notificationCount;
        appUpdate = 0;
        searchString = '';
        appLang = localStorage.getItem('appLang')
        lazyLoadding = localStorage.getItem('lazyLoadding');
        width = (window.innerWidth - 52) / 2
        constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public event: Events,
                public platform: Platform,
                private language: LanguageProvider,
                public menuCtrl: MenuController,
                public alertCtrl: AlertController,
                private service: CustomeProvider,
                private cart: CartProvider,
                private loadingCtrl: LoadingController) { }


        ionViewDidLoad() {
             
                this.menuCtrl.enable(false);
                this.appLang = localStorage.getItem('appLang')
                this.platform.ready().then(() => {
                        if (localStorage.getItem('notificationOpen') == '1') {
                                this.fnGoToProductPageForNotification()
                        }
                        this.menuCtrl.enable(true);
                        this.cartItem = this.cart.fnGetCountOfCart();
                        this.event.subscribe('cart:itemAdded', () => {
                                this.cartItem = this.cart.fnGetCountOfCart();
                        });
                        this.event.subscribe('notification:vieved', () => {
                                this.notificationCount = 0
                        });
                        let loader = this.loadingCtrl.create({
                                content: 'Loading...'
                        });
                        this.event.subscribe('appLangChange', () => {
                                this.appLang = this.language.fnGetSelectedAppLang()
                        });
                        loader.present()
                        this.platform.ready().then(() => {
                                let data = {}
                                if (localStorage.getItem('notificationTime')) {
                                        data['last_seen'] = localStorage.getItem('notificationTime')
                                }
                                this.service.post('category/get_category', data).then((result: any) => {
                                        this.banners = result.banner;
                                        this.categories = result.category;
                                        console.log(this.categories)
                                        console.log(this.banners)
                                        this.bank_detail = result.bank_detail
                                        this.notificationCount = result.item_count
                                        this.event.publish('category:updated', this.categories);
                                        this.event.publish('userNotloggedin', result.helpline_number);
                                        localStorage.setItem('helpline', result.helpline_number)
                                        localStorage.setItem('whatsapp_number', result.whatsapp_number)
                                        loader.dismiss()
                                }, (error: any) => {
                                        loader.dismiss()
                                        if (error.message) {
                                                let alert = this.alertCtrl.create({
                                                        subTitle: error.message,
                                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                                });
                                                alert.present();
                                                return false;
                                        }
                                        let alert = this.alertCtrl.create({
                                                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes') + '.',
                                                buttons: [this.language.fnGetLanguage('title_ok')]
                                        });
                                        alert.present();
                                })
                        })
                })
        }
        fnOpenCart() {
                this.navCtrl.push(CartPage)

        }
        fnOpenNotificationPage() {
                this.navCtrl.push(NotoficationPage)
        }
        fnCategary(category) {
                this.navCtrl.push(HomePage, { 'data': category })
        }
        fnImageNotFound(event) {
                event.target.src = "http://www.sunerpsolutions.in/hr/img/category-block-thumb.jpg"
        }
        fnBannerNotFound(event) {
                event.target.src = " http://www.sunerpsolutions.in/hr/img/banner.jpg"
        }
        fnGoToProductPageForNotification() {
                this.navCtrl.push(ProductPage, { 'data': this.navParams.get('data') })
        }
        fnSearch() {
                if (this.searchString && this.searchString != '') {
                        let str = this.searchString
                        this.searchString = '';
                        this.navCtrl.push(SearchItemPage, { 'data': str })
                }
        }
        fnLoadImage(elementID) {
                let id = "#"+elementID
                $(id).css("background", "none");
                $(id).css("background", "#fff");
        }
}
