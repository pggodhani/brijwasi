import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { CategoryPage } from '../category/category';
import { LanguageProvider } from '../../providers/language/language';

/**
 * Generated class for the OrderSuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-order-success',
        templateUrl: 'order-success.html',
})
export class OrderSuccessPage {

        constructor(public navCtrl: NavController,
                private language:LanguageProvider,private menu:MenuController, public navParams: NavParams) {
        }

        ionViewDidLoad() {
                if(!this.navCtrl.canGoBack()){
                        this.menu.enable(true);
                }
                console.log('ionViewDidLoad OrderSuccessPage');
        }
        fnGotToCategoryPage() {
                this.navCtrl.setRoot(CategoryPage)
        }
}
