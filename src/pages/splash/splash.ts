import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Platform } from 'ionic-angular/platform/platform';
import { LanguageProvider } from '../../providers/language/language';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { CustomeProvider } from '../../providers/custome/custome';
import { CartProvider } from '../../providers/cart/cart';
import { Market } from '@ionic-native/market';
import { SettingsPage } from '../settings/settings';
import { CategoryPage } from '../category/category';

/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-splash',
        templateUrl: 'splash.html',
})
export class SplashPage {

        constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public platform: Platform,
                private language: LanguageProvider,
                public menuCtrl: MenuController,
                public alertCtrl: AlertController,
                private service: CustomeProvider,
                private cart: CartProvider,
                private market: Market,
                private langService: LanguageProvider) {
        }

        ionViewDidLoad() {
                this.menuCtrl.enable(false);
                this.platform.ready().then(() => {
                        if (localStorage.getItem('notificationOpen') == '1') {
                                this.fnGoToProductPageForNotification()
                        } else {
                                if (this.cart.appUpdate == 0) {
                                        if (this.platform.is('ios')) {
                                                this.service.post('startup/check_apple_version_update', { version: '1.0.3' })
                                                        .then((result: any) => {
                                                                if (result && result.maintenance && result.maintenance.apple) {
                                                                        let alert = this.alertCtrl.create({
                                                                                subTitle: result.maintenance.msg,
                                                                                enableBackdropDismiss: false,

                                                                                buttons: [
                                                                                        {
                                                                                                text: this.language.fnGetLanguage('close'),
                                                                                                handler: () => {
                                                                                                        this.platform.exitApp();
                                                                                                }
                                                                                        }
                                                                                ]
                                                                        });
                                                                        alert.present();
                                                                        alert.onDidDismiss(() => {
                                                                                this.platform.exitApp();
                                                                        })
                                                                } else if (result.update_status == 2 || result.update_status == '2') {
                                                                        let alert = this.alertCtrl.create({
                                                                                subTitle: this.language.fnGetLanguage('please_update_an_app'),
                                                                                enableBackdropDismiss: false,
                                                                                buttons: [
                                                                                        {
                                                                                                text: this.language.fnGetLanguage('update'),
                                                                                                handler: () => {
                                                                                                        this.market.open(result.package_name).then((r) => {
                                                                                                                // this.market.open()
                                                                                                        })

                                                                                                }
                                                                                        }
                                                                                ]
                                                                        });
                                                                        alert.present();
                                                                        alert.onDidDismiss(() => {
                                                                                this.platform.exitApp();
                                                                        })
                                                                } else if (result.update_status == 1 || result.update_status == '1') {
                                                                        this.cart.appUpdate = 1;
                                                                        let alert = this.alertCtrl.create({
                                                                                subTitle: this.language.fnGetLanguage('new_update_available') + '.',
                                                                                enableBackdropDismiss: false,
                                                                                buttons: [
                                                                                        {
                                                                                                text: this.language.fnGetLanguage('close'),
                                                                                                handler: () => {
                                                                                                        if (result.multy_language) {
                                                                                                                if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                                                                                                        this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                                                                                                        this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'))
                                                                                                                        this.navCtrl.setRoot(CategoryPage)
                                                                                                                } else {
                                                                                                                        this.navCtrl.setRoot(SettingsPage)
                                                                                                                }
                                                                                                        } else {
                                                                                                                this.navCtrl.setRoot(CategoryPage)

                                                                                                        }
                                                                                                }
                                                                                        },
                                                                                        {
                                                                                                text: this.language.fnGetLanguage('update'),
                                                                                                handler: () => {
                                                                                                        this.market.open('com.brijwasibj').then((r) => {
                                                                                                                // open a play store
                                                                                                        },(e)=>{

                                                                                                        })
                                                                                                }
                                                                                        }
                                                                                ]
                                                                        });
                                                                        alert.present();
                                                                } else if (result.multy_language) {
                                                                        if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                                                                this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                                                                this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'))
                                                                                this.navCtrl.setRoot(CategoryPage)
                                                                        } else {
                                                                                this.navCtrl.setRoot(SettingsPage)
                                                                        }
                                                                } else {
                                                                        this.navCtrl.setRoot(CategoryPage)
                                                                }
                                                        }, (error: any) => {
                                                                // error
                                                                if (error && error.msg) {
                                                                        this.alertCtrl.create({
                                                                                message: error.msg,
                                                                                buttons: [
                                                                                        {
                                                                                                text: 'OK',
                                                                                                handler: () => {
                                                                                                        this.platform.exitApp()
                                                                                                }
                                                                                        }
                                                                                ]
                                                                        }).present()
                                                                }

                                                        })
                                        } else if (this.platform.is('android')) {
                                                this.service.post('startup/check_android_version_update', { version: '1.0.3' })
                                                        .then((result: any) => {
                                                                if (result && result.maintenance && result.maintenance.android) {
                                                                        let alert = this.alertCtrl.create({
                                                                                subTitle: result.maintenance.msg,
                                                                                enableBackdropDismiss: false,

                                                                                buttons: [
                                                                                        {
                                                                                                text: this.language.fnGetLanguage('close'),
                                                                                                handler: () => {
                                                                                                        this.platform.exitApp();
                                                                                                }
                                                                                        }
                                                                                ]
                                                                        });
                                                                        alert.present();
                                                                        alert.onDidDismiss(() => {
                                                                                this.platform.exitApp();
                                                                        })
                                                                } else if (result.update_status == 2 || result.update_status == '2') {
                                                                        let alert = this.alertCtrl.create({
                                                                                subTitle: this.language.fnGetLanguage('please_update_an_app'),
                                                                                enableBackdropDismiss: false,

                                                                                buttons: [
                                                                                        {
                                                                                                text: this.language.fnGetLanguage('close'),
                                                                                                handler: () => {
                                                                                                        this.platform.exitApp();
                                                                                                }
                                                                                        },
                                                                                        {
                                                                                                text: 'Update',
                                                                                                handler: () => {
                                                                                                        this.market.open(result.package_name);
                                                                                                        this.platform.exitApp();
                                                                                                }
                                                                                        }
                                                                                ]
                                                                        });
                                                                        alert.present();
                                                                        alert.onDidDismiss(() => {
                                                                                this.platform.exitApp();
                                                                        })
                                                                } else if (result.update_status == 1 || result.update_status == '1') {
                                                                        this.cart.appUpdate = 1;
                                                                        let alert = this.alertCtrl.create({
                                                                                subTitle: this.language.fnGetLanguage('new_update_available') + '.',
                                                                                enableBackdropDismiss: false,

                                                                                buttons: [
                                                                                        {
                                                                                                text: this.language.fnGetLanguage('cancel'),
                                                                                                handler: () => {
                                                                                                        if (result.multy_language) {
                                                                                                                if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                                                                                                        this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                                                                                                        this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'))
                                                                                                                        this.navCtrl.setRoot(CategoryPage)
                                                                                                                } else {
                                                                                                                        this.navCtrl.setRoot(SettingsPage)
                                                                                                                }
                                                                                                        } else {
                                                                                                                this.navCtrl.setRoot(CategoryPage)

                                                                                                        }
                                                                                                }
                                                                                        },
                                                                                        {
                                                                                                text: this.language.fnGetLanguage('update'),
                                                                                                handler: () => {
                                                                                                        this.market.open(result.package_name);
                                                                                                        this.platform.exitApp();
                                                                                                }
                                                                                        }
                                                                                ]
                                                                        });
                                                                        alert.present();
                                                                } else if (result.multy_language) {
                                                                        if (localStorage.getItem('appLang') && localStorage.getItem('langArray')) {
                                                                                this.langService.fnSetAppLanguage(localStorage.getItem('appLang'));
                                                                                this.langService.labelLanguageArray = JSON.parse(localStorage.getItem('langArray'))
                                                                                this.navCtrl.setRoot(CategoryPage)
                                                                        } else {
                                                                                this.navCtrl.setRoot(SettingsPage)
                                                                        }
                                                                } else {
                                                                        this.navCtrl.setRoot(CategoryPage)
                                                                }
                                                        }, (error: any) => {
                                                                //error
                                                                if (error && error.msg) {
                                                                        this.alertCtrl.create({
                                                                                message: error.msg,
                                                                                buttons: [
                                                                                        {
                                                                                                text: 'OK',
                                                                                                handler: () => {
                                                                                                        this.platform.exitApp()
                                                                                                }
                                                                                        }
                                                                                ]
                                                                        }).present()
                                                                } else {
                                                                        this.alertCtrl.create({
                                                                                message: 'Something happened wrong try again after sometimes.',
                                                                                buttons: [
                                                                                        {
                                                                                                text: 'OK',
                                                                                                handler: () => {
                                                                                                        this.platform.exitApp()
                                                                                                }
                                                                                        }
                                                                                ]
                                                                        }).present()
                                                                }
                                                        })
                                        }
                                } else {
                                        this.cart.appUpdate = 1
                                }
                        }
                })
        }

        fnGoToProductPageForNotification() {
                this.navCtrl.setRoot(CategoryPage, { 'data': this.navParams.get('data') })
        }
}
