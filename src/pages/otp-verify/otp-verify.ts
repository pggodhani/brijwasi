import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Events } from 'ionic-angular';
import { CustomeProvider } from '../../providers/custome/custome';
import { UserDetailPage } from '../user-detail/user-detail';
import { CategoryPage } from '../category/category';
import { CartProvider } from '../../providers/cart/cart';
import { ShippingAddressPage } from '../shipping-address/shipping-address';
import { LanguageProvider } from '../../providers/language/language';

/**
 * Generated class for the OtpVerifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-otp-verify',
        templateUrl: 'otp-verify.html',
})
export class OtpVerifyPage {

        constructor(public navCtrl: NavController,
                private language:LanguageProvider,private cart:CartProvider, public event: Events, private loadingCtrl: LoadingController, public navParams: NavParams, private service: CustomeProvider, public alertCtrl: AlertController) {
        }
        phoneNo;
        password = '';
        conirmPassword = '';
        otp = '';
        firstStap = true;
        resenedOtpFlag = true;
        second = 60;
        ForgottPAsswordIcon = false
        ionViewDidLoad() {
                this.phoneNo = this.navParams.get('mobile_no')
                if (this.navCtrl.getPrevious().name == 'ForgottPasswordPage') {
                        this.ForgottPAsswordIcon = true
                }
                if (this.phoneNo) {
                        let interval = setInterval(() => {
                                if (this.second > 0) {
                                        this.second--;
                                } else {
                                        this.resenedOtpFlag = false
                                        clearInterval(interval)
                                }
                        }, 1000)
                }
        }

        fnRegisterSecondStap() {
                if (!this.otp || this.otp == '' ) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('enter_valid_otp')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                }
                if (!this.password || this.password == '' ) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('enter_valid_password')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                }
                if (this.password != this.conirmPassword) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('password_and_confirm_password_does_not_match')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                }
                let form = {
                        'otp': this.otp,
                        'password': this.password,
                        'mobile_no': this.phoneNo
                }
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present()
                this.service.post('login/create_password', form).then((result: any) => {
                        let cart = localStorage.getItem('cart')
                        localStorage.clear()
                        localStorage.setItem('cart', cart)
                        loader.dismiss()
                        localStorage.setItem('token', result.auth_token)
                        this.service.token = result.auth_token
                        this.service.post('account/get_account_details', form).then((result: any) => {
                                localStorage.setItem('customer_id', result.user.customer_id)
                                localStorage.setItem('name', result.user.name)
                                localStorage.setItem('mobile_no', result.user.mobile_no)
                                localStorage.setItem('address_line_1', result.user.address_line_1)
                                localStorage.setItem('address_line_2', result.user.address_line_2)
                                localStorage.setItem('city', result.user.city)
                                localStorage.setItem('state', result.user.state)
                                localStorage.setItem('country', result.user.country)
                                localStorage.setItem('email_id', result.user.email_id)
                                localStorage.setItem('phone_no', result.user.phone_no)
                                localStorage.setItem('photo', result.user.photo)
                                localStorage.setItem('created_on', result.user.created_on)
                                localStorage.setItem('status', result.user.status)

                                this.event.publish('userloggedin')

                                if (this.ForgottPAsswordIcon) {
                                        let alert = this.alertCtrl.create({
                                                subTitle: this.language.fnGetLanguage('new_password_has_been_set')+'.',
                                                buttons: [this.language.fnGetLanguage('title_ok')]
                                        });
                                        alert.present();
                                        alert.onDidDismiss((result) => {
                                                if(this.cart.isPlaceOrder){
                                                        if(!localStorage.getItem('name') || localStorage.getItem('name') == '' || localStorage.getItem('name')=='null'||localStorage.getItem('name')==null){
                                                                this.navCtrl.setRoot(UserDetailPage)
                                                        }else{
                                                                this.navCtrl.push(ShippingAddressPage)
                                                        }
                                                }else{
                                                        this.navCtrl.setRoot(CategoryPage)
                                                }
                                        })
                                } else {
                                        if(this.cart.isPlaceOrder){
                                                if(!localStorage.getItem('name') || localStorage.getItem('name') == '' || localStorage.getItem('name')=='null'||localStorage.getItem('name')==null){
                                                        this.navCtrl.push(UserDetailPage)
                                                }else{
                                                        this.navCtrl.push(ShippingAddressPage)
                                                }
                                        }else{
                                                this.navCtrl.setRoot(UserDetailPage)
                                        }
                                }
                        }, (error: any) => {
                                loader.dismiss()
                                this.event.publish('userloggedin')
                                if (this.ForgottPAsswordIcon) {
                                        let alert = this.alertCtrl.create({
                                                subTitle: this.language.fnGetLanguage('new_password_set')+'.',
                                                buttons: [this.language.fnGetLanguage('title_ok')]
                                        });
                                        alert.present();
                                        alert.onDidDismiss((result) => {
                                                if(this.cart.isPlaceOrder){
                                                        if(!localStorage.getItem('name') || localStorage.getItem('name') == '' || localStorage.getItem('name')=='null'||localStorage.getItem('name')==null){
                                                                this.navCtrl.push(UserDetailPage)
                                                        }else{
                                                                this.navCtrl.push(ShippingAddressPage)
                                                        }
                                                }else{
                                                        this.navCtrl.setRoot(CategoryPage)
                                                }
                                        })
                                } else {
                                        if(this.cart.isPlaceOrder){
                                                if(!localStorage.getItem('name') || localStorage.getItem('name') == '' || localStorage.getItem('name')=='null'||localStorage.getItem('name')==null){
                                                        this.navCtrl.push(UserDetailPage)
                                                }else{
                                                        this.navCtrl.push(ShippingAddressPage)
                                                }
                                        }else{
                                                this.navCtrl.setRoot(UserDetailPage)
                                        }
                                }
                        })
                }, (error: any) => {
                        loader.dismiss()
                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        } else {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                        }
                })
        }

        fnResendOtp() {
                this.second = 60
                this.resenedOtpFlag = true
                let form = {};
                form = {
                        'mobile_no': this.phoneNo
                }
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present()
                this.service.post('login/resend_otp', form).then((result: any) => {
                        loader.dismiss()
                        this.alertCtrl.create({
                                subTitle:this.language.fnGetLanguage('otp_has_been_sent_on_your_mobile_no')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        }).present()
                        let interval = setInterval(() => {
                                if (this.second > 0) {
                                        this.second--;
                                } else {
                                        clearInterval(interval)
                                        this.resenedOtpFlag = false

                                }
                        }, 1000)
                }, (error: any) => {
                        loader.dismiss()
                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        } else {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                        }
                })
        }
}
