import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, Platform, MenuController, Events } from 'ionic-angular';
import { CustomeProvider } from '../../providers/custome/custome';
import { CategoryPage } from '../category/category';
import { LanguageProvider } from '../../providers/language/language';

@Component({
        selector: 'page-settings',
        templateUrl: 'settings.html',
})
export class SettingsPage {
        languages = []
        lang:any;
        constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public service: CustomeProvider,
                public loader: LoadingController,
                private alertCtrl: AlertController,
                private platform: Platform,
                private menuCtrl: MenuController,
                private language: LanguageProvider,
                private events:Events,
                public LanguageService: LanguageProvider
        ) {
        }

        ionViewDidLoad() {
                console.log('ionViewDidLoad SettingsPage');
                this.menuCtrl.enable(false);
                this.getLanguageList();
        }

        getLanguageList() {
                let loader = this.loader.create({ content: 'Please wait...' })
                loader.present();
                this.service.post('startup/get_lang', {}).then((result: any) => {
                        loader.dismiss();
                        this.languages = []
                        let languages = result.language
                        for (var k in languages) {
                                this.languages.push({ key: k, value: languages[k] })
                        }
                        this.lang = this.languages[0]
                        this.LanguageService.fnSetLanguageArray(this.languages)
                        this.LanguageService.fnSetLangLabelArray(result.label)

                }, (error: any) => {
                        loader.dismiss();
                        if (error && error.msg && error.msg != '') {
                                this.alertCtrl.create(
                                        {
                                                message: error.msg,
                                                buttons: [
                                                        {
                                                                text: this.language.fnGetLanguage('title_ok'),
                                                                handler: data => {
                                                                        this.platform.exitApp();
                                                                }
                                                        }
                                                ]
                                        }
                                ).present()
                        } else {
                                this.alertCtrl.create(
                                        {
                                                message: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                                buttons: [
                                                        {
                                                                text: this.language.fnGetLanguage('title_ok'),
                                                                handler: data => {
                                                                        this.platform.exitApp();
                                                                }
                                                        }
                                                ]
                                        }
                                ).present()
                        }
                })
        }

        fnSelectLanguage(lang) {
                this.lang = lang
        }


        fnChoseLang(){
                this.LanguageService.fnSetAppLanguage(this.lang.key)
                this.events.publish('langSet')
                this.navCtrl.setRoot(CategoryPage)   
        }

}
