import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { CustomeProvider } from '../../providers/custome/custome';
import { CategoryPage } from '../category/category';
import { LanguageProvider } from '../../providers/language/language';

/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-change-password',
        templateUrl: 'change-password.html',
})
export class ChangePasswordPage {
        oldPassword
        conformPassword
        newPassword;
        constructor(public navCtrl: NavController, private language: LanguageProvider, public loadingCtrl: LoadingController, public navParams: NavParams, private alertCtrl: AlertController, private service: CustomeProvider) {
        }

        ionViewDidLoad() {
        }
        fnChangePassword() {
                if (!this.oldPassword || this.oldPassword.length < 6) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('enter_valid_password'),
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present()
                        return false;
                }
                if (!this.newPassword || this.newPassword.length < 6) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('password_length_must_be_minimum') + ' 6',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present()
                        return false;
                }
                if (this.newPassword != this.conformPassword) {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('new_password_and_conform_password_are_diffrent') + '.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present()
                        return false;
                }
                let form = {
                        'password': this.oldPassword,
                        'new_password': this.conformPassword
                }
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present()
                this.service.post('account/change_password', form).then((result: any) => {
                        loader.dismiss()
                        let msg = ('your_password_has_been_changed') + '.'
                        if (result.message) {
                                msg = result.message
                        }
                        let alert = this.alertCtrl.create({
                                subTitle: msg,
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present()
                        alert.onDidDismiss((result) => {
                                this.navCtrl.setRoot(CategoryPage)
                        })
                }, (error) => {
                        loader.dismiss()
                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        }
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                })

        }

}
