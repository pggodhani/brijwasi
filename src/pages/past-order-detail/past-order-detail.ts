import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, Events, ModalController } from 'ionic-angular';
import { CartPage } from '../cart/cart';
import { CustomeProvider } from '../../providers/custome/custome';
import { CartProvider } from '../../providers/cart/cart';
import { NoteModalPage } from '../note-modal/note-modal';
import { LanguageProvider } from '../../providers/language/language';
import * as $ from 'jquery';
/**
 * Generated class for the PastOrderDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-past-order-detail',
        templateUrl: 'past-order-detail.html',
})
export class PastOrderDetailPage {

        constructor(public navCtrl: NavController,private language:LanguageProvider, public modalCtrl: ModalController, public event: Events, public navParams: NavParams, private cart: CartProvider, public service: CustomeProvider, private loadingCtrl: LoadingController, private alertCtrl: AlertController) {
        }
        order;
        items;
        orderData;
        cartItem;
        lazyLoadding = localStorage.getItem('lazyLoadding');
        width = (window.innerWidth - 52) / 2
        appLang = localStorage.getItem('appLang')
        ionViewDidLoad() {
                
                // this.event.unsubscribe('cart:itemAdded')
                this.cartItem = this.cart.fnGetCountOfCart();
                this.event.subscribe('cart:itemAdded', () => {
                        this.cartItem = this.cart.fnGetCountOfCart();
                });
                this.event.subscribe('appLangChange', () => {
                        this.appLang = this.language.fnGetSelectedAppLang()
                });
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                })
                loader.present();
                let form = {
                        'order_id': this.navParams.get('data').order_id
                }
                this.order = this.navParams.get('data')
                this.service.post('order/get_order_details', form).then((result) => {
                        this.items = result.order.items
                        this.orderData = result.order
                        loader.dismiss()
                }, (error) => {
                        loader.dismiss()
                        if (error.message) {
                                let alert = this.alertCtrl.create({
                                        subTitle: error.message,
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        }
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                })
        }
        fnOpenCart() {
                this.navCtrl.push(CartPage)
        }
        fnShowNotes(item) {
                let modal = this.modalCtrl.create(NoteModalPage, { isCart: false, data: item })
                if (item && item.note) {
                        modal.present()
                } else {
                        this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('no_notes_for_this_product')+'.',
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        }).present()
                }
        }
        fnLoadImage(elementID) {
                let id = "#"+elementID
                $(id).css("background", "none");
                $(id).css("background", "#fff");
        }
}
