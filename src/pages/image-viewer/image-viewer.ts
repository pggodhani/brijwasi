import { Component, ViewChild, ElementRef, Renderer, ViewChildren } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController, Slides, LoadingController, Loading, ToastController, Platform, AlertController } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
import 'fabric';
declare const fabric: any;
import { Base64ToGallery } from '@ionic-native/base64-to-gallery';
import { CustomeProvider } from '../../providers/custome/custome';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LanguageProvider } from '../../providers/language/language';
import { ImageViewerController } from '../../Library/ionic-img-viewer/dist/es2015/ionic-img-viewer';
/**
 * Generated class for the ImageViewerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-image-viewer',
        templateUrl: 'image-viewer.html',
})
export class ImageViewerPage {
        loader: Loading;
        clientHeight: number;
        clientWidth: number;
        @ViewChild(Slides) slides: Slides;
        @ViewChildren('cmp') components;
        selectedSlide = 0;
        value = 0;
        constructor(public navCtrl: NavController,
                private localNotifications: LocalNotifications,
                public elementRef: ElementRef,
                private language: LanguageProvider,
                private viewCtrl: ViewController,
                public navParams: NavParams,
                public loadingCtrl: LoadingController,
                public modal: ModalController,
                private androidPermissions: AndroidPermissions,
                private toastCtrl: ToastController,
                private imageViewerCtrl: ImageViewerController,
                private platform: Platform,
                public service: CustomeProvider,
                private base64ToGallery: Base64ToGallery,
                public el: ElementRef,
                private photoLibrary: PhotoLibrary,
                private alertCtrl: AlertController,
                public renderer: Renderer) {
        }
        data;
        height = window.innerHeight - 120
        canvas;
        item;
        child

        ionViewDidLoad() {
                this.item = this.navParams.get('data')
                this.data = this.navParams.get('data').gallary
                this.loader = this.loadingCtrl.create({
                        content: 'Please wait...'
                });


        }

        fnCloseImageViewer() {
                this.viewCtrl.dismiss().then(() => {
                }, () => {
                })
        }

        slideChanged() {
                let index = this.slides.getActiveIndex()
                if (index < this.data.length) {
                        this.selectedSlide = index;
                }
        }

        fnchangeSlide(i) {
                this.slides.slideTo(i)
        }


        fnDownloadImage(img, id) {
                this.localNotifications.schedule({
                        id: 1,
                        title: this.language.fnGetLanguage('image_is_downloading'),
                        progressBar: { value: this.value }
                });
                if (this.platform.is('ios')) {
                        this.loader = this.loadingCtrl.create({
                                content: 'Please wait...'
                        });
                        this.loader.present()
                        this.download(img, this.item.item_code)
                } else {
                        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then((result: any) => {
                                if (result.hasPermission) {
                                        this.loader = this.loadingCtrl.create({
                                                content: 'Please wait...'
                                        });
                                        this.loader.present()
                                        this.getMeta(img, id)
                                } else {
                                        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE]).then((r: any) => {
                                                if (r.hasPermission) {
                                                        this.loader = this.loadingCtrl.create({
                                                                content: 'Please wait...'
                                                        });
                                                        this.loader.present()
                                                        this.getMeta(img, id)
                                                } else {
                                                        this.alertCtrl.create({ 'message': this.language.fnGetLanguage('please_give_permission_to_save_file') }).present()
                                                }
                                        }, (error) => {
                                                this.alertCtrl.create({ 'message': this.language.fnGetLanguage('error_while_saving_a_file')+'.' }).present()
                                        })
                                }
                        }, (error) => {
                                this.alertCtrl.create({ 'message': this.language.fnGetLanguage('error_while_saving_a_file')+'.' }).present()
                        })
                }
        }

        download(url, text) {
                this.photoLibrary.requestAuthorization({
                        read: true,
                        write: true
                }).then((res) => {
                        let mainUrl = this.service.baseUrl + 'item/image_addtext?image_path=' + url + '&item_name=' + text
                        this.photoLibrary.saveImage(mainUrl, 'HRSALES').then((result: any) => {
                                this.localNotifications.clear(1)
                                this.localNotifications.schedule({
                                        id: 2,
                                        title: this.item.item_name + this.language.fnGetLanguage("image_downloded")
                                })
                                this.toastCtrl.create({
                                        message: this.language.fnGetLanguage('image_is_saved_successfully'),
                                        duration: 3000,

                                }).present()
                                this.loader.dismiss();
                        }, (error) => {
                                this.loader.dismiss();
                                this.toastCtrl.create({
                                        message: this.language.fnGetLanguage('image_can_not_be_downloaded')+'.',
                                        duration: 3000,
                                }).present();
                                this.localNotifications.clear(1)
                                this.localNotifications.schedule({
                                        id: 2,
                                        title: this.language.fnGetLanguage('error_occured_during_image_saving')
                                });
                        })
                }, (err) => {
                        this.loader.dismiss();
                        this.alertCtrl.create({message:err}).present()
                })
        }

        fnOpenImageZoomer(cmp, i) {
                let imageViewer = this.imageViewerCtrl.create(this.components._results[i].nativeElement
                );
                imageViewer.present();
        }

        getMeta(url, id) {
                this.value = 0
                var img = document.getElementById('gallary' + id);
                this.clientWidth = img.clientWidth
                this.clientHeight = img.clientHeight
                this.canvas = new fabric.Canvas('canvas', {
                        hoverCursor: 'pointer',
                        selection: true,
                        selectionBorderColor: 'blue',
                });
                fabric.Image.fromURL(url, (image) => {
                        this.clientWidth = image.width
                        this.clientHeight = image.height
                        this.canvas.setHeight(this.clientHeight);
                        this.canvas.setWidth(this.clientWidth);
                        this.canvas.add(image)
                        let textString = this.item.item_code;
                        let text = new fabric.Text(textString, {
                                fontFamily: 'helvetica',
                                fill: '#000000',
                                right: 100,
                                top: 100,
                                fontSize: 80,
                                left: 100,
                                fontWeight: 'bold',
                                objectType: 'text',
                                hasRotatingPoint: false
                        });
                        this.canvas.add(text);
                        var imageDownload = new Image();
                        imageDownload.crossOrigin = 'anonymous'
                        imageDownload.src = this.canvas.toDataURL('jpeg', 1.0)
                        this.downloadAndroid(imageDownload.src)
                })
        }

        downloadAndroid(url) {
                this.base64ToGallery.base64ToGallery(url)
                        .then((res) => {
                                this.localNotifications.clear(1)
                                this.localNotifications.schedule({
                                        id: 2,
                                        title: this.item.item_name+' ' +this.language.fnGetLanguage('image_downloded')
                                });
                                this.toastCtrl.create({
                                        message: this.language.fnGetLanguage('image_is_saved_successfully'),
                                        duration: 3000,

                                }).present()

                                this.value = 100;
                                this.canvas = null;
                                this.loader.dismiss()


                        }, (err) => {

                                this.localNotifications.clear(1)
                                this.localNotifications.schedule({
                                        id: 2,
                                        title: this.language.fnGetLanguage('error_saving_image_to_gallery') +','+ err
                                });
                                this.value = 100;
                                this.value = 100;
                                this.loader.dismiss()
                                this.toastCtrl.create({
                                        message: this.language.fnGetLanguage('image_can_not_be_download')+'.',
                                        duration: 3000,
                                }).present();
                        }
                        );
                
        }
}
