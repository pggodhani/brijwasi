import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Events, AlertController, LoadingController, Content } from 'ionic-angular';
import { CategoryPage } from '../category/category';
import { ProductPage } from '../product/product';
import { CustomeProvider } from '../../providers/custome/custome';
import { CartPage } from '../cart/cart';
import { CartProvider } from '../../providers/cart/cart';
import * as $ from 'jquery';
import { LanguageProvider } from '../../providers/language/language';

/**
 * Generated class for the SearchItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-search-item',
        templateUrl: 'search-item.html',
})
export class SearchItemPage {

        pagination: any;
        cartItem: number;
        showFilter = false;
        Special = false;
        itemCode = '';
        size = '';
        data;
        showHeaderFilterBar = true;
        searchString = '';
        width = (window.innerWidth - 52) / 2;
        appLang = localStorage.getItem('appLang')
        @ViewChild(Content) content: Content;
        constructor(
                public navCtrl: NavController,
                private language: LanguageProvider,
                private loadingCtrl: LoadingController,
                public navParams: NavParams,
                public event: Events,
                public alertCtrl: AlertController,
                private service: CustomeProvider,
                private cart: CartProvider) {

        }
        items: any = [];
        selectedSub: any = 'all';
        subCategorys;

        

        ionViewDidLoad() {
                console.log('ionViewDidLoad SearchItemPage');
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present();
                this.cartItem = this.cart.fnGetCountOfCart();
                this.event.subscribe('cart:itemAdded', () => {
                        this.cartItem = this.cart.fnGetCountOfCart();
                });
                this.event.subscribe('appLangChange', () => {
                        this.appLang = this.language.fnGetSelectedAppLang()
                });
                this.searchString = this.navParams.get('data')
                let form = {
                        q: this.searchString
                }
                this.service.post('item/get_search_items', form)
                        .then((result: any) => {
                                this.items = result.item_list
                                this.subCategorys = result.sub_category
                                this.pagination = result.pagination
                                this.scrollToTop();
                                loader.dismiss()
                        }, (error: any) => {
                                loader.dismiss()
                                if (error.message) {
                                        let alert = this.alertCtrl.create({
                                                subTitle: error.message,
                                                buttons: [this.language.fnGetLanguage('title_ok')]
                                        });
                                        alert.present();
                                        return false;
                                }
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                        })

        }

        scrollToTop() {
                this.content.scrollToTop();
        }

        fnCategary() {
                this.navCtrl.push(CategoryPage)
        }

        fnSearch() {
                let loader = this.loadingCtrl.create({
                        content: "Loading..."
                });
                loader.present();
                let form = {
                        q: this.searchString
                }
                this.items = []
                this.pagination = null;
                this.service.post('item/get_search_items', form)
                        .then((result: any) => {
                                this.items = result.item_list
                                this.pagination = result.pagination
                                this.scrollToTop();
                                loader.dismiss()
                        }, (error: any) => {
                                loader.dismiss()
                                if (error.message) {
                                        let alert = this.alertCtrl.create({
                                                subTitle: error.message,
                                                buttons: [this.language.fnGetLanguage('title_ok')]
                                        });
                                        alert.present();
                                        return false;
                                }
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                        })
        }

        fnOpenCart() {
                this.navCtrl.push(CartPage)
        }

        fnOpenProductPage(item) {
                this.navCtrl.push(ProductPage, { 'data': item })
        }

        fnLoadMoreData(e) {
                let form = {}
                form['q'] = this.searchString
                form['page'] = this.pagination.next
                this.service.post('item/get_search_items', form)
                        .then((result: any) => {
                                this.items = this.items.concat(result.item_list)
                                this.pagination = result.pagination
                                e.complete();
                        }, (error: any) => {
                                e.complete();
                        })
        }

        fnImageNotFound(event) {
                event.target.src = "assets/img_logo/default-product.jpg"
        }

        onScroll(e) {
                if (e.directionY === 'up') {
                        this.showHeaderFilterBar = true;
                        $('.selector').fadeIn()
                        if (this.showFilter)
                                $('.filters').fadeIn()

                } else {
                        if (e.deltaY < 80 && e.scrollTop < 80) {
                                this.showHeaderFilterBar = true;
                                $('.selector').fadeIn()
                                if (this.showFilter) {
                                        $('.filters').fadeIn()
                                }
                        } else {
                                this.showHeaderFilterBar = false;
                                $('.selector').fadeOut()
                                $('.filters').fadeOut()
                        }
                }
        }

}
