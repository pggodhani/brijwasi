import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { LanguageProvider } from '../../providers/language/language';
import { Platform } from 'ionic-angular/platform/platform';

/**
 * Generated class for the FilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-filter',
        templateUrl: 'filter.html',
})
export class FilterPage {
        // prise: any
        selectedPrice: any = { lower: 0, upper: 0 };
        selectedcategory: any;
        categories = [];
        sizeArray: any = [];
        is_special = false
        in_stock = false
        item_code = ''
        min = 0;
        max;
        appLang='';
        constructor(public navCtrl: NavController,private language: LanguageProvider,private platform:Platform,private event:Events, public navParams: NavParams, private viewCtrl: ViewController) {
        }

        ionViewDidLoad() {
                console.log('ionViewDidLoad FilterPage');
                let data = this.navParams.get('data')
                data = JSON.parse(JSON.stringify(data))
                this.event.subscribe('appLangChange', () => {
                        this.appLang = this.language.fnGetSelectedAppLang()
                });
                this.appLang = data.appLang
                let categories: any = data.subCategorys
                let selectedcategory: string = data.category_id
                if (selectedcategory && selectedcategory != '') {
                        selectedcategory = ',' + selectedcategory + ',';
                } else {
                        selectedcategory = ''
                }

                for (let index = 0; index < categories.length; index++) {
                        if (selectedcategory.indexOf(',' + categories[index].category_id + ',') != -1) {
                                categories[index]['isSelected'] = true
                        } else {
                                categories[index]['isSelected'] = false
                        }
                }

                this.categories = categories
                let sizeArray: any = data.sizeArray
                let selectedSize = data.size

                if (selectedSize && selectedSize != '') {
                        selectedSize = ',' + selectedSize + ',';
                } else {
                        selectedSize = ''
                }
                for (let index = 0; index < sizeArray.length; index++) {
                        if (selectedSize.indexOf(',' + sizeArray[index].size_id + ',') != -1) {
                                sizeArray[index]['isSelected'] = true
                        } else {
                                sizeArray[index]['isSelected'] = false
                        }
                }
                this.sizeArray = sizeArray
                this.in_stock = data.in_stock
                this.is_special = data.is_special

                this.min = data.price.lower
                this.max = data.price.upper

                this.selectedPrice.lower = data.selectedPrice.lower
                this.selectedPrice.upper = data.selectedPrice.upper
                this.item_code = data.item_code
        }



        fnSearch() {
                let selectedCategory: any = '';
                for (let index = 0; index < this.categories.length; index++) {
                        if (this.categories[index].isSelected) {
                                if (selectedCategory && selectedCategory != '') {
                                        selectedCategory = selectedCategory + ',' + this.categories[index].category_id
                                } else {
                                        selectedCategory = this.categories[index].category_id
                                }
                        }
                }
                let size: any = ''
                for (let index = 0; index < this.sizeArray.length; index++) {
                        if (this.sizeArray[index].isSelected) {
                                if (size && size != '') {
                                        size = size + ',' + this.sizeArray[index].size_id
                                } else {
                                        size = this.sizeArray[index].size_id
                                }
                        }
                }
                let data = {
                        category_id: selectedCategory,
                        size: size,
                        item_code: this.item_code,
                        is_special: this.is_special,
                        in_stock: this.in_stock,
                        selectedPrice: this.selectedPrice,
                }
                this.viewCtrl.dismiss({ data: { isCancelled: false, filterData: data } })
        }
        fnCancel() {
                this.viewCtrl.dismiss({ data: { isCancelled: true } })
        }
}
