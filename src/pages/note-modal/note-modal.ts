import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { LanguageProvider } from '../../providers/language/language';

/**
 * Generated class for the NoteModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
        selector: 'page-note-modal',
        templateUrl: 'note-modal.html',
})
export class NoteModalPage {
        note: string = '';
        isCart: any;
        item;
        constructor(public navCtrl: NavController,
                private language:LanguageProvider,
                public navParams: NavParams, public viewCtrl: ViewController, platform: Platform) {
                platform.ready().then(() => {
                        platform.registerBackButtonAction(() => this.fnDismissModal());
                })
        }

        ionViewDidLoad() {
                this.item = this.navParams.get('data')
                this.isCart = this.navParams.get('isCart')
                if (this.navParams.get('data').note) {
                        this.item = this.navParams.get('data')
                        this.note = this.navParams.get('data').note
                }
        }

        fnDismissModal() {
                let data = {
                        addToCart: false
                }
                this.viewCtrl.dismiss(data)
        }

        fnAddtoCart() {
                let data = {
                        addToCart: true,
                        note: this.note
                }
                this.viewCtrl.dismiss(data)
        }
}
