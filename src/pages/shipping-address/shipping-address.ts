import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Events, MenuController, Navbar } from 'ionic-angular';
import { CustomeProvider } from '../../providers/custome/custome';
import { CartProvider } from '../../providers/cart/cart';
import { LoginPage } from '../login/login';
import { CartPage } from '../cart/cart';
import { OrderSuccessPage } from '../order-success/order-success';
import { LanguageProvider } from '../../providers/language/language';



@Component({
        selector: 'page-shipping-address',
        templateUrl: 'shipping-address.html',
})
export class ShippingAddressPage {
        @ViewChild(Navbar) navBar: Navbar;
        constructor(public navCtrl: NavController,
                private language: LanguageProvider,
                public menu: MenuController,
                public event: Events,
                private alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                public navParams: NavParams,
                public service: CustomeProvider,
                public cart: CartProvider) {

        }

        address_line_1 = localStorage.getItem('address_line_1')
        address_line_2 = localStorage.getItem('address_line_2')

        new_address_line_1;
        new_address_line_2;
        new_phone_no
        newCity
        newState
        flag = false;
        sameAsbillingAddress = true;
        billingAddress = ''
        shippingAddress = ''

        ionViewDidLoad() {
                this.navBar.backButtonClick = (e: UIEvent) => {
                        this.navCtrl.setRoot(CartPage)
                }

                if (localStorage.getItem('address_line_1') && localStorage.getItem('address_line_1') != '' && localStorage.getItem('address_line_1') != 'null' && localStorage.getItem('address_line_1') != null && localStorage.getItem('address_line_1') != undefined && localStorage.getItem('address_line_1') != 'undefined') {
                        this.billingAddress = this.billingAddress + this.address_line_1 + ',\n'
                }
                if (localStorage.getItem('address_line_2') && localStorage.getItem('address_line_2') != '' && localStorage.getItem('address_line_2') != 'null' && localStorage.getItem('address_line_2') != null && localStorage.getItem('address_line_2') != undefined && localStorage.getItem('address_line_2') != 'undefined') {
                        this.billingAddress = this.billingAddress + localStorage.getItem('address_line_2') + ',\n'
                    
                }

                if (localStorage.getItem('city') && localStorage.getItem('city') != '' && localStorage.getItem('city') != 'null' && localStorage.getItem('city') != null && localStorage.getItem('city') != undefined && localStorage.getItem('city') != 'undefined') {
                        this.billingAddress = this.billingAddress + localStorage.getItem('city')
                        if (localStorage.getItem('pincode') && localStorage.getItem('pincode') != '' && localStorage.getItem('pincode') != 'null' && localStorage.getItem('pincode') != null && localStorage.getItem('pincode') != undefined && localStorage.getItem('pincode') != 'undefined') {
                                this.billingAddress = this.billingAddress + ' - ' + localStorage.getItem('pincode');
                        }
                        this.billingAddress = this.billingAddress + ',\n'
                }

                if (localStorage.getItem('state') && localStorage.getItem('state') != '' && localStorage.getItem('state') != 'null' && localStorage.getItem('state') != null && localStorage.getItem('state') != undefined && localStorage.getItem('state') != 'undefined') {
                        this.billingAddress = this.billingAddress  + localStorage.getItem('state') + ',\n'
                }
                if (localStorage.getItem('country') && localStorage.getItem('country') != '' && localStorage.getItem('country') != 'null' && localStorage.getItem('country') != null && localStorage.getItem('country') != 'undefined' && localStorage.getItem('country') != undefined) {
                        this.billingAddress = this.billingAddress + localStorage.getItem('country') + ',\n'
                }

                if (this.sameAsbillingAddress == true) {
                        this.shippingAddress = this.billingAddress
                }

        }

        fnSameAsBillingAddress() {
                if (this.sameAsbillingAddress == true) {
                        this.shippingAddress = this.billingAddress
                }
        }

        fnChangeBillingAddress() {
                if (this.sameAsbillingAddress == true) {
                        this.shippingAddress = this.billingAddress
                }
        }

        fnPlaceOrder() {
                let data = this.cart.fnGetCart()
                let cartItem = [];
                if (data && data.length > 0) {
                        for (let index = 0; index < data.length; index++) {
                                cartItem.push({
                                        'item_id': data[index].item_id,
                                        'qty': data[index].qty,
                                        'note': data[index].note
                                })
                        }
                }
                let form = {
                        'cart': JSON.stringify(cartItem)
                }
                if (!this.billingAddress || this.billingAddress == '') {
                        let alert = this.alertCtrl.create({
                                subTitle: this.language.fnGetLanguage('enter_a_billing_address'),
                                buttons: [this.language.fnGetLanguage('title_ok')]
                        });
                        alert.present();
                        return false;
                }
                form['billing_detail'] = this.billingAddress
                if (this.sameAsbillingAddress) {
                        form['billing_as_shipping'] = '1'
                } else {
                        if (!this.billingAddress || this.billingAddress == '') {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('enter_a_billing_address'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                return false;
                        }
                        form['shipping_detail'] = this.shippingAddress
                }
                let loader = this.loadingCtrl.create({
                        content: 'Loading...'
                });
                loader.present()
                this.service.post('order/create_order', form).then((result: any) => {

                        localStorage.removeItem('cart')
                        this.cart.fnGetCart();
                        this.event.publish('cart:itemAdded');
                        loader.dismiss()
                        this.navCtrl.setRoot(OrderSuccessPage)

                }, (error: any) => {
                        loader.dismiss()
                        if (error && error.status == 4) {
                                let alert = this.alertCtrl.create({
                                        subTitle: this.language.fnGetLanguage('enter_a_billing_address'),
                                        buttons: [this.language.fnGetLanguage('title_ok')]
                                });
                                alert.present();
                                alert.onDidDismiss(() => {
                                        this.navCtrl.setRoot(LoginPage)
                                })
                        } else {
                                if (error.message) {
                                        let alert = this.alertCtrl.create({
                                                subTitle: error.message,
                                                buttons: [this.language.fnGetLanguage('title_ok')]
                                        });
                                        alert.present();
                                } else {
                                        let alert = this.alertCtrl.create({
                                                subTitle: this.language.fnGetLanguage('something_happened_wrong_try_again_after_sometimes'),
                                                buttons: [this.language.fnGetLanguage('title_ok')]
                                        });
                                        alert.present();
                                }
                        }
                })
        }

        fnGotoCartPage() {
                this.navCtrl.setRoot(CartPage)
        }

}