import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the CartProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CartProvider {
        appUpdate = 0
        isPlaceOrder = false;
        cartData: any = [];
        constructor(public http: HttpClient) {
                console.log('Hello CartProvider Provider');
        }
        fnAddToCart(data) {
                let cart = JSON.parse(localStorage.getItem('cart'))
                if (cart) {
                        cart.push(data)
                        this.cartData = cart;
                        localStorage.setItem('cart', JSON.stringify(this.cartData))
                } else {
                        this.cartData = []
                        this.cartData.push(data)
                        localStorage.setItem('cart', JSON.stringify(this.cartData))

                }
        }

        fnGetCart() {
                this.cartData = JSON.parse(localStorage.getItem('cart'))
                if(this.cartData){
                        for (let index = 0; index < this.cartData.length; index++) {
                                if(this.cartData[index].item_name_lang && this.cartData[index].item_name_lang!='' && this.cartData[index].item_name_lang.length>0){
                                        alert(this.cartData[index].item_name_lang.length)
                                        alert(this.cartData[index].item_name_lang)
                                        this.cartData[index].item_name_lang = JSON.parse(this.cartData[index].item_name_lang)
                                }
                        }
                }
                return this.cartData;
        }

        fnGetCountOfCart() {
                this.cartData = JSON.parse(localStorage.getItem('cart'))
                if (this.cartData) {
                        return this.cartData.length;
                }
                return '0';
        }

        fnRemoveFromCart(index) {
                this.cartData.splice(index, 1)
                localStorage.setItem('cart', JSON.stringify(this.cartData))
        }

        fnChangeQty(index, qty) {
                this.cartData = JSON.parse(localStorage.getItem('cart'))
                this.cartData[index].qty = qty;
                localStorage.setItem('cart', JSON.stringify(this.cartData))
        }

        fnNoteChange(index, note) {
                this.cartData = JSON.parse(localStorage.getItem('cart'))
                this.cartData[index].note = note;
                localStorage.setItem('cart', JSON.stringify(this.cartData))
        }

}
