import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import { LanguageProvider } from '../language/language';
/*
  Generated class for the CustomeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CustomeProvider {
        baseUrl = 'http://www.brijwasibj.com/api/v1.0/'
        // baseUrl = 'http://www.hrsales.co.in/api/test_v1.2/'
        token = '';
        constructor(public http: HttpClient,
                public language:LanguageProvider,
                public nativeHttp: HTTP) {
                console.log('Hello CustomeProvider Provider');
                if (localStorage.getItem('token')) {
                        this.token = localStorage.getItem('token')
                }
        }
        j = 0

        post(url, data) {

                var headers = {}
                if (localStorage.getItem('token')) {
                        headers = {
                                'authorization': localStorage.getItem('token')
                        }
                }
                if (data == 'data' || data == '') {
                        data = {}
                }
                return new Promise<any>((resolve, reject) => {
                        return this.nativeHttp.post(this.baseUrl + url, data, headers)
                                .then((result: any) => {
                                        var data;
                                        if (result.data) {
                                                data = JSON.parse(result.data)
                                        }
                                        if (data.status == 1) {
                                                resolve(data)
                                        } else {
                                                reject(data)
                                        }
                                }, (error: any) => {
                                        let err = {
                                                status: 'error',
                                                message: this.language.fnGetLanguage('make sure that you are connected with internet'),
                                                flag: this.j
                                        }
                                        this.j++;
                                        reject(err);
                                })

                })

        }

        imageDownloder(url, data) {
                var headers = {}
                if (localStorage.getItem('token')) {
                        headers = {
                                'authorization': localStorage.getItem('token')
                        }

                }

                return new Promise<any>((resolve, reject) => {
                        return this.http.post(this.baseUrl + url, data, headers).subscribe((result: any) => {
                                if (result.status == 1) {
                                        resolve(result)
                                } else {
                                        reject(result)
                                }
                        }, (error: any) => {
                                let err = {
                                        status: 'error',
                                        message: 'Make sure that you are connected with internet',
                                        flag: this.j
                                }
                                this.j++;
                                reject(err);
                        })

                })
        }

}
