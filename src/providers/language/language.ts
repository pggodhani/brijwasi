import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the LanguageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LanguageProvider {
  languageArray: any;
  language = 'english';
  newLanguageArray = [];
  labelLanguageArray = [];

  constructor(public http: HttpClient) {
    console.log('Hello LanguageProvider Provider');
  }

  fnGetLanguage(string) {
    if (this.labelLanguageArray && this.labelLanguageArray[this.language] && this.labelLanguageArray[this.language][string] && this.labelLanguageArray[this.language][string] != '') {
      return this.labelLanguageArray[this.language][string];
    } else {
      return string;
    }
  }

  fnSetLanguageArray(language) {
    localStorage.setItem('langList', JSON.stringify(language));
    this.languageArray = language;
  }

  fnSetAppLanguage(language) {
    localStorage.setItem('appLang', language)
    this.language = language;
  }

  fnSetLangLabelArray(labelArray) {
    localStorage.setItem('langArray', JSON.stringify(labelArray))
    this.labelLanguageArray = labelArray
  }

  fnGetSelectedAppLang(){
    
    return this.language;
  }

}
